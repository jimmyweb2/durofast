<?php
/**
 *	Template Name: Sidebar
 */

get_header(); ?>
    <div class="header-section-inner">
		<?php
    	do_action('woo_custom_breadcrumb');
    ?>
		<h1 class="main_title"><?php the_title(); ?></h1>
	</div>
	
	<div class="entry-content">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<?php the_content(); ?>

		<?php endwhile; ?>
		<?php endif; ?>
	</div>
	<?php get_sidebar('checkout'); ?>

<?php get_footer(); ?>
