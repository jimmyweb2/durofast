<?php
/**
 * This is the template that displays all of the <head> section.
 *
 * @link https://livecomposerplugin.com/themes/
 *
 * @package LC Blank
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700&amp;lang=en" />
	<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>
<!-- Main Content -->
<div id="main-content">
	<!-- Main contenter -->
	<div class="main-container">
		<div id="header" class="Fixed">
			<nav class="navbar navbar-default df-desktop">
				<div class="header-top container">
					<?php
						wp_nav_menu(array(
							'theme_location' => 'top',
							'menu_class' => 'top-menu nav navbar-nav'
						));
					?>
				</div>
				<div class="header-mid container">
				 	<div class="row">
				 		<div class="logo col-md-6 col-sm-5"><a href="/"><img src="<?php echo get_template_directory_uri(); ?>/images/durofast-logo.png" title="durofast logo" class="main-logo"><img src="<?php echo get_template_directory_uri(); ?>/images/durofast-logo-coloured.png" title="durofast logo" class="fixed-logo duro-hidden"></a></div>
				 		<div class="col-md-6 col-sm-7">
				 			<div class="search"><?php echo get_search_form(); ?></div>
							<div class="icons">
								<ul>
									<?php
									if(is_user_logged_in()) {
										$current_user_df = wp_get_current_user();
										$user_display_name = explode(' ', $current_user_df->first_name);
										$login_title = ucfirst($user_display_name[0]);
									}
									else
										$login_title = 'Login';
									?>
									<li class="login"><a class="icon-img" href="/my-account/"><?php echo $login_title; ?></a><a class="icon-text" href="/my-account/"><?php echo $login_title; ?></a></li>
									<li class="cart"><a class="icon-img" href="<?php echo WC_Cart::get_cart_url(); ?>">Cart</a>
									<?php global $woocommerce; ?>
									<span class="top-qty">
										<span class="top-num">
											<a class="cart-content" href="<?php echo $woocommerce->cart->get_cart_url(); ?>"
											title="<?php _e('Cart View', 'woothemes'); ?>">
											<?php 
											echo number_format(get_cart_qty());
											//echo number_format( $woocommerce->cart->cart_contents_count ); 
											/*echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'woothemes'),
											 $woocommerce->cart->cart_contents_count);*/ ?>
											</a>
										</span>
									</span><a class="icon-text" href="<?php echo WC_Cart::get_cart_url(); ?>">Cart</a>
									</li>
								</ul>
							</div>
				 		</div>						
				 	</div>					
				</div>
				<div class="main-header duro-center">
					<div class="container">
						<?php
							wp_nav_menu(array(
								'theme_location' => 'primary',
								'menu_class' => 'mainNav nav navbar-nav'
							));
						?>
					</div>
				</div>
			</nav>
			<!-- Mobile menu -->
			<nav class="df-mobile">
				<div class="header-top-mobile container">
					<?php
						wp_nav_menu(array(
							'theme_location' => 'top',
							'menu_class' => 'top-menu nav navbar-nav'
						));
					?>
				</div>
				<div class="header-bottom-mobile container">
					<div class="row">
						<div class="logo-mobile mobile-left-header col-xs-5">
							<div class="logo-image">
								<a href="/"><img src="<?php echo get_template_directory_uri(); ?>/images/durofast-logo.png" title="durofast logo" class="main-logo"><img src="<?php echo get_template_directory_uri(); ?>/images/durofast-logo-coloured.png" title="durofast logo" class="fixed-logo duro-hidden"></a>
							</div>
						</div>
						<div class="mobile-right-header col-xs-7">
							<div class="mobile-right-header-inner">
								<div class="icons">
									<ul>
									    <li class="search"><a class="mobile-search-btn" href="javascript:void(0);"><i class="fa fa-search" aria-hidden="true"></i></a></li>
									    <li class="quick-order"><a class="icon-img" href="/quick-order/">Quick Order</a></li>
										<li class="login"><a href="/my-account/" class="icon-img">Login</a></li>
										<li class="cart"><a href="<?php echo WC_Cart::get_cart_url(); ?>" class="icon-img">Cart</a>
										<span class="top-qty">
											<span class="top-num">
												<a class="cart-content" href="<?php echo $woocommerce->cart->get_cart_url(); ?>"
												title="<?php _e('Cart View', 'woothemes'); ?>">
												<?php 
												echo get_cart_qty();
												//echo number_format( $woocommerce->cart->cart_contents_count );
												/*echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'woothemes'),
												 $woocommerce->cart->cart_contents_count);*/ ?>
												</a>
											</span>
										</span>
										</li>
									</ul>
								</div>
								<div class="menu-mobile">
									<a href="#menu" class="navbar-toggle">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</a>
								</div>
								<nav id="menu">
									<?php
										wp_nav_menu(array(
											'menu' => 'Mobile Menu',
											'menu_class' => 'mainNav'
										));
									?>
								</nav>
							</div>
						</div>
						<div class="mobile-search search col-xs-12">
							<?php echo get_search_form(); ?>
						</div>
					</div>
				</div>
			</nav>
		</div>
		<?php get_template_part( 'content', 'banner' ); ?>
	<?php
	if(!is_front_page()){
		?>
		<div class="container">
		<div id="content-area" class="clearfix">
		<?php
	}
	?>
