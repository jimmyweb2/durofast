<?php
/**
 * Blog Page
 */

get_header(); ?>
<div class="header-section-inner">
    <?php
    do_action('woo_custom_breadcrumb');
?>
</div>
<div class="main-page-title">
    <h1 class="main_title"><?php echo get_the_title( get_option('page_for_posts', true) ); ?></h1>
</div>
<!-- <div class="container-fluid light-grey-background blue-text"> -->
<div class="row-section latest-news">
    <div class="row">
        <div class="col-md-9 col-sm-9">
            <?php
            $args = array(
            'post_type' => 'post',
            'posts_per_page' => -1,
            'order' => 'DESC',
            'orderby' => 'post_date',
            'post_status ' => 'publish'
            );
            $blog_query = new WP_Query($args);
            if ( $blog_query->have_posts() ) :

                /* Start the Loop */
                while ( $blog_query->have_posts() ) : 
                    $blog_query->the_post();

                    /*
                     * Include the Post-Format-specific template for the content.
                     * If you want to override this in a child theme, then include a file
                     * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                     */
                    get_template_part( 'content', get_post_format() );

                endwhile;
            else :

                echo 'No content found';

            endif;
            ?>
        </div>
        <div class="col-md-3 col-sm-3 df-sidebar">
            <?php get_sidebar(); ?>
        </div>
    </div>
</div>
<!-- </div> -->
<?php get_footer(); ?>