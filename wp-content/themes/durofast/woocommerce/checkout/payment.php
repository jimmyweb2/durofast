<?php
/**
 * Checkout Payment Section
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/payment.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.5.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! is_ajax() ) {
	do_action( 'woocommerce_review_order_before_payment' );
}
$curr_users = get_currentuserinfo();
$curr_users_role = $curr_users->roles;
if(in_array('credit-account', $curr_users_role) ) {
	$user_type_class = 'credit-account';
	$account_type_name = '';
}
else {
	$user_type_class = 'pay-account';
	$account_type_name = 'Prepaid Account';
}
?>
<div id="payment" class="woocommerce-checkout-payment <?php echo $user_type_class; ?>">
	<?php if ( WC()->cart->needs_payment() ) : ?>
	    <?php
	    if( $account_type_name != "" ) {
			echo '<h3>'.$account_type_name.'</h3>';
		}
	    ?>
		<ul class="wc_payment_methods payment_methods methods">
			<?php
				if ( ! empty( $available_gateways ) ) {
					foreach ( $available_gateways as $gateway ) {
						if(in_array('credit-account', $curr_users_role) && $gateway->id=='cod')
						{
							wc_get_template( 'checkout/payment-method.php', array( 'gateway' => $gateway ) );
						}
						else if($gateway->id!='cod' && !in_array('credit-account', $curr_users_role)) {
							wc_get_template( 'checkout/payment-method.php', array( 'gateway' => $gateway ) );
						}
						/*else if(in_array('cash-account', $curr_users_role) && $gateway->id!='cod') {
							wc_get_template( 'checkout/payment-method.php', array( 'gateway' => $gateway ) );
						}*/					
					}
				} else {
					echo '<li>' . apply_filters( 'woocommerce_no_available_payment_methods_message', WC()->customer->get_country() ? __( 'Sorry, it seems that there are no available payment methods for your state. Please contact us if you require assistance or wish to make alternate arrangements.', 'woocommerce' ) : __( 'Please fill in your details above to see available payment methods.', 'woocommerce' ) ) . '</li>';
				}
			?>
		</ul>
	<?php endif; ?>
	<div class="form-row place-order">
		<noscript>
			<?php _e( 'Since your browser does not support JavaScript, or it is disabled, please ensure you click the <em>Update Totals</em> button before placing your order. You may be charged more than the amount stated above if you fail to do so.', 'woocommerce' ); ?>
			<br/><input type="submit" class="button alt" name="woocommerce_checkout_update_totals" value="<?php esc_attr_e( 'Update totals', 'woocommerce' ); ?>" />
		</noscript>

		<?php wc_get_template( 'checkout/terms.php' ); ?>

		<?php do_action( 'woocommerce_review_order_before_submit' ); ?>

		<?php echo apply_filters( 'woocommerce_order_button_html', '<input type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="' . esc_attr( $order_button_text ) . '" data-value="' . esc_attr( $order_button_text ) . '" />' ); ?>

		<?php do_action( 'woocommerce_review_order_after_submit' ); ?>

		<?php wp_nonce_field( 'woocommerce-process_checkout' ); ?>
	</div>
</div>
<?php
if ( ! is_ajax() ) {
	do_action( 'woocommerce_review_order_after_payment' );
}
