<?php
/**
 * Checkout billing information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-billing.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.9
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/** @global WC_Checkout $checkout */

?>
<div class="woocommerce-billing-fields">
	<?php if ( wc_ship_to_billing_address_only() && WC()->cart->needs_shipping() ) : ?>

		<h3><?php _e( 'Billing &amp; Shipping', 'woocommerce' ); ?></h3>

	<?php else : ?>

		<h3><?php _e( 'Billing details', 'woocommerce' ); ?></h3>

	<?php endif; ?>

	<?php do_action( 'woocommerce_before_checkout_billing_form', $checkout ); ?>

	<div class="billing-info">
		<?php echo get_field( 'checkout_billing_description', 'options' ); ?>
	</div>
	<div class="clear"></div>
	<div class="woocommerce-billing-fields__field-wrapper">
		<?php
		    $is_credit_user = is_credit_account();
		    if( $is_credit_user === true ) {
		    
				$current_user = wp_get_current_user();
				$billing_email_c = get_user_meta( $current_user->ID, 'billing_email', true );
				$billing_address_1_C = get_user_meta( $current_user->ID, 'billing_address_1', true );
				$billing_address_2_c = get_user_meta( $current_user->ID, 'billing_address_2', true );
				$billing_city_c = get_user_meta( $current_user->ID, 'billing_city', true );
				$billing_state_c = get_user_meta( $current_user->ID, 'billing_state', true );
				$billing_postcode_c = get_user_meta( $current_user->ID, 'billing_postcode', true );
				$billing_company_c = get_user_meta( $current_user->ID, 'billing_company', true );
				$billing_name_c = $current_user->first_name. ' '. $current_user->last_name;
			}

			$fields = $checkout->get_checkout_fields( 'billing' );
			$billing_field_count = 1;

			foreach ( $fields as $key => $field ) {
				if( $is_credit_user === true ) {
					if( $billing_field_count == 2 ) {
					?>
					<p class="form-row">
						<label for="billing_purchase_order_number" class="">Email:
						</label>
						<span class="woocommerce-input-wrapper purchase_order_number"><?php echo get_user_meta( $current_user->ID, 'billing_email', true ); ?></span></p>
					<?php
					}
				}
				/*print_r("<pre style='margin-left:50px'>");
				echo $key;
				print_r($field);
				echo $checkout->get_value( $key );
				print_r("</pre>");*/
				if ( isset( $field['country_field'], $fields[ $field['country_field'] ] ) ) {
					$field['country'] = $checkout->get_value( $field['country_field'] );
				}
				if( $is_credit_user === true ) {
					if( $key == 'billing_email' ) {
						woocommerce_form_field( $key, $field, $billing_email_c );
					}
					else if( $key == 'billing_address_1' ) {
						woocommerce_form_field( $key, $field, $billing_address_1_C );
					}
					else if( $key == 'billing_address_2' ) {
						woocommerce_form_field( $key, $field, $billing_address_2_c );
					}
					else if( $key == 'billing_city' ) {
						woocommerce_form_field( $key, $field, $billing_city_c );
					}
					else if( $key == 'billing_state' ) {
						woocommerce_form_field( $key, $field, $billing_state_c );
					}
					else if( $key == 'billing_postcode' ) {
						woocommerce_form_field( $key, $field, $billing_postcode_c );
					}
					else
						woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
				}
				else {
					woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
				}
				

				$billing_field_count++;
			}
		?>
	</div>
	<?php
	if( $is_credit_user === true ) {
	?>
	<div class="df-billing-addresses">
		
		<div class="df-billing-address">
			<span class="df-bold">
				<?php echo $billing_company_c; ?>
			</span>
			<span>
				<?php echo $billing_name_c; ?>
			</span>
			<span>
				<?php echo $billing_address_1_C; ?>
			</span>
			<?php
			if( $billing_address_2_C != "" ) {
			?>
			<span>
				<?php echo $billing_address_2_C; ?>
			</span>
			<?php } ?>
			<span>
				<?php echo $billing_city_c.' '.$billing_postcode_c.' '.$billing_state_c; ?>
			</span>
		</div>
	</div>
	<div class="request-update">
		<a href="<?php echo get_permalink(40792); ?>">Request Update to Company Details</a>
	</div>
<?php } ?>

	<?php do_action( 'woocommerce_after_checkout_billing_form', $checkout ); ?>
</div>

<?php if ( ! is_user_logged_in() && $checkout->is_registration_enabled() ) : ?>
	<div class="woocommerce-account-fields">
		<?php if ( ! $checkout->is_registration_required() ) : ?>

			<p class="form-row form-row-wide create-account">
				<label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
					<input class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" id="createaccount" <?php checked( ( true === $checkout->get_value( 'createaccount' ) || ( true === apply_filters( 'woocommerce_create_account_default_checked', false ) ) ), true ) ?> type="checkbox" name="createaccount" value="1" /> <span><?php _e( 'Create an account?', 'woocommerce' ); ?></span>
				</label>
			</p>

		<?php endif; ?>

		<?php do_action( 'woocommerce_before_checkout_registration_form', $checkout ); ?>

		<?php if ( $checkout->get_checkout_fields( 'account' ) ) : ?>

			<div class="create-account">
				<?php foreach ( $checkout->get_checkout_fields( 'account' ) as $key => $field ) : ?>
					<?php woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>
				<?php endforeach; ?>
				<div class="clear"></div>
			</div>

		<?php endif; ?>

		<?php do_action( 'woocommerce_after_checkout_registration_form', $checkout ); ?>
	</div>
<?php endif; ?>
