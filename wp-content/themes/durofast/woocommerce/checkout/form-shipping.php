<?php
/**
 * Checkout shipping information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-shipping.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.9
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
$is_credit_user = is_credit_account();
if( $is_credit_user === true ) {
	$shipping_credit_account = ' shipping-credit-account';
}
else {
	$shipping_credit_account = '';
}
?>
<div class="woocommerce-shipping-fields<?php echo $shipping_credit_account; ?>">
	<?php if ( true === WC()->cart->needs_shipping_address() ) : ?>

		<h3 id="ship-to-different-address">
			<label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
				<!-- <div class="custom-checkbox"> -->
					<input id="ship-to-different-address-checkbox" class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" <?php checked( apply_filters( 'woocommerce_ship_to_different_address_checked', 'shipping' === get_option( 'woocommerce_ship_to_destination' ) ? 1 : 0 ), 1 ); ?> type="checkbox" name="ship_to_different_address" value="1" />
					<!-- <label for="ship-to-different-address-checkbox"></label> -->
				<!-- </div> -->
				<span class="checkbox-label"><?php _e( 'Delivery via Standard Runs', 'woocommerce' ); ?></span>
				<span class="delivery-run-info">
					<?php echo wpautop( get_field( 'delivery-run-info', 'options' ) ); ?>
				</span>
			</label>
			<label class="run-details">
				<a href="#run-details" class="run-details-link button">Run Details</a>
			</label>
		</h3>

		<div class="shipping_address">

			<?php do_action( 'woocommerce_before_checkout_shipping_form', $checkout ); ?>

			<div class="woocommerce-shipping-fields__field-wrapper">
				<?php
					$fields = $checkout->get_checkout_fields( 'shipping' );

					foreach ( $fields as $key => $field ) {
						if ( isset( $field['country_field'], $fields[ $field['country_field'] ] ) ) {
							$field['country'] = $checkout->get_value( $field['country_field'] );
						}
						woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
					}
				?>
			</div>
			<?php
			if( $is_credit_user === true ) {
			?>
			<div class="shipping-credit-account-details">
				<p class="form-row form-row-first" id="ca_shipping_first_name_field">
					<label>Site Contact Name</label>
					<span class="ca_shipping_first_name ca-span"></span>
				</p>
				<p class="form-row form-row-last" id="ca_shipping_last_name_field">
					<label>Site Contact Phone</label>
					<span class="ca_shipping_last_name"></span>
				</p>
				<p class="form-row form-row-wide" id="ca_shipping_address_1_field">
					<label>Street Address</label>
					<span class="ca_shipping_address_1"></span>
				</p>
				<p class="form-row form-row-wide" id="ca_shipping_address_2_field">
					<span class="ca_shipping_address_2 no-address-2">Apartment, suite, unit etc.</span>
				</p>
				<p class="form-row form-row-last" id="ca_shipping_city_field">
					<label>Suburb</label>
					<span class="ca_shipping_city"></span>
				</p>
				<p class="form-row form-row-last" id="ca_shipping_state_field">
					<label>State</label>
					<span class="ca_shipping_state"></span>
				</p>
				<p class="form-row form-row-last" id="ca_shipping_postcode_field">
					<label>Postcode</label>
					<span class="ca_shipping_postcode"></span>
				</p>
			</div>
			<?php } ?>

			<?php do_action( 'woocommerce_after_checkout_shipping_form', $checkout ); ?>

		</div>

	<?php endif; ?>
</div>
<div class="woocommerce-additional-fields">
	<?php do_action( 'woocommerce_before_order_notes', $checkout ); ?>

	<?php if ( apply_filters( 'woocommerce_enable_order_notes_field', 'yes' === get_option( 'woocommerce_enable_order_comments', 'yes' ) ) ) : ?>

		<?php if ( ! WC()->cart->needs_shipping() || wc_ship_to_billing_address_only() ) : ?>

			<h3><?php _e( 'Additional information', 'woocommerce' ); ?></h3>

		<?php endif; ?>

		<div class="woocommerce-additional-fields__field-wrapper">
			<?php foreach ( $checkout->get_checkout_fields( 'order' ) as $key => $field ) : ?>
				<?php woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>
			<?php endforeach; ?>
		</div>

	<?php endif; ?>

	<?php do_action( 'woocommerce_after_order_notes', $checkout ); ?>
</div>
<div id="run-details" class="white-popup-block mfp-hide">
	<?php echo get_field( 'run_details', 'options' ); ?>
</div>
