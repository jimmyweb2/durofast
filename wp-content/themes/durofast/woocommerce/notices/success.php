<?php
/**
 * Show messages
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/notices/success.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! $messages ) {
	return;
}
/*if( $messages && $_SERVER['HTTP_REFERER'] == get_home_url().'/quick-order/' && $_SERVER['REQUEST_URI'] != get_home_url().'/quick-order/' ) {
	$quick_url = get_home_url().'/quick-order/';
	?>
	<script type="text/javascript">
		jQuery(document).ready(function( $ ) {
			setTimeout(function(){
			  document.location = '<?php echo $quick_url; ?>';
			}, 2000);
		});
	</script>
	<?php
}*/
?>

<?php foreach ( $messages as $message ) : ?>
	<?php 
		$message =  str_replace('Product successfully added to your wishlist.', 'Product successfully added to your list.', $message);
		$message =  str_replace('Wishlist successfully created. 1 items moved', 'Everyday Items list successfully created. 1 item added.', $message);
		$message =  str_replace( array('View cart', 'Basket'), array('View Cart', 'Cart'), $message);
		$shop_return_page = '';
		if( $_SERVER['REDIRECT_URL'] == '/my-lists/edit-my-list/' && isset($_GET['wlid']) && isset($_GET['wlm']) )
		{
			if(isset($_SESSION['shop_page']) && $_SESSION['shop_page']!="")
				$shop_return_page = '<a href="'.$_SESSION['shop_page'].'" class="button">Return to Product List →</a>';
			unset($_SESSION['shop_page']);
		}
		?>
	<div class="woocommerce-message"><?php echo $shop_return_page; echo wp_kses_post( $message ); ?></div>
<?php endforeach; ?>
