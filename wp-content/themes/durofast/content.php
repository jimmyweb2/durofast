<?php
/**
 * Template part for displaying posts
 */
?>
<!-- <article id="post-<?php the_ID(); ?>"> -->
	<?php
	/*if(is_home()) {
		$blog_lead_image = get_field("lead_image",get_the_ID());
	    if(!is_array($blog_lead_image)) {
	        $blog_lead_image = get_field("default_blog_image", "option");
	    }
	    //var_dump($blog_lead_image);
	    ?>
	    <div class="news-img">
	    	<a href="<?php the_permalink(); ?>">
	    		<img class="" src="<?php echo $blog_lead_image["sizes"]["blogimg"]; ?>"/>
	    	</a>
	    </div>
	    <?php
    }*/
	?>
	<div class="row article" id="post-<?php the_ID(); ?>">
		<div class="col-md-4 col-sm-4">
			<?php if ( '' !== get_the_post_thumbnail() && ! is_single() ) : ?>
				<div class="post-thumbnail">
					<a href="<?php the_permalink(); ?>">
						<?php the_post_thumbnail( 'blogimg' ); ?>
					</a>
				</div><!-- .post-thumbnail -->
			<?php endif; ?>
		</div>
		<?php
		if( is_archive() || is_home() ) {
			?>
			<div class="col-md-8 col-sm-8 blog-excerpt">
			<?php
		}
		else {
			?>
			<div class="col-md-12 col-sm-12 blog-content">
			<?php
		}
		?>		
			<header class="entry-header">
				<?php

					if ( is_single() ) {
						// do nothing
					} elseif ( is_front_page() && is_home() ) {
						the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
					} else {
						the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
					}

					if ( 'post' === get_post_type() ) :
						echo '<div class="entry-meta">';
							if ( is_single() ) :
								blog_posted_on();
							else :
								echo blog_time_link();
							endif;
						echo '</div><!-- .entry-meta -->';
					endif;
				?>
			</header><!-- .entry-header -->
			<div class="entry-content">
				<?php
				if( '' !== get_the_post_thumbnail() && is_single() ) {
					?>
					<div class="post-img">
						<?php the_post_thumbnail(); ?>
					</div>
					<?php
				}
				?>
				<?php
					/* translators: %s: Name of current post */
					if(is_home() || is_archive()) {
						$excerpt = get_the_excerpt();
						echo '<p>'.get_excerpt_by_str($excerpt).'</p>';
					}
					else {
						the_content( sprintf(
						__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'cruise-russia' ),
						get_the_title()
						) );
					}
					

					wp_link_pages( array(
						'before'      => '<div class="page-links">' . __( 'Pages:', 'cruise-russia' ),
						'after'       => '</div>',
						'link_before' => '<span class="page-number">',
						'link_after'  => '</span>',
					) );

				if( is_home() || is_archive() ) {
				?>
				<a href="<?php the_permalink(); ?>" class="more-link btn normal">Read More</a>
				<?php } ?>
			</div><!-- .entry-content -->
		</div>
	</div>
<!-- </article> --><!-- #post-## -->
