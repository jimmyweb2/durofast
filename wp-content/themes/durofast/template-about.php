<?php
/**
 *  Template Name: About Us
 */
get_header();
?>
    <div class="header-section-inner">
    <?php
      do_action('woo_custom_breadcrumb');
    ?>
  </div>
  <div class="main-page-title">
    <h1 class="main_title"><?php the_title(); ?></h1>
  </div>
  
  <div class="about-image">
    <?php
    echo $about_image = get_the_post_thumbnail();
    ?>
  </div>
  <div class="entry-content about general-cms-page">
    <?php
    $about_cotent = get_field('about_cotent');
    if($about_cotent) {
      foreach($about_cotent as $about_cotent_row) {
        $title = $about_cotent_row['title'];
        $description = $about_cotent_row['description'];
        ?>
        <div class="page-inner">
          <div class="page-inner-title">
            <h2><?php echo $title; ?></h2>
          </div>
          <div class="page-innter-content">
            <?php echo $description; ?>
          </div>
        </div>
        <?php
      }
    }
    ?>
    <div class="page-inner">
    <?php
    $team_title = get_field('team_title');
    $team_info = get_field('team_info');
    if($team_title!='')
    {
      ?>
        <div class="page-inner-title">
          <h2><?php echo $team_title; ?></h2>
        </div>        
      <?php
    }
    if($team_info!='') {
      ?>
      <div class="page-innter-content">
        <?php echo $team_info; ?>
      </div>
      <?php
    }
    if( have_rows( 'team_list' ) ){
      ?>
      <div class="row">
        <?php
        while ( have_rows( 'team_list' ) ) {
          the_row();
          $name = get_sub_field( 'name' );
          $designation = get_sub_field( 'designation' );
          $phone = get_sub_field( 'phone' );
          $email = get_sub_field( 'email' );
          $profile_image = get_sub_field( 'profile_image' );
          ?>
          <div class="col-md-4 col-sm-6 col-xs-12 team-row">
            <div class="team-inner-wrapper">
              <div class="team-pp-image team-item">
                <img src="<?php echo $profile_image['sizes']['team_pp']; ?>" alt="<?php echo $name; ?>">
              </div>
              <div class="team-info team-item">
                <span><strong><?php echo $name.'</strong>'; if($designation!="") echo ' - '.$designation; ?></span>
                <span><?php if($phone!='') echo '<a href="tel:'.$phone.'">'.$phone.'</a>'; if($email!='') echo ' | <a href="mailto:'.$email.'">'.$email.'</a>';  ?></span>
              </div>
            </div>
          </div>
          <?php
        }
        ?>
      </div>
      <?php
    }
    ?>
    </div>
  </div>

<?php get_footer(); ?>