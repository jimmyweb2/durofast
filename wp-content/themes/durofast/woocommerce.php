<?php
/**
 * The template for displaying all woocommerce pages.
 *
 *
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if( is_shop() || is_product_category() ) {
get_header( 'shop' ); ?>
	
		<div class="row">
		    <div class="col-md-12 header-section">
		    	<div class="header-section-inner">
		    		<?php
			    	do_action('woo_custom_breadcrumb');
			    	if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

							<h1 class="page-title"><?php woocommerce_page_title(); ?></h1>

					<?php endif; ?>
		    	</div>
		    	<div class="header-section-inner">
		    	    <?php //woocommerce_result_count(); ?>
		    	    <?php
		    	    if( $wp_query->max_num_pages == 1 ) {
		    	    	?>
		    	    	<nav class="woocommerce-pagination">
		    	    		<div class="pagination-result-section">
		    	    			<p class="woocommerce-result-count">
		    	    			<?php echo 'Showing '.$wp_query->post_count.' resutls'; ?>		
		    	    			</p>
		    	    		</div>
		    	    	</nav>
		    	    	<?php
					}
					else {
		    	    ?>
		    		<?php woocommerce_pagination(); } ?>
		    	</div>	    	
				
		    </div>
			<div class="col-md-9 col-sm-12 woocommerce-content">
				<?php
					/**
					 * woocommerce_before_main_content hook.
					 *
					 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
					 * @hooked woocommerce_breadcrumb - 20
					 */
					do_action( 'woocommerce_before_main_content' );
				?>

					<?php
						/**
						 * woocommerce_archive_description hook.
						 *
						 * @hooked woocommerce_taxonomy_archive_description - 10
						 * @hooked woocommerce_product_archive_description - 10
						 */
						do_action( 'woocommerce_archive_description' );
					?>

					<?php if ( have_posts() ) : ?>

						<?php
							/**
							 * woocommerce_before_shop_loop hook.
							 *
							 * @hooked woocommerce_result_count - 20
							 * @hooked woocommerce_catalog_ordering - 30
							 */
							do_action( 'woocommerce_before_shop_loop' );
						?>

						<?php woocommerce_product_loop_start(); ?>

							<?php woocommerce_product_subcategories(); ?>

							<?php while ( have_posts() ) : the_post(); ?>

								<?php wc_get_template_part( 'content', 'product' ); ?>

							<?php endwhile; // end of the loop. ?>

						<?php woocommerce_product_loop_end(); ?>

						<?php
							if( $wp_query->max_num_pages == 1 ) {
				    	    	?>
				    	    	<nav class="woocommerce-pagination">
				    	    		<div class="pagination-result-section">
				    	    			<p class="woocommerce-result-count">
				    	    			<?php echo 'Showing '.$wp_query->post_count.' resutls'; ?>		
				    	    			</p>
				    	    		</div>
				    	    	</nav>
				    	    	<?php
							}
							else {
								/**
								 * woocommerce_after_shop_loop hook.
								 *
								 * @hooked woocommerce_pagination - 10
								 */
								do_action( 'woocommerce_after_shop_loop' );
							}
						?>

					<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

						<?php wc_get_template( 'loop/no-products-found.php' ); ?>

					<?php endif; ?>

				<?php
					/**
					 * woocommerce_after_main_content hook.
					 *
					 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
					 */
					do_action( 'woocommerce_after_main_content' );
				?>
			</div>
			<div class="col-md-3 col-sm-12 woocommerce-sidebar">
			    <?php
					/**
					 * woocommerce_sidebar hook.
					 *
					 * @hooked woocommerce_get_sidebar - 10
					 */
					get_template_part( 'inc/product-cat' );
				?>
			</div>
		</div>

<?php get_footer( 'shop' );
}
else if( is_product() ) {
	get_header();
	?>
	<div class="header-section-inner">
		<?php do_action('woo_custom_breadcrumb'); ?>
	</div>	
	<div id="container">
		<div id="content" role="main">
			<?php woocommerce_content(); ?>
		</div>
	</div>
	<?php
	get_footer();
}
else {
	?>
	<?php get_header(); ?>
    <div class="header-section-inner">
		<?php
    	do_action('woo_custom_breadcrumb');
    ?>
		<h1 class="main_title"><?php the_title(); ?></h1>
	</div>
	
	<div class="entry-content">
		<?php woocommerce_content(); ?>
	</div>

	<?php get_footer(); ?>
	<?php
}
