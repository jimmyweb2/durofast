jQuery(document).ready(function ($) {

    jQuery('#banner .banner-slider').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 1200,
        arrows: true,
        dots: true,
        lazyLoad: 'ondemand',
        fade: true,
        cssEase: 'linear',
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    /*variableWidth: true,*/
                }
            }
        ]
    });

    function stickyMenu() {
        winscroll = jQuery(window).scrollTop();
        var menu_height = jQuery('.header-top').height() + jQuery('.header-mid').height() + jQuery('.main-header').height() + jQuery('.df-mobile').height();
        if (winscroll > 100) {

            jQuery('#header').addClass('fixed-header');
        }
        else {
            jQuery('#header').removeClass('fixed-header');
        }
    }

    /*Sticky header*/
    stickyMenu();
    jQuery(window).scroll(function () {
        stickyMenu();
    });

    jQuery('#menu').mmenu({
        classNames: {
            fixedElements: {
                fixed: "fixed-header-mbl"
            }
        },
        navbar: {
            title: "Home",
            titleLink: "parent"
        }
    });

    jQuery('.mobile-search-btn').on('click', function () {
        //console.log('hello');
        //jQuery('.mobile-search').addClass('show-search-form');
        //jQuery(".mobile-search").toggle();
        jQuery(".mobile-search").fadeToggle("slow", "linear");
    });


    /* Place holder for my account page woocommerce*/
    //jQuery( "form.edit-account p.form-row, .login p.form-row, #wc-eway_payments-cc-form p.form-row, .woocommerce-MyAccount-content form p.form-row, form.register p.form-row, form.woocommerce-ResetPassword p.form-row, .wl-form p.form-row, form.checkout.woocommerce-checkout p.form-row" ).each(function( index ) {
    jQuery("form.edit-account p.form-row, #wc-eway_payments-cc-form p.form-row, .woocommerce-MyAccount-content form p.form-row, .wl-form p.form-row, form.checkout.woocommerce-checkout p.form-row").each(function (index) {
        //console.log( index + ": " + $( this ).text() );
        //var placeholdertext = jQuery(this).find('label').text();
        //jQuery(this).find('input').attr('placeholder',placeholdertext);
    });
    jQuery(".wl-form p.form-row").each(function (index) {
        //console.log( index + ": " + $( this ).text() );
        //var placeholdertext = jQuery(this).find('label').text();
        //jQuery(this).find('textarea').attr('placeholder', placeholdertext);
    });
    if (jQuery("#wl-wrapper .shop_table.cart.wl-table.wl-manage tbody tr").length == 0) {
        jQuery('#wl-wrapper .shop_table.cart.wl-table.wl-manage tbody').append('<tr><td colspan="3">There are no lists yet <a href="/my-lists/create-a-list/">Create a list</a></td></tr></p>');
    }

    /* Replace h1 to h2 */
    jQuery('.qty-fieldset').find('h1.qty-legend').replaceWith('<h2>' + jQuery('h1.qty-legend').html() + '</h2>');
    $(".top-num .cart-content").each(function (index) {
        var str = jQuery(this).text().replace(/items/g, '').replace(/item/g, '').replace(/s/g, '');
        jQuery(this).text('');
        //console.log(str);
        jQuery(this).text(str);
        if (str != 0)
            jQuery(this).parents('.top-qty').show();
    });

    /* Product Detail Page */

    //jQuery('body').live('load', function () {
    var idindex = '';
    var nameindex = '';
    var idNameVal = '';
    var classNameVal = '';
    jQuery("div.single_variation_wrap > div").each(function (index) {
        //console.log( index + ": " + jQuery( this ).text() );
        var idName = jQuery(this).attr('id');
        var className = jQuery(this).attr('class');

        if (idName == 'wl-wrapper') {
            idindex = index;
            idNameVal = 'wl-wrapper';
            //console.log(idName+index+'idho id');
        }
        if (className == 'woocommerce-variation-add-to-cart variations_button') {
            nameindex = index
            classNameVal = 'woocommerce-variation-add-to-cart variations_button';          //console.log(className+index);
        }
        if (classNameVal == 'woocommerce-variation-add-to-cart variations_button' && idNameVal == 'wl-wrapper' && (nameindex != "" && idindex != "" && idindex < nameindex)) {
            //console.log('hahaha');
            jQuery("#wl-wrapper").remove().clone().insertAfter(".woocommerce-variation-add-to-cart.variations_button");

        }
        /*console.log("id index"+idindex);
        console.log("name index"+nameindex);*/
        //console.log(className+idName);
    });
    //});

    jQuery('.woocommerce-page .products li').each(function (index, value) {
        if (index % 4 == 0) {
            $(this).addClass("clear")
        } else {
            $(this).addClass("remove-clear");
        }

        if (index % 2 == 0) {
            $(this).addClass("clear-mobile")
        } else {
            $(this).addClass("remove-clear-mobile");
        }
    });

    addSearchClass();
    //setHeightMenu();
    jQuery(window).resize(function () {
        addSearchClass();
    });

    function addSearchClass() {
        if (jQuery(window).width() > 767) {//console.log('hello hehe');
            jQuery('.search-results .entry-content .search-row').each(function (index, value) {
                if (index % 4 == 0) {
                    $(this).addClass("clear")
                } else {
                    $(this).addClass("remove-clear");
                }
                //console.log('hhahahahha');
            });
        }
        else {
            //console.log('hahahello');
            jQuery('.search-results .entry-content .search-row').each(function (index, value) {
                if (index % 2 == 0) {
                    $(this).addClass("clear-mobile")
                } else {
                    $(this).addClass("remove-clear-mobile");
                }
            });
        }
    }

    jQuery(document).ready(function () {
        if (jQuery(".woocommerce .woocommerce-message:contains('Undo?')").length) {
            jQuery('<span class="undo-btn"><i class="fa fa-times" aria-hidden="true"></i></span>').appendTo(".woocommerce .woocommerce-message");
            jQuery('.undo-btn i.fa').on('click', function () {
                jQuery('.woocommerce-message').fadeOut("slow");
            });
        }
    });

    jQuery('ul.products h2.woocommerce-loop-product__title').dotdotdot();
    jQuery('.search-header h2.entry-title').dotdotdot();

    /* AutoSuggest Search */
    function split_s(val) {
        return val.split(/,\s*/);
    }

    function extractLast_s(term) {
        return split_s(term).pop();
    }

    $("#searchs, .mobile-search #searchs")
    // don't navigate away from the field on tab when selecting an item
        .on("keydown", function (event) {
            if (event.keyCode === $.ui.keyCode.TAB &&
                $(this).autocomplete("instance").menu.active) {
                event.preventDefault();
            }
        })
        .autocomplete({
            source: function (request, response) {
                $.getJSON("/wp-content/themes/durofast/inc/ajax-get-search-results.php", {
                    sterm: extractLast_s(request.term)
                }, response);
            },
            response: function (event, ui) {

                /*response($.map(data.d, function (item) {
                    return {
                        id: item.Value,
                        value: item.Text
                    }
                }));*/
            },
            search: function () {
                // custom minLength
                var term = extractLast_s(this.value);
                if (term.length < 2) {
                    return false;
                }
            },
            focus: function () {
                // prevent value inserted on focus
                return false;
            },
            select: function (event, ui) {
                var terms = split_s(this.value);
                // remove the current input
                terms.pop();
                // add the selected item
                terms.push(ui.item.value);
                // add placeholder to get the comma-and-space at the end
                //terms.push( "" );
                this.value = terms.join(", ");
                //console.log(ui);

                return false;
            },
            classes: {
                "ui-autocomplete": "general-search-result",
            },
        });
    fixSearchResultDivWidth();

    function fixSearchResultDivWidth() {
        var search_box_width = jQuery('input#search').width();
        //console.log(search_box_width);
        jQuery('.ui-menu.ui-autocomplete.general-search-result').css('width', search_box_width);
    }

    /* End AutoSuggest Search */

    jQuery('.wl-rad-table #rad_priv').attr('checked', 'checked');

    // home banner height detection
    set_home_banner_height($);

    // make form text field surrounded by text
    surround_test_field($);
    var refreshBtn = jQuery('#refresh-img').val();
    //jQuery('input[name="update_cart"]').attr({"type": 'image', "src": refreshBtn, 'alt': 'Submit', 'padding': '0'});

    if ( jQuery( '.woocommerce-MyAccount-content' ).find('div').hasClass( "woocommerce-Addresses" ) ) {
        //jQuery('.woocommerce-MyAccount-content > p').css({'font-size':24,'display': 'inline-block'});
        jQuery('.woocommerce-MyAccount-content > p').addClass('edit-address-title');
    }

    //if (window.location.href.indexOf("/checkout/") > -1) {
        var shippingInfo = jQuery('.shipping-address-info').html();
        jQuery('div.shipping-tab-contents').prepend( "<div class='shipping-info'>"+shippingInfo+"</div>" );        
    //}

    jQuery( ".woocommerce ul.products li.product" ).each(function( index ) {
        //jQuery(this).css( 'border', '1px solid red' );
        if( jQuery(this).find('.price > .woocommerce-Price-amount.amount').length > 0 )
            jQuery(this).find('.price').wrapInner( "<div class='price-wrapper'></div>" );
    });
    if(jQuery('#wcmca_address_form_shipping label.checkbox.wcmca_default_checkobx_label').length > 0) {
        //jQuery(this).find('input.input-checkbox ').
        var shipping_text = jQuery('#wcmca_address_form_shipping label.checkbox.wcmca_default_checkobx_label').html();
        var replace_shipping_text = shipping_text.replace( 'shipping', 'delivery' );
        $('#wcmca_address_form_shipping label.checkbox.wcmca_default_checkobx_label').contents().filter(function() {
            return this.nodeType == 3
        }).each(function(){
            this.textContent = this.textContent.replace('Make this address the default shipping address','Make this address the default delivery address');
        });
        //console.log(replace_shipping_text);
        //jQuery('#wcmca_address_form_shipping label.checkbox.wcmca_default_checkobx_label').html('hello');
    }
    //console.log(jQuery('#wcmca_address_select_menu_shipping').length);
    if(jQuery('#wcmca_address_select_menu_shipping').length>0) {
        //console.log(jQuery('#wcmca_address_select_menu_shipping').val());
        var newOptionList = '';
        jQuery('#wcmca_address_select_menu_shipping option').each(function () {
            var opValue = jQuery(this).val();
            var opText = jQuery(this).text();
            var opSelected = jQuery(this).attr('selected');
            if(opSelected=="selected") {
                var opSelectedVal = 'selected="selected"';
            }
            else {
                var opSelectedVal = '';
            }
            opText = opText.replace( 'shipping', 'delivery' );
            opText = opText.replace( 'Shipping', 'Delivery' );
            //console.log(opValue+opText+opSelected);
            newOptionList = newOptionList+'<option value="'+opValue+'" '+opSelectedVal+'>'+opText+'</option>'
        });
        if(newOptionList!="") {
            jQuery('#wcmca_address_select_menu_shipping').html('');
            jQuery('#wcmca_address_select_menu_shipping').html(newOptionList);
        }
        //console.log(newOptionList);
    }

    if( jQuery('.cart-summary.cart-page').length > 0 ) {
        //jQuery('.cart-summary.cart-page').find('.shipping th').html('Delivery');
    }

    if(jQuery('.woocommerce form fieldset').length > 0 ) {
        jQuery('.woocommerce form fieldset .form-row').each(function (index) {
            var lebelTxt = jQuery(this).find('label').html();
            lebelTxt = lebelTxt.replace('Current password','Current Password');
            lebelTxt = lebelTxt.replace('New password','New Password');
            lebelTxt = lebelTxt.replace('Confirm new password','Confirm New Password');
            jQuery(this).find('label').html(lebelTxt);
            //console.log(lebelTxt);
        });
    }
    if(jQuery('.shop_table.wl-table.wl-manage').length > 0 && jQuery('.shop_table.wl-table.wl-manage tbody tr').length == 1) {
        var checkText = jQuery('.shop_table.wl-table.wl-manage').find('tbody tr td').html();
        if(checkText.indexOf("There is no list") != -1) {
            // something
            var lebelTxtEI = jQuery('.shop_table.wl-table.wl-manage').find('tbody tr td > a').html();
            lebelTxtEI = lebelTxtEI.replace('Create a wish list','Create a Wish List');
            jQuery('.shop_table.wl-table.wl-manage').find('tbody tr td > a').html(lebelTxtEI);
        }        
    }
    if(jQuery('.wl-intro .wl-share-url').length > 0) {
        var wishListURLTxt = jQuery('.wl-intro .wl-share-url').find('strong').html();
        wishListURLTxt = wishListURLTxt.replace('Wishlist URL','Everyday Items List URL');
        jQuery('.wl-intro .wl-share-url').find('strong').html(wishListURLTxt);
    }

    if( jQuery('#wl-wrapper .woocommerce-message').length > 0 ) {
        var wlMsg = jQuery('#wl-wrapper .woocommerce-message').find('a').html();
        if( typeof wlMsg != "undefined" ) {
            wlMsg = wlMsg.replace('Continue Shopping','Return to Product List');
            jQuery('#wl-wrapper .woocommerce-message').find('a').html(wlMsg);
        }        
    }    
    jQuery('.wl-share-url').remove();
    //getSearchBoxWidth($);
    jQuery.ui.autocomplete.prototype._resizeMenu = function () {
      var ul = this.menu.element;
      ul.outerWidth(this.element.outerWidth());
    }

    /* Add green tick for for wishlist*/
    if(jQuery('.in-wishlist .wl-already-in').length > 0 ) {
        jQuery('.in-wishlist .wl-already-in').prepend('<i class="fa fa-check" aria-hidden="true"></i>');
    }
    /* Magnific pop up */
    jQuery('.download-pdf').on( 'click', function() {
        set_download_cookie('download-pdf');
    });
    if( jQuery('#download-catalogue-popup').length ) {
        jQuery('.df-newsletter').magnificPopup({
            type: 'inline',
            preloader: false,
            //modal: true
        });
    }
    jQuery('.four-block-link').on('click', function() {
        var f_block = $(this).data('click-id');
        setTimeout(function(){
            jQuery.magnificPopup.open({
              items: {
                src: f_block
              },
              type:'inline'
            });
        }, 300);
    });    

    jQuery('a.df-tooltip').tooltip(); 
    /* mulstiple customer address */
    //console.log( jQuery('#wcmca_address_form_shipping #wcmca_shipping_is_default_address_field').html() );
    jQuery('#wcmca_address_form_shipping #wcmca_shipping_is_default_address_field').remove().clone().insertAfter('#wcmca_shipping_postcode_field');
    jQuery('#wcmca_country_field_container_shipping').remove().clone().insertAfter('#wcmca_shipping_city_field');
    jQuery('#wcmca_shipping_first_name_field label').html('Site Contact Name<abbr class="required" title="required">*</abbr>');
    jQuery('#wcmca_shipping_last_name_field label').html('Site Contact Phone<abbr class="required" title="required">*</abbr>');
    jQuery('#wcmca_shipping_company_field').remove();

    jQuery('#shipping_first_name_field label').html('Site Contact Name<abbr class="required" title="required">*</abbr>').show();
    jQuery('#shipping_last_name_field label').html('Site Contact Phone<abbr class="required" title="required">*</abbr>').show();
    jQuery('.woocommerce-MyAccount-content form #shipping_company_field').remove();
    if( jQuery('#ship-to-different-address-checkbox').is(':checked') ) {
        jQuery('#shipping_option_pickup').prop('checked', false);
    }
    jQuery('#shipping_option_pickup').on( 'click', function() {
        if( jQuery(this).is(':checked') ) {
            jQuery('#ship-to-different-address-checkbox').prop('checked', false);
            jQuery('.woocommerce-shipping-fields .shipping_address').hide();
            jQuery(this).trigger("update_checkout");
            jQuery( document ).ajaxComplete( function() {
                if( jQuery('#shipping_option_pickup').is(':checked') ) {
                    console.log('pick up in store');
                    jQuery('.delivery-method-pickup').show();
                    jQuery('.delivery-method').hide();
                }
            });
        }
        else {
            jQuery('#ship-to-different-address-checkbox').prop('checked', true);
            jQuery('input[name="ship_to_different_address"]').trigger('change');
        }
        
    });
    jQuery('#ship-to-different-address-checkbox').on( 'click', function() {
        if( jQuery(this).is(':checked') ) {
            jQuery('#shipping_option_pickup').prop('checked', false);
            jQuery('.delivery-method-pickup').hide();
            jQuery('.delivery-method').show();
        }
        else {
            jQuery('#shipping_option_pickup').prop('checked', true);
        }
    });
    //jQuery('#wcmca_shipping_country_field').remove();
    //console.log(jQuery('.woocommerce-MyAccount-content #wcmca_custom_addresses .woocommerce-Addresses.addresses > .woocommerce-Address').html());
    jQuery('.woocommerce-MyAccount-content #wcmca_custom_addresses .woocommerce-Addresses.addresses > .woocommerce-Address').remove().clone().insertAfter('#wcmca_custom_addresses h2.wcmca_additional_addresses_list_title');
    
    if( jQuery('.four-blocks-wrappers').length > 0 ) {
        if (navigator.userAgent.match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile/i)) {
                jQuery('.tooltip').tooltipster({
                contentCloning: false,
                theme: ['tooltipster-noir', 'tooltipster-noir-customized'],
                animation: 'fade',
                delay: 300,
                trigger: 'click',
                /*functionPosition: function(instance, helper, position){
                                    position.coord.left += 125;
                                    return position;
                                }*/
            });
        }
        else {
            jQuery('.tooltip').tooltipster({
                    contentCloning: false,
                    theme: ['tooltipster-noir', 'tooltipster-noir-customized'],
                    animation: 'fade',
                    delay: 300,
                    //trigger: 'click',
                    /*functionPosition: function(instance, helper, position){
                                        position.coord.left += 125;
                                        return position;
                                    }*/
                });
        }
    }
    

    /*jQuery('.tooltip.t-right').tooltipster({
        contentCloning: false,
        theme: ['tooltipster-noir', 'tooltipster-noir-customized'],
        animation: 'fade',
        delay: 300,
        functionPosition: function(instance, helper, position){
                            position.coord.right += -200;
                            return position;
                        }
    });*/
    if( jQuery('.woocommerce-MyAccount-content form #billing_phone_field').length > 0 )
    {
        /*console.log(jQuery(".woocommerce-MyAccount-content form #billing_company_field").html());

        //jQuery(".woocommerce-MyAccount-content form #billing_phone_field").remove().clone().insertAfter(".woocommerce-MyAccount-content p#billing_email_field");
        jQuery(".woocommerce-MyAccount-content form #billing_phone_field").appendTo(".woocommerce-MyAccount-content p#billing_email_field");*/
    }
    /* bold address section on my account */
    /*jQuery( '.woocommerce-MyAccount-content > .woocommerce-Addresses > .woocommerce-Address address' ).addClass('hahaha');*/
    /*jQuery( ".woocommerce-MyAccount-content > .woocommerce-Addresses > .woocommerce-Address address br:nth-of-type(1)" ).wrap( "<div class='new'></div>" );*/
    var brCount = 1;
    jQuery('.woocommerce-MyAccount-content > .woocommerce-Addresses > .woocommerce-Address address').each(function() {
        
            jQuery(this).contents().filter(function() {
                
                return this.nodeType == 3;  
            }).wrap('<span></span>');
        
        brCount++;
    });
    if( jQuery( '#wl-wrapper' ).length > 0 ) {
        var listName = jQuery( 'table.shop_table.wl-table.wl-manage tbody tr:nth-of-type(1) .product-name a' ).html();
        if( listName == 'Wishlist' ) {
            jQuery( 'table.shop_table.wl-table.wl-manage tbody tr:nth-of-type(1) .product-name a' ).html('My List');
        }
    }
    if( jQuery( '.wl-list-pop.woocommerce' ).length > 0 ) {
        jQuery('.wl-list-pop.woocommerce dl dd').each(function() {
            var listName = jQuery( this ).find('a').html();
            if( listName == 'Wishlist' ) {
                jQuery( this ).find('a').html('My List');
            }
        });       
        
    }
    /*jQuery( "<p>Test</p>" ).insertBefore( ".woocommerce-MyAccount-content > .woocommerce-Addresses > .woocommerce-Address address br:nth-of-type(1)" ).wrap( "<div class='new'></div>" );*/
    changeAddressTitle();
    addClassHtml();
    orderSummaryFix($);
    add_table_row_class();
    general_function();
    //df_check_phone();
    validateRegistrationForm();
    validateLoginForm();
    validate_checkout_phone();
    validate_checkout_pob();
});

function validateLoginForm() {
    /* Login form validation */
    if( jQuery(".woocomerce-form.login").length > 0 ) {
        jQuery( ".woocomerce-form.login p" ).each(function( index ) {
            jQuery( this ).find( 'input[type=text], input[type=password]' ).attr( 'required', 'required' );
        });
        jQuery(".woocomerce-form.login").validate();
        jQuery.validator.messages.required = 'Opps!';
    }

    /* Register form validation */
    if( jQuery(".woocommerce form.register").length > 0 ) {
        jQuery( "form.register p" ).each(function( index ) {
            var tname = jQuery( this ).find( 'input[type=text]').attr("name");
            if( tname != "billing_address_2" ) {
                jQuery( this ).find( 'input[type=text], input[type=password]' ).attr( 'required', 'required' );

                jQuery( this ).find( 'textarea' ).attr( 'required', 'required' );
                jQuery( this ).find( 'select' ).attr( 'required', 'required' );
            }
        });
        jQuery("form.register").validate({
            rules: {
                billing_phone: {
                  phoneAUS: true
                }
              }
        });
        jQuery.extend(jQuery.validator.messages, {
             required: "Opps!",
             email: "Opps!"
        });
        jQuery.validator.addMethod("phoneAUS", function(phone_number, element) {
            phone_number = phone_number.replace(/\s+/g, "");
            return this.optional(element) || phone_number.length > 9 &&
            ( phone_number.match(/^0\d{9}$/) || phone_number.match(/^6\d{10}$/) || phone_number.match(/^\+6\d{10}$/) );
        }, "Opps!");

        //jQuery.validator.messages.required = 'Opps!';

    }
    /*console.log('haha');
    jQuery( '.woocomerce-form.login input[type=submit]' ).on( 'click', function(e) {
        console.log('maya');
        var isError = false;
        var inputUsername = jQuery( '#username' );
        var inputPassword = jQuery( '#password' );
        var formField = jQuery( this );
        if( inputUsername.val() == "" ) {
            console.log('uername');
            setError( inputUsername, '' )
            isError = true;
            //console.log(chk_mobile);
        }
        if( inputPassword.val() == "" ) {
            //console.log(inputtxt);
            setError( inputPassword, '' )
            isError = true;
            //console.log(chk_mobile);
        }
        if( isError === true ) {
            e.preventDefault();
        }
    } );*/
    /* Error message display for checkout form */
    jQuery( document ).ajaxComplete( function() {
    if ( jQuery( 'body' ).hasClass( 'woocommerce-checkout' ) || jQuery( 'body' ).hasClass( 'woocommerce-cart' ) ) {
            if( jQuery( '.woocommerce-error' ).length > 0 ) {
                jQuery('html, body').stop().animate({ 
                    //scrollTop: 0 
                    scrollTop: jQuery("body").offset().top
                }, "slow");
            }
        }
    } );
}

function validateRegistrationForm() {
    jQuery( 'form.registers input[name=register]' ).on( 'click', function(e) {
        
        var dfIsError = false;
        var firstName = jQuery( 'form.register #reg_billing_first_name' );
        var lastName = jQuery( 'form.register #reg_billing_last_name' );
        var companyName = jQuery( 'form.register #reg_billing_company' );
        var regEmail = jQuery( 'form.register #reg_email' );
        var regPhone = jQuery( 'form.register #reg_billing_phone' );
        var billingAdd = jQuery( 'form.register #billing_address_1' );
        var billingCity = jQuery( 'form.register #billing_city' );
        var billingState = jQuery( '.register #billing_state' );
        var billingPostcode = jQuery( 'form.register #billing_postcode' );
        var regPassword = jQuery( 'form.register #reg_password' );
        var regHowHear = jQuery( 'form.register #reg_billing_how_did_you_hear' );

        if( firstName.val() == '' ) {
            setError( firstName, '' );
            dfIsError = true;
        }
        else {
            setError( firstName, 'no' );
        }

        if( lastName.val() == '' ) {
            setError( lastName, '' );
            dfIsError = true;
        }
        else {
            setError( lastName, 'no' );
        }

        if( companyName.val() == '' ) {
            setError( companyName, '' );
            dfIsError = true;
        }
        else {
            setError( companyName, 'no' );
        }

        if( regEmail.val() == '' ) {
            setError( regEmail, '' );
            dfIsError = true;
        }
        else if( !dfValidateEmail(regEmail.val()) ) {
            setError( regEmail, 'Invalid Email!', 'large' );
            dfIsError = true;
        }
        else {
            setError( regEmail, 'no' );
        }

        if( regPhone.val() == '' ) {
            setError( regPhone, '' );
            dfIsError = true;
        }
        else if( df_validate_phone( regPhone.val(), 'reg' ) === false ) {
            setError( regPhone, 'Invalid Phone!', 'large' );
            dfIsError = true;
        }
        else {
            setError( regPhone, 'no' );
        }

        if( billingAdd.val() == '' ) {
            setError( billingAdd, '' );
            dfIsError = true;
        }
        else {
            setError( billingAdd, 'no' );
        }

        if( billingCity.val() == '' ) {
            setError( billingCity, '' );
            dfIsError = true;
        }
        else {
            setError( billingCity, 'no' );
        }

        if( billingState.val() == '' ) {
            setError( billingState, '' );
            dfIsError = true;
        }
        else {
            setError( billingState, 'no' );
        }

        if( billingPostcode.val() == '' ) {
            setError( billingPostcode, '' );
            dfIsError = true;
        }
        else {
            setError( billingPostcode, 'no' );
        }

        if( regPassword.val() == '' ) {
            setError( regPassword, '' );
            dfIsError = true;
        }
        else {
            setError( regPassword, 'no' );
        }

        if( regHowHear.val() == '' ) {
            setError( regHowHear, '' );
            dfIsError = true;
        }
        else {
            setError( regHowHear, 'no' );
        }

        if( dfIsError === true ) {
            e.preventDefault();
        }
    });
}
function setError( id, error_msg, op = 'error' ) {
    if( error_msg == 'no' ) {
        jQuery( id ).closest( 'p' ).find( 'label.error-class' ).remove();
    }
    else {
        if( op == 'large' ) {
            var errClass = 'error-class error-l';
        }
        else 
            var errClass = 'error-class';
                
        if( error_msg == "" ) {
            error_msg = 'Opps!';
        }
        
        jQuery( id ).closest( 'p' ).append('<label class="'+errClass+'">'+error_msg+'</label>').show();
    }
}
function dfValidateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function validate_checkout_phone( ) {
    if( jQuery( '.checkout.woocommerce-checkout' ).length > 0 ) {
        checkPhoneByID( 'billing_phone' );
        checkPhoneByID( 'shipping_phone' );
        /*jQuery( '.checkout.woocommerce-checkout input#billing_phone' ).on( 'change paste keyup', function() {
            var inputtxt = jQuery( '#billing_phone' ).val();
            if( inputtxt != "" ) {
                //console.log(inputtxt);
                var chk_mobile = df_validate_phone_checkout(inputtxt,'billing_phone');
                //console.log(chk_mobile);
                if( jQuery( '#billing_phone' ).closest( 'p' ).find( 'label.error-class' ).length < 0 ) {}
                if(!chk_mobile) {
                    

                    if( jQuery( '#billing_phone' ).closest( 'p' ).find( 'label.error-class' ).length > 0 ) {

                    }
                    else {
                        setError( jQuery( '#billing_phone' ), 'Opps!' );
                        if( !jQuery( this ).closest( 'form.checkout' ).find( '.actions a[href=#next]' ).hasClass( 'disable-action' ) ) {
                            
                            var action_val = jQuery( this ).closest( 'form.checkout' ).find( '.actions a[href=#next]' ).addClass( 'disable-action' );
                        }
                        
                    }

                    
                    //console.log(action_val);
                }
                else
                {
                    setError( jQuery( '#billing_phone' ), 'no' );
                    if( jQuery( this ).closest( 'form.checkout' ).find( '.actions a[href=#next]' ).hasClass( 'disable-action' ) ) {

                        jQuery( this ).closest( 'form.checkout' ).find( '.actions a[href=#next]' ).removeClass( 'disable-action' );
                    }
                }
            }
        } );

        jQuery( '.checkout.woocommerce-checkout input#shipping_phone' ).on( 'change paste keyup', function() {
            var inputtxt = jQuery( '#shipping_phone' ).val();
            
            if( inputtxt != "" ) {
                //console.log(inputtxt);
                var chk_mobile = df_validate_phone_checkout(inputtxt,'shipping_phone');
                if(!chk_mobile) {
                    setError( jQuery( '#shipping_phone' ), 'Opps!' );
                }
                else
                {
                    setError( jQuery( '#shipping_phone' ), 'no' );
                }
                //console.log(chk_mobile);
            }
        } );/*

        /* Check phone on next click */
        /*jQuery( '.checkout.woocommerce-checkout .actions ul li a' ).on( 'click', function() {
            var next = jQuery( this ).attr( 'href' );
            //console.log( next );
            if( next == '#next' ) {
                var inputtxt = jQuery( '#billing_phone' ).val();
                if( inputtxt != "" && jQuery( 'input#billing_phone' ).length > 0 ) {
                    console.log(inputtxt);
                    var chk_mobile = df_validate_phone_checkout(inputtxt,'billing_phone');
                    //console.log(chk_mobile);
                    if(!chk_mobile) {
                        console.log('haha');
                        setError( jQuery( '#billing_phone' ), 'Opps!' );
                        event.preventDefault();
                        return false;
                    }
                    else
                    {
                        setError( jQuery( '#billing_phone' ), 'no' );
                        return true
                    }
                }
            }        
        });*/
    }
}

function checkPhoneByID( id ) {
    jQuery( '.checkout.woocommerce-checkout input#'+id ).on( 'change paste keyup', function() {
        var inputtxt = jQuery( '#'+id ).val();

        if( inputtxt != "" ) {
            //console.log(inputtxt);
            var chk_mobile = df_validate_phone_checkout(inputtxt,id);
            //console.log(chk_mobile);
            
            if(!chk_mobile) {
                

                if( jQuery("#shipping_phone-error").is(":visible") ) {
                    jQuery( '#shipping_phone-error' ).hide();
                }
                if( jQuery( '#'+id ).closest( 'p' ).find( 'label.error-class' ).length > 0 ) {
                    //setError( jQuery( '#'+id ), 'no' );
                }
                else {
                    setError( jQuery( '#'+id ), 'Invalid Phone!' );
                    
                }

                
                //console.log(action_val);
            }
            else
            {
                setError( jQuery( '#'+id ), 'no' );
            }
        }
        else {
            setError( jQuery( '#'+id ), 'no' );
        }
    } );
}

function df_validate_phone_checkout(inputtxt, id) {
    inputtxt = inputtxt.replace(/[\+\(\)]/g, '');
    inputtxt = inputtxt.replace(' ', '');
    var phoneno1 = /^\d{10}$/;
    var phoneno2 = /^\d{11}$/;
    if(!inputtxt.match(phoneno1) && !inputtxt.match(phoneno2))
    {
        jQuery( '.checkout.woocommerce-checkout input#'+id ).closest( 'p' ).find('.error-class').show();
        return false;
    }
    else
    {
        jQuery( '.checkout.woocommerce-checkout input#'+id ).closest( 'p' ).find('.error-class').hide();
        return true;
    }
}

function validate_checkout_pob() {
    if( jQuery( '.checkout.woocommerce-checkout' ).length > 0 ) {
        validate_checkout_pob_by_id( 'shipping_address_1' );
        validate_checkout_pob_by_id( 'shipping_address_2' );
        validate_checkout_pob_by_id( 'shipping_postcode' );
        /*jQuery( '.checkout.woocommerce-checkout input#shipping_address_1' ).on( 'change paste keyup', function() {
            var inputtxt = jQuery( '#shipping_address_1' ).val();
            if( inputtxt != "" ) {
                //console.log(inputtxt);
                var addresspob = df_validate_pob(inputtxt);
                //console.log(chk_mobile);
                if( jQuery( '#shipping_address_1' ).closest( 'p' ).find( 'label.error-class' ).length < 0 ) {}
                if(!addresspob) {
                    setError( jQuery( '#shipping_address_1' ), 'Opps!' );
                }
                else
                {
                    setError( jQuery( '#shipping_address_1' ), 'no' );
                }
            }
        } );

        jQuery( '.checkout.woocommerce-checkout input#shipping_address_2' ).on( 'change paste keyup', function() {
            var inputtxt = jQuery( '#shipping_address_2' ).val();
            
            //if( inputtxt != "" ) {
                //console.log(inputtxt);
                var addresspob = df_validate_pob(inputtxt);
                if(!addresspob) {
                    setError( jQuery( '#shipping_address_2' ), 'Opps!' );
                }
                else
                {
                    setError( jQuery( '#shipping_address_2' ), 'no' );
                }
                //console.log(chk_mobile);
            //}
        } );

        jQuery( '.checkout.woocommerce-checkout input#shipping_postcode' ).on( 'change paste keyup', function() {
            var inputtxt = jQuery( '#shipping_postcode' ).val();
            
            if( inputtxt != "" ) {
                //console.log(inputtxt);
                var addresspob = df_validate_pob(inputtxt);
                if(!addresspob) {
                    setError( jQuery( '#shipping_postcode' ), 'Opps!' );
                }
                else
                {
                    setError( jQuery( '#shipping_postcode' ), 'no' );
                }
                //console.log(chk_mobile);
            }
        } );*/
    }
}

function validate_checkout_pob_by_id( id ) {
    jQuery( '.checkout.woocommerce-checkout input#'+id ).on( 'change paste keyup', function() {
        var inputtxt = jQuery( '#'+id ).val();
        
        if( inputtxt != "" ) {
            //console.log(inputtxt);
            var addresspob = df_validate_pob(inputtxt);
            if(!addresspob) {
                
                if( jQuery("#shipping_address_1-error").is(":visible") ) {
                    jQuery( '#shipping_address_1-error' ).hide();
                }
                if( jQuery( '#'+id ).closest( 'p' ).find( 'label.error-class' ).length > 0 ) {

                }
                else {
                    setError( jQuery( '#'+id ), 'No PO Box!' );
                    if( !jQuery( this ).closest( 'form.checkout' ).find( '.actions a[href=#next]' ).hasClass( 'disable-action' ) ) {
                        
                        //var action_val = jQuery( this ).closest( 'form.checkout' ).find( '.actions a[href=#next]' ).addClass( 'disable-action' );
                    }
                    
                }
            }
            else
            {
                setError( jQuery( '#'+id ), 'no' );
                if( jQuery( this ).closest( 'form.checkout' ).find( '.actions a[href=#next]' ).hasClass( 'disable-action' ) ) {

                    //jQuery( this ).closest( 'form.checkout' ).find( '.actions a[href=#next]' ).removeClass( 'disable-action' );
                }
            }
            //console.log(chk_mobile);
        }
    } );
}

function df_validate_pob(inputtxt) {
    var inputtxt = inputtxt.replace(/ /g,"");
    inputtxt = inputtxt.toLowerCase();
    inputtxt = inputtxt.replace('p.o.', 'po');
    inputtxt = inputtxt.replace('p.o', 'po');
    
    if (inputtxt.indexOf('pobox') > -1)
    {
      
      return false;
    }
    else {
       
        return true;
    }
}

function df_check_phone()
{
    // For billing info
    if( jQuery( '#reg_billing_phone' ).length > 0 ) {
        jQuery( '.checkout.woocommerce-checkout input#billing_phone' ).on( 'keyup', function() {
            var inputtxt = jQuery( '#billing_phone' ).val();
            if( inputtxt != "" ) {
                //console.log(inputtxt);
                var chk_mobile = df_validate_phone(inputtxt);
                //console.log(chk_mobile);
            }
        } );
    }
    jQuery( '.checkout.woocommerce-checkout .actions ul li a' ).on( 'click', function() {
        var next = jQuery( this ).attr( 'href' );
        //console.log( next );
        if( next == '#next' ) {
            var inputtxt = jQuery( '#billing_phone' ).val();
            if( inputtxt != "" ) {
                /*console.log(inputtxt);
                var chk_mobile = df_validate_phone(inputtxt);
                console.log(chk_mobile);*/
                var chk_mobile = df_validate_phone(inputtxt);
            }
        }        
    });

    // For shipping info
    jQuery( '.checkout.woocommerce-checkout input#shipping_last_name' ).on( 'keyup', function() {
        var inputtxt = jQuery( this ).val();
        if( inputtxt != "" ) {
            //console.log(inputtxt);
            //var chk_mobile = df_validate_phone(inputtxt);
            //console.log(chk_mobile);
        }
    } );
    jQuery( '.checkout.woocommerce-checkout .actions ul li a' ).on( 'click', function() {
        var next = jQuery( this ).attr( 'href' );
        //console.log( next );
        if( next == '#next' ) {
            var inputtxt = jQuery( '#shipping_last_name' ).val();
            if( inputtxt != "" ) {
                /*console.log(inputtxt);
                var chk_mobile = df_validate_phone(inputtxt);
                console.log(chk_mobile);*/
                //var chk_mobile = df_validate_phone(inputtxt);
            }
        }        
    });
}

function df_validate_phone(inputtxt, op = 'check') {
    var phoneno1 = /^\d{10}$/;
    var phoneno2 = /^\d{11}$/;
    if( op == 'reg' ) {
        if(!inputtxt.match(phoneno1) && !inputtxt.match(phoneno2))
        {
            return false;
        }
    }
    else {
        if(!inputtxt.match(phoneno1) && !inputtxt.match(phoneno2))
        {
            jQuery( '.checkout.woocommerce-checkout input#billing_phone' ).closest( 'p' ).find('.error-class').show();
            return false;
        }
        else
        {
            jQuery( '.checkout.woocommerce-checkout input#billing_phone' ).closest( 'p' ).find('.error-class').hide();
        }
    }
}

function general_function() {
    // Remove edit button for my accounts addresses section
    if( jQuery( 'body.df-credit-account' ).length > 0 ) {
        jQuery( '.woocommerce-MyAccount-content .woocommerce-Address-title.title a' ).remove();
        jQuery( '#wcmca_custom_addresses .col2-set.addresses header a' ).remove();
    }

    /* Run details pop up */
    if( jQuery('.run-details-link').length > 0 ) {
        jQuery('.run-details-link').magnificPopup({
            type: 'inline',
            preloader: false,
            //modal: true
        });
    }    

    if( jQuery( '.woocommerce-checkout' ).length > 0 ) {
        var checkout_el = jQuery('#customise_checkout_field');
        checkout_el.clone(true).insertAfter('#ship-to-different-address');
        checkout_el.remove();
    }

    /* style checkbox */
    /*var checkout_chk_id = jQuery('#customise_checkout_field input[type=checkbox]').attr( 'id' );
    jQuery('#customise_checkout_field input[type=checkbox]').wrap('<div class="custom-checkbox"></div>').after('<label for="'+checkout_chk_id+'"></label>');

    var checkbox_count = 1;
    jQuery('#wl-items-form input[type=checkbox]').each( function( index ) {
        jQuery(this).attr( 'id', checkbox_count );
        jQuery(this).wrap('<div class="custom-checkbox"></div>').after('<label for="'+checkbox_count+'"></label>');
        checkbox_count++;
    });

    var shipping_add_chk_id = jQuery('#wcmca_address_form_shipping .checkbox.wcmca_default_checkobx_label input[type=checkbox]').attr( 'id' );
    jQuery('#wcmca_address_form_shipping .checkbox.wcmca_default_checkobx_label input[type=checkbox]').wrap('<div class="custom-checkbox"></div>').after('<label for="'+shipping_add_chk_id+'"></label>');

    var checkbox_count = 1;
    jQuery('.woocommerce-form-login input[type=checkbox], .gform_wrapper form input[type=checkbox]').each( function( index ) {
        var checkbox_id = jQuery( this ).attr( 'id' );
        jQuery(this).wrap('<div class="custom-checkbox"></div>').after('<label for="'+checkbox_id+'" class="gform"></label>');
        checkbox_count++;
    });*/
    /* Checkbox style for add new address pop up */
    var shipping_add_chk_id = jQuery('#wcmca_address_form_shipping .checkbox.wcmca_default_checkobx_label input[type=checkbox]').attr( 'id' );
    jQuery('#wcmca_address_form_shipping .checkbox.wcmca_default_checkobx_label input[type=checkbox]').wrap('<div class="custom-checkbox"></div>').after('<label for="'+shipping_add_chk_id+'"></label>');

    /* Checkbox style normal */
    if( jQuery( 'form#wl-items-form' ).length > 0 ) {
        var checkbox_count = 1;
        jQuery('#wl-items-form input[type=checkbox]').each( function( index ) {
            jQuery(this).attr( 'id', 'df-checkbox-'+checkbox_count );
            jQuery(this).wrap('<div class="custom-checkbox"></div>').after('<label for="df-checkbox-'+checkbox_count+'"></label>');
            checkbox_count++;
        });
    }
    else {
        jQuery('form input[type=checkbox]').each( function( index ) {
            var checkbox_id = jQuery( this ).attr( 'id' );
            jQuery(this).wrap('<div class="custom-checkbox"></div>').after('<label for="'+checkbox_id+'" class="gform"></label>');
        });
    }    


    /* payment method icons */
    if( jQuery( 'li.wc_payment_method.payment_method_eway_payments' ).length > 0 ) {
        //console.log('credit card');        

        setInterval(function(){
            if( !jQuery( 'li.wc_payment_method.payment_method_eway_payments' ).find( 'i' ).hasClass( 'fa-cc-mastercard' ) ) {
                jQuery( '<i class="fa fa-cc-visa"></i><i class="fa fa-cc-mastercard"></i>' ).appendTo( 'li.wc_payment_method.payment_method_eway_payments > label' );
            }
        }, 100);
    }

    /* append table for payment */
    if( jQuery( '#payment.credit-account' ).length > 0 ) {
        setInterval(function(){
            if( jQuery( 'table.shop_table.woocommerce-checkout-review-order-table tfoot tr.payment-credit-account' ).length <= 0 ) {
                jQuery('table.shop_table.woocommerce-checkout-review-order-table tfoot').append( '<tr class="payment-credit-account"><th>Payment Method</th><td>Pay with Credit Account</td></tr>' );
            }
        }, 100);      
    }

    if( jQuery('form.checkout.woocommerce-checkout').length > 0 ) {
        setInterval( function() {
            var check_total = jQuery( '.woocommerce-checkout #order_review thead tr th.product-total' ).html();
            if( check_total.indexOf( 'Total incl. GST' ) > -1 ) {
                //console.log( check_total );
                jQuery( '.woocommerce-checkout #order_review thead tr th.product-total' ).html( 'Total' );
            }            
            
            /*if( jQuery( '.woocommerce-checkout #order_review thead tr th.product-total' ).html() ) {
                console.log(jQuery( '.woocommerce-checkout #order_review thead tr th.product-total' ).html( 'Total' ));
            }*/
        }, 100);
    }

    // Copy shipping info
    if( jQuery( '.shipping-credit-account-details' ).length > 0 ) {
        jQuery( '.checkout.woocommerce-checkout .actions ul li a' ).on( 'click', function() {
            var next = jQuery( this ).attr( 'href' );
            //console.log( next );
            if( next == '#next' ) {
                load_delivery_address_credit_account();
            }        
        });
        
        jQuery( '#wcmca_address_select_menu_shipping' ).on( 'change', function() {
            var callC_count = 1;
            setInterval( function(){
                if( callC_count <= 10 ) {
                    load_delivery_address_credit_account();
                }                
                callC_count++;
            }, 1000 );
        } );
    }

    /* Style select dropdown box*/
    //jQuery('select#wcmca_address_select_menu_shipping').wrap('<div class="df-select-style"></div>');
    //console.log(jQuery( '#tab-wl-items' ).length);
    if( jQuery( '#tab-wl-items' ).length > 0 ) {
        // do nothing
    }
    else {
        if( jQuery( 'select' ).length > 0 ) {
            jQuery('select#wcmca_address_select_menu_shipping, form.register select').select2();
        }
    }

    
    jQuery('.woocommerce-message a:contains("Create a list")').hide();
    

    // add info create everyday item
    jQuery( 'form' ).find( 'label[for=wishlist_title]' ).append( "<span class='everyday-tip'>Tip - Name and describe the list according to your project that you're working on</span>" ); 

    // Style creat a list button
    if( jQuery( 'table.wl-table' ).find('a').html() == 'Create a list' ) {
        jQuery( 'table.wl-table' ).find('a').html( 'Create a List' )
        jQuery( 'table.wl-table' ).find('a').addClass( 'btn add-list' );
    }
    

    /* Disable fields force */
    /*if( jQuery( '.woocommerce-shipping-fields.shipping-credit-account' ).length > 0 ) {
        jQuery( '.woocommerce-shipping-fields.shipping-credit-account .woocommerce-shipping-fields__field-wrapper' ).find('input').attr( 'disabled', 'disabled' );
        jQuery( '.woocommerce-shipping-fields.shipping-credit-account .woocommerce-shipping-fields__field-wrapper' ).find('select').attr( 'disabled', 'disabled' );
    }*/

    /* Add name in address select dropdown on checkout page*/
    if( jQuery( '#wcmca_address_select_menu_shipping' ).length > 0 ) {
        jQuery( '#wcmca_address_select_menu_shipping' ).attr( 'name', 'wmua_address_id' );
        jQuery( '#wcmca_address_select_menu_shipping' ).on( 'change', function() {
            var addID = jQuery( this ).val();
            jQuery('#shipping_address_id').attr( 'value', addID );
        } );
        jQuery( '.checkout.woocommerce-checkout .actions ul li a[href=#next]' ).on( 'click', function() {
            var addID = jQuery( '#wcmca_address_select_menu_shipping' ).val();
            jQuery('#shipping_address_id').attr( 'value', addID );
        });
    }

    /* Populate email address */
    if( jQuery( 'input.logged_user_email' ).length > 0 ) {
        jQuery( 'input#input_5_10' ).val( jQuery( 'input.logged_user_email' ).val() );
    }

    /* Cart col update from ipad */
    resize_cart();
    jQuery(window).resize(function () {
        resize_cart();
    });

}

function resize_cart($) {
    if( jQuery('#main-quick-order').length > 0 || jQuery('.df-cart').length > 0 ) {
        var left_el, right_el;
        if( jQuery('#main-quick-order').length > 0 ) {
            var left_el = jQuery('#main-quick-order .quick-order-left');
            var right_el = jQuery('#main-quick-order .quick-order-right');
        }
        else {
            var left_el = jQuery('.df-cart .entry-content .row div.left');
            var right_el = jQuery('.df-cart .entry-content .row div.right');
        }
        if (jQuery(window).width() < 1200) {
            if( left_el.hasClass('col-md-9') ) {
                
                left_el.removeClass( 'col-md-9' );
                left_el.addClass( 'col-md-8' );
            }

            if( right_el.hasClass('col-md-3') ) {
                right_el.removeClass( 'col-md-3' );
                right_el.addClass( 'col-md-4' );
            }
            
        }
        else {
            if( left_el.hasClass('col-md-8') ) {
                left_el.removeClass( 'col-md-8' );
                left_el.addClass( 'col-md-9' );
            }

            if( right_el.hasClass('col-md-4') ) {
                right_el.removeClass( 'col-md-4' );
                right_el.addClass( 'col-md-3' );
            }
        }
    }
    
}

function load_delivery_address_credit_account() {
    
        
        var s_first_name = jQuery('.woocommerce-shipping-fields.shipping-credit-account input#shipping_site_contact').val();
        var s_last_name = jQuery('.woocommerce-shipping-fields.shipping-credit-account input#shipping_phone').val();
        var s_address_1 = jQuery('.woocommerce-shipping-fields.shipping-credit-account input#shipping_address_1').val();
        var s_address_2 = jQuery('.woocommerce-shipping-fields.shipping-credit-account input#shipping_address_2').val();
        var s_city= jQuery('.woocommerce-shipping-fields.shipping-credit-account input#shipping_city').val();
        var s_state_val = jQuery('.woocommerce-shipping-fields.shipping-credit-account select#shipping_state').val();
        var s_state_name = jQuery('.woocommerce-shipping-fields.shipping-credit-account select#shipping_state option:selected').text();
        jQuery('.woocommerce-shipping-fields.shipping-credit-account span#select2-shipping_state-container').css('border','1px solid red');
        var s_postcode = jQuery('.woocommerce-shipping-fields.shipping-credit-account input#shipping_postcode').val();

        jQuery( '.ca_shipping_first_name' ).html( s_first_name );
        jQuery( '.ca_shipping_last_name' ).html( s_last_name );
        jQuery( '.ca_shipping_address_1' ).html( s_address_1 );
        if( s_address_2 != "" ) {
            jQuery( '.ca_shipping_address_2' ).html( s_address_2 );
            jQuery( '.ca_shipping_address_2' ).removeClass( 'no-address-2' );
        }
        else
        {
            jQuery( '.ca_shipping_address_2' ).html( 'Apartment, suite, unit etc.' );
            jQuery( '.ca_shipping_address_2' ).addClass( 'no-address-2' ).parent( 'p' ).hide();
        }
        jQuery( '.ca_shipping_city' ).html( s_city );
        if( s_state_name.indexOf("Select a state") > -1 )
            jQuery( '.ca_shipping_state' ).html( '' );
        else
            jQuery( '.ca_shipping_state' ).html( s_state_name );

        jQuery( '.ca_shipping_postcode' ).html( s_postcode );

        /*console.log( jQuery( 'select#shipping_state' ).val() + jQuery( 'select#shipping_state option:selected' ).text() );*/

        //console.log(s_first_name+s_last_name+s_address_1+s_address_2+s_city+s_state_val+s_state_name+s_postcode);
}
function add_table_row_class() {
    if( ( jQuery('#order_review .shop_table').length > 0 || jQuery('.woocommerce-order.custom .shop_table.order_details').length > 0 ) && jQuery('.shop_table tfoot').length > 0 ) {
        

        var trClass;
        
        // remove payment method row for confirmation page
        if( jQuery('.woocommerce-order.custom .shop_table.order_details').length > 0 ) {
            jQuery( '.woocommerce-order.custom .shop_table.order_details' ).find('tfoot tr th:contains("Payment Method:")').parent().remove();
        }

        setInterval(function(){
            if ( !jQuery('#order_review .shop_table tfoot').hasClass("styled") && !jQuery('.woocommerce-order.custom .shop_table.order_details tfoot').hasClass("styled") ) {
                if( jQuery('.woocommerce-order.custom .shop_table.order_details').length > 0 )
                    var row_count = jQuery('.woocommerce-order.custom .shop_table.order_details tbody tr').length;
                else
                    var row_count = jQuery('#order_review .shop_table tbody tr').length;

                jQuery('#order_review .shop_table tfoot tr, .woocommerce-order.custom .shop_table.order_details tfoot tr').each( function( index ) {
                    
                    if( row_count % 2 == 1 ) {
                        trClass = 'tr-even';
                    }
                    else
                        trClass = 'tr-odd';
                    
                    jQuery(this).addClass( trClass );
                    row_count++;
                });
                jQuery('#order_review .shop_table tfoot, .woocommerce-order.custom .shop_table.order_details tfoot').addClass('styled');
            }
        }, 1000);
        
    }
}

function orderSummaryFix($) {
    
    $( ".woocommerce-checkout #content-area aside" ).clone().appendTo( ".woocommerce-checkout .checkout.woocommerce-checkout .billing-tab-contents, .woocommerce-checkout .checkout.woocommerce-checkout .shipping-tab-contents" );
    jQuery('.woocommerce-checkout form.checkout.woocommerce-checkout #wizard aside h3').on('click', function () {
        //console.log('hello');
        //jQuery('.mobile-search').addClass('show-search-form');
        //jQuery(".mobile-search").toggle();
        jQuery(".woocommerce-checkout form.checkout.woocommerce-checkout #wizard aside .widget_shopping_cart_content").fadeToggle("slow", "linear");
    });
    
    setInterval( function() {
        if( jQuery('ul.woocommerce-mini-cart').length > 0 ) {
            jQuery( 'ul.woocommerce-mini-cart li' ).each( function( index ) {
                if( jQuery( this ).find( 'dl' ).length > 0 && !jQuery( this ).find( 'span.quantity' ).hasClass( 'variable' ) )
                {
                    jQuery( this ).find( 'span.quantity' ).addClass( 'variable' );
                }
            });
        }
    }, 200);
}

function set_home_banner_height($){
    $('.banner-image').css('height', $(window).height());
    $('.resize-to-parent').resizeToParent();
}

function surround_test_field($){
    $('.paragraph-text').find('label').remove();
    var element = $('.paragraph-text').detach();
    $('.field-space').append(element);
}

/* Cookie for download pdf */
function ccgetCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
function set_download_cookie(cname) {
    date = new Date();
    date.setTime(date.getTime()+(600*1000));
    expires = "; expires="+date.toGMTString();
    var expiryDate = new Date();
    document.cookie = cname+"=yes; expires="+date.toGMTString()+"; path=/";
}
function cc_delete_cookie(name) {
    document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;Path=/;';
}

function setHeightMenu($) {
    /* Get tallest height of the menu items in mega menu*/
    if (jQuery(window).width() > 767) {
        var maxHeight = -1;
        //console.log('menu height');
        jQuery('#mega-menu-primary > li ').each(function() {
            var maxHeight = -1;
            jQuery(this).find("ul.mega-sub-menu li a").each(function() {
                maxHeight = maxHeight > jQuery(this).height() ? maxHeight : jQuery(this).height();
                //jQuery(this).css('border','1px solid red');
            });
            //console.log(maxHeight);
            jQuery(this).find("ul.mega-sub-menu li a").css('height',maxHeight+11);
        });
    }
}

/* tooltip */
function showToolTip($) {
    jQuery('.tooltip.left').tooltipster({
        contentCloning: false,
        theme: ['tooltipster-noir', 'tooltipster-noir-customized'],
        animation: 'fade',
        delay: 300,
        functionPosition: function(instance, helper, position){
                            position.coord.left += 150;
                            return position;
                        }
    });
}

// Customize the text in address section
function changeAddressTitle( $ ) {

    jQuery( ".woocommerce-MyAccount-content #wcmca_custom_addresses .addresses" ).each(function( index ) {
        /*var additional_address = jQuery(this).find('h4').html();
        jQuery(this).find('h4').html('Delivery Address');
        jQuery(this).find('h4').show();
        jQuery(this).find('address').prepend( '<span class="address-title">'+additional_address +'</span><div style="clear:both"></div>' );*/

        jQuery(this).find('.address').each( function( index ) {
            var additional_address = jQuery(this).find('h4').html();
            jQuery(this).find('h4').html('Delivery Address');
            jQuery(this).find('h4').show();
            jQuery(this).find('address').prepend( '<span class="address-title">'+additional_address +'</span><div style="clear:both"></div>' );

            var default_add = jQuery(this).find('.wcmca_default_address_badge').html();
            if( jQuery.type(default_add) !== 'undefined' ) {
            //console.log(default_add);
                jQuery(this).find('h4').html('Default Delivery Address');
            }
            
        });
        //jQuery(this).ecg('.address header .wcmca_default_address_badge').css('border','1px solid red');
    });
    /*var additional_address = jQuery('.woocommerce-MyAccount-content #wcmca_custom_addresses .addresses').find('h4').html();
    jQuery('.woocommerce-MyAccount-content #wcmca_custom_addresses .addresses').find('h4').html('Delivery Address');
    jQuery('.woocommerce-MyAccount-content #wcmca_custom_addresses .addresses').find('h4').show();
    jQuery('.woocommerce-MyAccount-content #wcmca_custom_addresses .addresses > address ').prepend( '<span class="address-title">'+additional_address +'</span>' );*/
}

function addClassHtml( $ ) {
    jQuery('.quick-order-cart-update input.button').attr('src','/wp-content/themes/durofast/images/refresh-btn-icon.png');
}