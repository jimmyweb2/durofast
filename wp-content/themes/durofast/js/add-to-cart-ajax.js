jQuery(document).ready(function($){     
  $("#quick-order-btn").click(function(e){ 
    e.preventDefault();  // Prevent the click from going to the link
    jQuery('.quick-order-error').hide();
    if(jQuery('#quick-order').val()=="") {
        return false;
    }
    var product_id = '';
    var variation_id = '';
    var prod_attribute = 'novalue';
    var prod_attribute_val = '';
    var add_to_cart = '';
    var item = {};
    $( ".form-addtocart input" ).each(function( index ) {
      //console.log( index + ": " + $( this ).val() );
      if(index!=0) {
        var element_name = $(this).attr('name');
        var element_value = $(this).val();
        if(element_name=='product_id')
          product_id = element_value;
        if(element_name=='add-to-cart')
          add_to_cart = element_value;
        if(element_name=='variation_id')
          variation_id = element_value;

        if(index==4) {
            prod_attribute = element_name;
            prod_attribute_val = element_value;
            //console.log('variableprod'+prod_attribute+prod_attribute_val);
        }
        var attr_name = jQuery(this).attr('name');
        //console.log(attr_name);
        if( attr_name.indexOf('attribute') > -1 ) {
            //console.log('variable attribute');
            attributeName = jQuery(this).attr( 'name' ),
            attributevalue = jQuery(this).val();
            item[attributeName] = attributevalue;
        }
      }
    });
    // Remove from here
    /*var item = {},
    check = true;
    variations = $variation_form.find( 'select[name^=attribute]' ); 
    if ( !variations.length) {
    variations = $variation_form.find( '[name^=attribute]:checked' );
    }
    if ( !variations.length) {
    variations = $variation_form.find( 'input[name^=attribute]' );
    }
    variations.each( function() {
        var $this = $( this ),
        attributeName = $this.attr( 'name' ),
        attributevalue = $this.val(),
        index,
        attributeTaxName;

        $this.removeClass( 'error' );

        if ( attributevalue.length === 0 ) {
            index = attributeName.lastIndexOf( '_' );
            attributeTaxName = attributeName.substring( index + 1 );
            $this
            .addClass( 'required error' )
            .before( 'Please select' + attributeTaxName + '' )
            check = false;
        } else {
            item[attributeName] = attributevalue;
        }
    });*/
    // Remove to here
    $.ajax({
        url: wc_add_to_cart_params.ajax_url,
        method: 'post',
        data: { 
            'action'            : 'df_ajax_add_to_cart',
            'product_id'        : product_id,
            //'add-to-cart'       : add_to_cart,
            'variation_id'      : variation_id,
            //'var_attribute'     : prod_attribute,
            //'var_attribute_val' : prod_attribute_val,
            'variation'   : item
        }
    }).done( function (response) {
          jQuery('#quick-order').val('');
          if( response.error != 'undefined' && response.error ){
            //some kind of error processing or just redirect to link
            // might be a good idea to link to the single product page in case JS is disabled
            var error_mgs = response.woo_error_notice;
            jQuery('.quick-order-error').html('');
            jQuery('.quick-order-error').html(error_mgs);
            jQuery('.quick-order-error').removeClass('success');
            
            jQuery('.quick-order-error').show();
            //return true;
          } else {
            //window.location.href = SO_TEST_AJAX.checkout_url;
            /*console.log('else');
            console.log(response.success);
            console.log(response.result);*/
            var success_mgs = response.woo_success_notice;
            jQuery('.quick-order-error').html('');
            jQuery('.quick-order-error').html(success_mgs);
            jQuery('.quick-order-error').addClass('success');
            jQuery('.quick-order-error').show();
            if(response.cart_item_count!=0) {
                jQuery('.top-qty a.cart-content').html('');
                jQuery('.top-qty a.cart-content').html(response.cart_item_count);
                jQuery('.top-qty').show();
            }
            jQuery('.cart-list table tbody').html('');
            jQuery('.cart-list table tbody').html(response.result);
            jQuery('.cart-summary-inner .order-total td').html('');
            jQuery('.cart-summary-inner  .cart-subtotal td').html('');
            jQuery('.cart-summary-inner  .tax-rate td').html('');
            jQuery('.cart-summary-inner .order-total td').html(response.carttotal);
            jQuery('.cart-summary-inner  .cart-subtotal td').html(response.cartsubtotal);
            jQuery('.cart-summary-inner  .tax-rate td').html(response.gst);
            if ( jQuery( '.wc-proceed-to-checkout' ).hasClass( "not-incart" ) ) {
                jQuery('.wc-proceed-to-checkout').removeClass('not-incart');
            }
            var updateBtn = jQuery('.hidden-cart-update-btn').html();
            //console.log(updateBtn);
            jQuery( '<tr class="custom-cart-update"><td colspan="8" class="quick-order-cart-update">'+updateBtn+'</td></tr>' ).appendTo( '.cart-list table tbody' );
            addClassHtml();
            jQuery('#main-quick-order.woocommerce .cart-summary').removeClass('hide-quick-order-summary');
            //jQuery('.cart-summary-inner .order-total .woocommerce-Price-currencySymbol').html(response.carttotal);
            //jQuery('.cart-summary-inner  .cart-subtotal .woocommerce-Price-currencySymbol').html(response.carttotal);
            //console.log(response.carsubtotal);
            //console.log(response.carttotal);
          }
    });

  });

  /* Ajax add to cart for product page */
    jQuery('.custom-qty').on('change paste keyup',function(e){
        var qty_val = jQuery(this).val();
        //console.log(qty_val);
        jQuery(this).closest('tr').find('form .quantity input[name=quantity]').attr( 'value', qty_val );
    });
  $( document ).on( 'click', '.single_add_to_cart_button', function(e) {
        e.preventDefault();
        jQuery('.custom-woo-message').html('').hide();
        $variation_form = jQuery( this ).closest( '.variations_form' );
        /*console.log(jQuery(this).closest('.fixed-btn-cart').html());
        console.log(jQuery('form.variations_form').html());*/
        var var_id = $variation_form.find( 'input[name=variation_id]' ).val();
        var product_id = $variation_form.find( 'input[name=product_id]' ).val();
        var quantity = $variation_form.find( 'input[name=quantity]' ).val();
        var boxqty_val = $variation_form.find( 'input[name=boxqty_val]' ).val();
        $( '.ajaxerrors' ).remove();
        //.log( var_id + product_id + quantity );
        var item = {},
        check = true;
        variations = $variation_form.find( 'select[name^=attribute]' ); 
        if ( !variations.length) {
        variations = $variation_form.find( '[name^=attribute]:checked' );
        }
        if ( !variations.length) {
        variations = $variation_form.find( 'input[name^=attribute]' );
        }
        variations.each( function() {
            var $this = $( this ),
            attributeName = $this.attr( 'name' ),
            attributevalue = $this.val(),
            index,
            attributeTaxName;

            $this.removeClass( 'error' );

            if ( attributevalue.length === 0 ) {
                index = attributeName.lastIndexOf( '_' );
                attributeTaxName = attributeName.substring( index + 1 );
                $this
                .addClass( 'required error' )
                .before( 'Please select' + attributeTaxName + '' )
                check = false;
            } else {
                item[attributeName] = attributevalue;
            }
        });

        if ( !check ) {
            return false;
        }

        var $thisbutton = $( this );
        if ( $thisbutton.is( '.single_add_to_cart_button' ) ) {
            $thisbutton.removeClass( 'added' );
            $thisbutton.append( '<i class="fa fa-spinner fa-spin"></i>' );
            if ( $(this).hasClass('variable-prod') === true ){
                var product_id = $variation_form.find( 'input[name=product_id]' ).val();
                var quantity = $variation_form.find( 'input[name=quantity]' ).val();
                var data = {
                    action: 'bodycommerce_ajax_add_to_cart_woo',
                    product_id: product_id,
                    quantity: quantity,
                    variation_id: var_id,
                    variation: item,
                    boxqty_val: boxqty_val
                };
            }
            else {
                $simple_form = $( this ).closest( '.cart.variations_form' );
                var product_id = $simple_form.find( 'input[name=product_id]' ).val();
                var quantity = $simple_form.find( 'input[name=quantity]' ).val();
                var data = {
                    action: 'bodycommerce_ajax_add_to_cart_woo_single',
                    product_id: product_id,
                    quantity: quantity,
                    boxqty_val: boxqty_val
                };
            }
            $( 'body' ).trigger( 'adding_to_cart', [ $thisbutton, data ] );
            $.post( wc_add_to_cart_params.ajax_url, data, function( response ) {
                if ( ! response )
                return;
            //console.log('response');
                var this_page = window.location.toString();
                this_page = this_page.replace( 'add-to-cart', 'added-to-cart' );
                if ( response.error && response.product_url ) {
                    window.location = response.product_url;
                    return;
                }
                if ( wc_add_to_cart_params.cart_redirect_after_add === 'yes' ) {
                    window.location = wc_add_to_cart_params.cart_url;
                    return;
                } else {
                    //console.log('loading');
                    $thisbutton.find( 'i.fa' ).remove();
                    var fragments = response.fragments;
                    var cart_hash = response.cart_hash;
                    if ( fragments ) {
                        $.each( fragments, function( key ) {
                            //console.log(key);
                            $( key ).addClass( 'updating' );
                        });
                    }
                    $( '.shop_table.cart, .updating, .cart_totals' ).fadeTo( '400', '0.6' ).block({
                        message: null,
                        overlayCSS: {
                            opacity: 0.6
                        }
                    });
                    //$thisbutton.addClass( 'added' );
                    if ( fragments ) {
                        $.each( fragments, function( key, value ) {
                            $( key ).replaceWith( value );
                        });
                    }
                    if( $($thisbutton).hasClass('variable-btn') === true ) {
                        var jsonresponse = JSON.parse(response);
                        /*console.log($thisbutton.html());
                        console.log($thisbutton.parent('div.buttons-wrap').html());*/
                        if( jsonresponse.success === true ) {
                            var woo_message = jQuery($thisbutton).closest('tr').find('.custom-woo-message');
                            woo_message.html('<div>'+jsonresponse.message+'</div><a href="#" class="msg-close"></a>').show();
                            jQuery('.top-num .cart-content').html(jsonresponse.cart_num);
                            jQuery('.top-qty').show();

                            jQuery('a.msg-close').on('click', function(e) {
                                e.preventDefault();
                                woo_message.html('').hide();
                            });
                            setInterval(function(){
                                woo_message.html('').hide();
                            },2000);                            

                            /*$('li.shopping a.shoping span').html('');
                            if( $('li.shopping a.shoping span').length > 0 )
                                $('li.shopping a.shoping span').html(jsonresponse.cart_num);
                            else {
                                $('<span>'+jsonresponse.cart_num+'</span>').appendTo('li.shopping a.shoping');
                            }
                            $('<div class="cart-msg">'+jsonresponse.message+'<a href="#" class="dt-close"></div>').appendTo($thisbutton.parent('div.buttons-wrap'));
                            setTimeout(hideMessage, 5000);*/
                        }
                        else {
                            
                            var jsonresponse = JSON.parse(response);
                            if( jsonresponse.error === true ) {                            
                                var woo_message = jQuery($thisbutton).closest('tr').find('.custom-woo-message');
                                woo_message.addClass('error');
                                woo_message.html('<div>'+jsonresponse.error_msg+'</div><a href="#" class="msg-close"></a>').show();

                                jQuery('a.msg-close').on('click', function(e) {
                                    e.preventDefault();
                                    woo_message.html('').hide();
                                    return false;
                                });
                                setInterval(function(){
                                    woo_message.html('').hide();
                                },2000);
                            }
                        }
                    }
                    else {
                        if( response.success === true ) {

                            var woo_message = jQuery($thisbutton).closest('tr').find('.custom-woo-message');
                            woo_message.html('<div>'+response.message+'</div><a href="#" class="msg-close"></a>').show();
                            jQuery('.top-num .cart-content').html(response.cart_num);
                            jQuery('.top-qty').show();

                            jQuery('a.msg-close').on('click', function(event) {
                                event.preventDefault();
                                woo_message.html('').hide();
                                return false;
                            });
                            setInterval(function(){
                                woo_message.html('').hide();
                            },2000);
                        }
                        else {
                            var jsonresponse = JSON.parse(response);
                            if( jsonresponse.error === true ) { //console.log(' sinle response error');
    
                                var woo_message = jQuery($thisbutton).closest('tr').find('.custom-woo-message');
                                woo_message.addClass('error');
                                woo_message.html('<div>'+jsonresponse.error_msg+'</div><a href="#" class="msg-close"></a>').show();

                                jQuery('a.msg-close').on('click', function(e) {
                                    e.preventDefault();
                                    woo_message.html('').hide();
                                });
                                setInterval(function(){
                                    woo_message.html('').hide();
                                },2000);
                            }
                        }
                        
                    }
                    //$($thisbutton).appendTo('.shoping');
                    $( '.widget_shopping_cart, .updating' ).stop( true ).css( 'opacity', '1' ).unblock();

                    $( '.shop_table.cart' ).load( this_page + ' .shop_table.cart:eq(0) > *', function() {
                        $( '.shop_table.cart' ).stop( true ).css( 'opacity', '1' ).unblock();
                        $( document.body ).trigger( 'cart_page_refreshed' );
                    });
                    $( '.cart_totals' ).load( this_page + ' .cart_totals:eq(0) > *', function() {
                        $( '.cart_totals' ).stop( true ).css( 'opacity', '1' ).unblock();
                    });

                }
            });
            return false;
        } else {
            return true;
        }
    });
    
    jQuery('.return-top-prod-list .continue-shopping').clone().appendTo('.cart-collaterals.custom');

    /* Update qty when changed */
    jQuery( '.cart.custom-cart .boxqty-quantity input[name=boxqty]' ).live( 'change paste keyup', function() {
       
        var boxqty = jQuery(this).val();
        jQuery(this).closest( 'td' ).find('.quantity input.qty').val(boxqty);
    } );

});