<?php
/**
 * The template for displaying all pages.
 *
 * @link https://livecomposerplugin.com/themes/
 *
 * @package LC Blank
 */
/* Session for return product page after adding new list */
if( $_SERVER['REDIRECT_URL'] == '/my-lists/create-a-list/' && strpos($_SERVER['REDIRECT_QUERY_STRING'],'wl_return_to') !== false )
{
	$_SESSION['shop_page'] = $_SERVER['HTTP_REFERER'];
}
//$wp_session = WP_Session::get_instance();

get_header(); ?>
    <div class="header-section-inner">
		<?php
    	do_action('woo_custom_breadcrumb');
    ?>
	</div>
	<div class="main-page-title">
		<h1 class="main_title"><?php the_title(); ?></h1>
	</div>
	
	<div class="entry-content general-cms-page">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<?php the_content(); ?>
			<?php
			if(is_page( 'free-voucher' ) || is_page( 'in-the-trade' ) ) {
				?>
				<a href="<?php echo get_field( 'apply_now_link', 'options' ); ?>" class="btn normal"><?php echo get_field( 'apply_now_text', 'options' ); ?></a>
				<?php
			}
			?>

		<?php endwhile; ?>
		<?php endif; ?>
		<?php
		$page_image = get_field( 'page_image' );
		if( $page_image ) {
			?>
			<div class="page-image">
				<div class="page-image-img">
					<img src="<?php echo $page_image['url']; ?>" alt="<?php echo $page_image['title']; ?>">
				</div>
			</div>
			<?php
		}
		?>
	</div>

<?php get_footer(); ?>
