<?php
/**
 *	Template Name: Homepage
 */
get_header();
?>
<div class="featured-products homepage-section">
<div class="container">
	<div><h2>Featured Products</h2></div>
	<?php
	    echo do_shortcode('[featured_products per_page="12" columns="6"]'); 
	?>
	</div>
</div>
<?php
$voucher_image = get_field('voucher_image');
$voucher_text = get_field('voucher_text');
$voucher_link = get_field('voucher_link');
$newsletter_image = get_field('newsletter_image');
$newsletter_text = get_field('newsletter_text');
$intrade_image = get_field('intrade_image');
$intrade_text = get_field('intrade_text');
$intrade_link = get_field('in_trade_link');
?>
<div class="homepage-three-blocks">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-4 col-xs-12 homepage-section link">
			<?php
			if($voucher_link!="") {
				?>
				<a href="<?php echo $voucher_link; ?>">
				<?php
			}
			?>
				<div class="three-block-image"><img src="<?php echo $voucher_image['url']; ?>"></div>
				<div class="overlay-content">
					<h3><?php echo $voucher_text; ?></h3>
				</div>
			<?php
			if($voucher_link!="") {
				?>
				</a>
				<?php
			}
			?>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-12 homepage-section">
				<a href="#download-catalogue-popup" class="df-newsletter">
					<div class="three-block-image"><img src="<?php echo $newsletter_image['url']; ?>"></div>
					<div class="overlay-content">
						<h3><?php echo $newsletter_text; ?></h3>
					</div>
				</a>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-12 homepage-section link">
			<?php
			if($intrade_link!="") {
				?>
				<a href="<?php echo $intrade_link; ?>">
				<?php
			}
			?>
				<div class="three-block-image"><img src="<?php echo $intrade_image['url']; ?>"></div>
				<div class="overlay-content">
					<h3><?php echo $intrade_text; ?></h3>
				</div>
			<?php
			if($intrade_link!="") {
				?>
				</a>
				<?php
			}
			?>
			</div>
		</div>
	</div>
</div>
<?php
$download_image = get_field('homepage_download_image',10);
$download_text = get_field('homepage_download_text',10);
$download_pdf = get_field('download_link',10);
if ( is_user_logged_in() ) {
		$download_html = '<a class="download-now" href="'.$download_pdf['url'].'" target="_blank">Download Now</a>';
}
else {
	$download_html = '<a href="#download-catalogue-popup" class="download-pdf df-newsletter">Download Now</a>';
}
?>
<div class="homepage-download homepage-section">
	<div class="container">
		<div class="download">
			<div class="download-image"><img src="<?php echo $download_image['url']; ?>"></div>
			<div class="download-content">
			 	<span><?php echo $download_text; ?></span>
                <?php echo $download_html; ?>
			</div>
		</div>
	</div>
</div>
<div class="homepage-four-blocks">
	<div class="container">
		<div class="row">
			<?php
				$homepage_four_blocks = get_field('homepage_four_blocks', 10);
				$four_section_id = 1;
				if($homepage_four_blocks) {
					foreach($homepage_four_blocks as $homepage_four_blocks_row) {
						$background_image = $homepage_four_blocks_row['background_image'];
						$icon = $homepage_four_blocks_row['icon'];
						$title = $homepage_four_blocks_row['title'];
						$description = $homepage_four_blocks_row['description'];
						$desc_id = 'four-section-'.$four_section_id;
						if( $four_section_id%2 == 1 )
							$tooltip_id = 't-left';
						else
							$tooltip_id = 't-right';
						?>
						<div class="four-blocks-wrapper col-md-6 col-sm-6 col-xs-12 tooltip <?php echo $tooltip_id; ?>" data-tooltip-content="#<?php echo $desc_id; ?>" data-target="#<?php echo $desc_id; ?>">
							<a data-click-id="#<?php echo $desc_id; ?>" class="four-block-link">

								<div class="four-blocks-image"><img src="<?php echo $background_image['url']; ?> " title="<?php echo $title; ?>"></div>
								<div class="four-blocks-inner">
									<div>
										<div class="logo">
											<div class="logo-inner">
												<!-- <div class="logo-img"> -->
													<img src="<?php echo $icon['url']; ?>" alt="<?php echo $title; ?>">
												<!-- </div> -->
											</div>
										</div>
										<div class="four-blocks-description">
											<h3><?php echo $title; ?></h3>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div id="<?php echo $desc_id; ?>" class="block-info white-popup-block mfp-hide" style="background-image: url(<?php echo $background_image['url']; ?>);">			
						    <!-- <div class="modal-header">
						      <h4 class="modal-title"><?php echo $title; ?></h4>
						    </div> -->
						    <div class="modal-body">
						    	<h4 class="modal-title"><?php echo $title; ?></h4>
						    	<?php echo $description; ?>
						    </div>
						</div>
						<?php
						$four_section_id++;
					}
				}
			?>			
		</div>
	</div>
</div>
<div class="homepage-brands homepage-section">
	<div class="container">
		<div class="row">
			<?php
				$brands = get_field('brands', 10);
				if($brands) {
					$brand_count = 1;
					foreach($brands as $brands_row) {
						$brands_image = $brands_row['brand_image'];
						
						?>
						<?php
						if( $brand_count == 1 || $brand_count % 5 == 0 )
							echo '<div class="col-md-6 col-sm-6 col-xs-12">';

						echo '<div class="col-md-3 col-sm-3 col-xs-3"><div class="brand-image"><img src="'. $brands_image['url'].'" title="'.$brands_image['title'].'"></div></div>';

						if( $brand_count % 4 == 0 || $brand_count % 8 == 0 )
							echo '</div>'; 
						$brand_count++;
					}
				}
			?>
		</div>
	</div>
</div>
<div id="download-catalogue-popup" class="white-popup-block mfp-hide">
	<div class="envelop"><img src="<?php echo get_template_directory_uri(); ?>/images/envelop-icon.jpg" title="envelop" /></div>
          <h4 class="modal-title">Sign up to our newsletter for all the latest tips, tricks and special offers</h4>
    <?php echo do_shortcode('[gravityform id="4" ajax="true" title="false" description="false"]'); ?>
</div>
<script type="text/javascript">
	cc_delete_cookie('download-pdf');
</script>
<?php get_footer(); ?>