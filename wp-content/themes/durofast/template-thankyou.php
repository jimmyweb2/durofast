<?php
/**
 *	Template Name: Thank you download
 */
get_header(); ?>
    <div class="header-section-inner">
		<?php
    	do_action('woo_custom_breadcrumb');
    ?>
	</div>
	<div class="main-page-title">
		<h1 class="main_title"><?php the_title(); ?></h1>
	</div>
	
	<div class="entry-content">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<?php the_content(); ?>
			<?php
			$download_text = get_field('homepage_download_text',10);
			$download_pdf = get_field('download_link',10);
			?>
			<div class="download-pdf-link hide"><a href="<?php echo $download_pdf['url']; ?>"><?php echo $download_text; ?></a></div>

		<?php endwhile; ?>
		<?php endif; ?>
	</div>
<?php get_footer(); ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		var catalogue = ccgetCookie('download-pdf');
		
	    if ( catalogue !== undefined && catalogue !== null && catalogue == 'yes' ) {
	        jQuery('.download-pdf-link').addClass('show');
	        jQuery('.download-pdf-link').removeClass('hide');
	        
	    }
	});
</script>
