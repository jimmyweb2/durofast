<?php
/**
 * The template for displaying search results pages.
 *
 *
 * @package LC Blank
 */

get_header(); ?>
    <div class="header-section-inner">
		<?php
    	do_action('woo_custom_breadcrumb');
    ?>
	</div>
	
	<div class="entry-content search-result">
		<?php
		if ( have_posts() ) :
			/* Start the Loop */
		?>
		<div class="row">
		<?php
			while ( have_posts() ) : the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'content', 'search' );

			endwhile; // End of the loop.

			/*the_posts_pagination( array(
				'prev_text' => twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '<span class="screen-reader-text">' . __( 'Previous page', 'twentyseventeen' ) . '</span>',
				'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'twentyseventeen' ) . '</span>' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyseventeen' ) . ' </span>',
			) );*/
			?>
			</div>
			<div class="nav-pagination">
				<?php
				$per_page_count = 20;
				$curr_post_count  = $GLOBALS['wp_query']->post_count;
				$total_count = $GLOBALS['wp_query']->found_posts;
				if( $total_count > 20 ) {
					$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
					$start = $per_page_count * ($paged-1) + 1;
					$end = ($per_page_count * ($paged - 1)) + $curr_post_count;
					echo '<div class="pagination-result-section"><p>Showing '.$start.'–'.$end.' of '.$total_count.' results</p></div>';
				}
				// Previous/next page navigation.
				the_posts_pagination( array(
					'mid_size' => 4,
					'prev_text'          => __( '<i class="fa fa-caret-left"></i>', 'durofast' ),
					'next_text'          => __( '<i class="fa fa-caret-right"></i>', 'durofast' ),
					'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'durofast' ) . ' </span>',
				) );
				?>
			</div>
			<?php

		else : ?>

			<?php
				echo get_field( 'search_result_text', 'options' );
				get_search_form();

		endif;
		?>
	</div>

<?php get_footer(); ?>