<?php
/**
 *  Template Name: Contact Us
 */
get_header();
?>
    <div class="header-section-inner">
    <?php
      do_action('woo_custom_breadcrumb');
    ?>
  </div>
  <div class="main-page-title">
    <h1 class="main_title"><?php the_title(); ?></h1>
  </div>
  
  <div class="entry-content general-cms-page">
    <div class="row">
      <div class="col-md-1 col-md-1 df-hide-mobile">&nbsp;</div>
      <div class="col-md-5 col-sm-6 col-xs-12">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

        <?php the_content(); ?>
        <?php echo do_shortcode('[gravityform id="1" ajax="true" title="false" description="false"]'); ?>

        <?php endwhile; ?>
        <?php endif; ?>
      </div>
      
      <div class="col-md-5 col-sm-6 col-xs-12 contact-right">
        <?php echo $about_cotent = get_field('contact_info'); ?>
        <?php
        $page_image = get_field( 'page_image' );
        ?>
        <div class="contact-page-image">
          <img src="<?php echo $page_image['url']; ?>" alt="<?php echo $page_image['title']; ?>">
        </div>
      </div>
      <div class="col-md-1 col-md-1 df-hide-mobile">&nbsp;</div>
      <!-- <div class="col-md-1 col-md-1 df-hide-mobile">&nbsp;</div> -->
    </div>
  </div>

<?php get_footer(); ?>