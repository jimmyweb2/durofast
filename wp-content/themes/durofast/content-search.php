<?php
/**
 * Template part for displaying posts
 *
 */
$images = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
//var_dump($images);
?>
<div class="col-md-3 col-sm-3 col-xs-6 search-row">
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="prod-img">
    	<?php
    	if($images)
    		$image = $images[0];
    	else
    		$image = get_template_directory_uri().'/images/no-image.png';

    	//echo $image;
    	?>
    	<a href="<?php the_permalink(); ?>"><img src="<?php echo $image; ?>" alt="<?php echo get_the_title(); ?>"></a>
    </div>
	<header class="entry-header search-header">
		<?php

			if ( is_single() ) {
				the_title( '<h1 class="entry-title">', '</h1>' );
			} else {
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			}
		?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<p>
			<?php
				/* translators: %s: Name of current post */
				//echo get_excerpt_by_word($post->post_content, 30);
			?>
			<a href="<?php the_permalink(); ?>" class="search-btn">View</a>
		</p>
	</div><!-- .entry-content -->

</article><!-- #post-## -->
</div>
