<?php
/**
 * The template for displaying all single posts.
 */

get_header(); ?>
<div class="header-section-inner">
	<?php
	do_action('woo_custom_breadcrumb');
?>
</div>
<div class="main-page-title">
	<h1 class="main_title"><?php the_title(); ?></h1>
</div>
<div class="row-section latest-news">
    <div class="row">
        <div class="col-md-9 col-sm-9">
            <?php
            while (have_posts()) : the_post();
                get_template_part( 'content', get_post_format() );
            endwhile;
            ?>
            <div class="latest-news-navigation">
                <div class="single-prev nav-link">
                    <?php previous_post_link('%link', 'Previous', TRUE); ?>
                </div>    
                <div class="single-next nav-link">
                    <?php next_post_link('%link', 'Next', TRUE); ?>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="col-md-3 col-sm-3 df-sidebar">
            <?php get_sidebar(); ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
