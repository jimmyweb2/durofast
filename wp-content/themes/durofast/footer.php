<?php
/**
 * The template for displaying the footer.
 *
 * @link https://livecomposerplugin.com/themes/
 *
 * @package LC Blank
 */

?>
<?php
if(!is_front_page()){
	?>
		</div>
		<!-- Content-area -->
	</div>
	<!-- Container -->
	<?php
}
?>
	</div>
	<!-- Main contenter -->
</div>
<!-- End Main Content -->
<footer id="footer">
	<?php
	if ( is_page_template( 'template-contact.php' ) ) {
		?>
		<div class="contact-map">
			<?php echo get_field('map'); ?>
		</div>
		<?php
	}
	?>
	<div class="container">
		<?php
		$facebook_link = get_field('facebook_link','options');
		$blog_link = get_field('twitter_link','options');		
		$insta_link = get_field('instagram_link','options');
		$linked_link = get_field('linkedin_link','options');
		?>
		<div class="footer-mid">
			<div class="social-icons">
				<ul>
					<li><a href="<?php echo $facebook_link; ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a href="<?php echo $insta_link; ?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
					<li><a href="<?php echo $blog_link; ?>" target="_blank"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
					<li><a href="<?php echo $linked_link; ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
				</ul>
			</div>
			<div class="footer-links">
                <div class="menu-footer-menu-container">
				<?php
					wp_nav_menu(array(
						'theme_location' => 'footer-bottom',
                        'container' => false,
                        'menu_class'=> 'menu-footer-menu'
                    ));
                    wp_nav_menu(array(
                        'theme_location' => 'footer-bottom-2',
                        'container' => false,
                        'menu_class'=> 'menu-footer-menu'
                    ));
				?>
                </div>
			</div>
		</div>
        <hr>
		<div class="footer-bottom">
			<div class="row">
				<div class="footer-logo footer-btm-left col-md-4 col-sm-4 col-xs-12"><img src="<?php echo get_template_directory_uri(); ?>/images/durofast-logo.png" title="durofast logo"></a></div>
				<div class="footer-btm-right col-md-8 col-sm-8 col-xs-12">
					<div class="payment-types">
						<!-- <ul>
							<li class="paypal">paypal</li>
							<li class="mastercard">mastercard</li>
							<li class="visa">visa</li>
						</ul> -->
						<!-- Begin eWAY Linking Code -->
						<div id="eWAYBlock">
						    <div style="text-align:center;">
						        <a href="http://www.eway.com.au/secure-site-seal?i=12&s=3&pid=7b42a6c8-cf22-476c-8b19-4341400edc47&theme=1" title="eWAY Payment Gateway" target="_blank" rel="nofollow">
						            <img alt="eWAY Payment Gateway" src="https://www.eway.com.au/developer/payment-code/verified-seal.ashx?img=12&size=3&pid=7b42a6c8-cf22-476c-8b19-4341400edc47&theme=1" />
						        </a>
						    </div>
						</div>
						<!-- End eWAY Linking Code -->
					</div>
					<div class="copy-rights">
						<?php echo get_field('copyright','options'); ?>
					</div>
				</div>
				<div class="credit col-md-12 col-sm-12 col-xs-12">
					<div class="abn-content">
						<span>ABN <?php echo get_field( 'abn_number', 'options' ); ?></span>
					</div>
					<p>Website Design by <a href="http://www.jimmyweb.net/" target="_blank">Jimmyweb</a></p>
				</div>
			</div>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>
<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <div class="envelop"><img src="<?php echo get_template_directory_uri(); ?>/images/envelop-icon.jpg" title="envelop" /></div>
          <h4 class="modal-title">Sign up to our newsletter for all the latest tips, tricks and special offers</h4>
        </div>
        <div class="modal-body">
        	<form id="newsletter-subscription" data-toggle="validator" role="form">
	          <div class="row">
	            <div class="col-md-6">
	            	<div class="form-group">
	            		<input type="text" name="firstname" placeholder="First Name" class="form-control" required>
	            	</div>	            	
	            </div>
	            <div class="col-md-6">
	            	<div class="form-group">
	            		<input type="text" name="lastname" placeholder="Last Name" class="form-control" required>
	            	</div>	            	
	            </div>	            
	          </div>
	          <div class="row">
	            <div class="col-md-12">
	            	<div class="form-group">
	            		<input type="email" name="email" placeholder="Email Address" class="form-control" required>
	            	</div>	          		
	          	</div>
	          </div>
	          <div class="row">
	          	<div class="col-md-12">
	          		<div class="form-group">
	          			<button type="submit" class="btn">Sign Up Now</button>
	          		</div>	          		
	          	</div>	          	
	          </div>
	        </form>
        </div>
      </div>
      
    </div>
  </div>
  <?php if(is_cart()) {
 	echo '<input type="hidden" name="refresh-img" id="refresh-img" value="'.get_template_directory_uri().'/images/refresh-btn-icon.png">'; 	
  	} 
  	if(is_checkout()) {
  		echo '<div class="shipping-address-info">'.get_field( 'delivery_information', 'options' ).'</div>';
  	}
  	?>
  	<?php
  	if( is_checkout() || is_account_page() ) {
  	?>
  	<script type="text/javascript">
  		jQuery(document).ready( function() {
  			jQuery( '.df-tooltip.checkout' ).tooltipster( {
  				theme: ['tooltipster-noir', 'tooltipster-noir-customized-simple'],
                animation: 'fade',
                delay: 300,
                //trigger: 'click',
                side:    'right',
  			});
  			jQuery( '.df-tooltip.reg' ).tooltipster( {
  				theme: ['tooltipster-noir', 'tooltipster-noir-customized-reg'],
                animation: 'fade',
                delay: 300,
                //trigger: 'click',
  			});
  		});
  	</script>
  	<?php
  	}
  	?>
  	<?php //echo get_num_queries().' queries in'.timer_stop(1).' seconds.'; ?>
</body>
</html>
