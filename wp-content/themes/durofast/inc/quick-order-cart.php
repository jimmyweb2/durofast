<div class="quick-order-error duro-hidden"></div>
<form action="<?php echo get_permalink($post->ID); ?>" method="post" class="woocommerce-cart-form">
	<table class="shop_table shop_table_responsive cart custom-cart" cellspacing="0">
		<thead>
			<tr>
				<th class="product-thumbnail"></th>
				<th class="product-price"><?php _e( 'Product Code', 'woocommerce' ); ?></th>
				<th class="product-name"><?php _e( 'Product', 'woocommerce' ); ?></th>
				<th class="product-name"><?php _e( 'Box Qty', 'woocommerce' ); ?></th>
				<th class="product-price">Unit Price</th>
				<th class="product-quantity"><?php _e( 'Select Box/es', 'woocommerce' ); ?></th>
				<th class="product-total">Total</th>	
				<th class="product-remove"></th>
			</tr>
		</thead>
		<tbody>
		<?php
		if ( WC()->cart->get_cart_contents_count() == 0 ) {
			$display_update_btn_cls = 'hide-cart-update-btn';
			?>
			<tr class="no-cart-item">
				<td colspan="8" class="quick-no-order"><p class="no-cart-item">There are no items in cart!</p></td>
			</tr>
			<?php
		}
		else {
			$display_update_btn_cls = '';
			?>

			<?php do_action( 'woocommerce_before_cart_contents' ); ?>

			<?php
			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
				$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
					$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
					?>
					<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">


						<td class="product-thumbnail">
							<div class="prod-img">
							<?php
								$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

								if ( ! $product_permalink ) {
									echo $thumbnail;
								} else {
									printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail );
								}
							?>
							</div>
						</td>

						<td data-title="Order Number">
							<?php echo $_product->get_sku(); ?>
						</td>

						<td class="product-name" data-title="<?php _e( 'Product', 'woocommerce' ); ?>">
							<?php
								if ( ! $product_permalink ) {
									echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) . '&nbsp;';
								} else {
									echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_title() ), $cart_item, $cart_item_key );
								}

								// Meta data
								echo WC()->cart->get_item_data( $cart_item );

								// Backorder notification
								if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
									echo '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>';
								}
							?>
						</td>
						<td data-title="Box Qty">
							<?php
							$prod_boxqty = $_product->get_attribute( 'pa_boxqty' );

							if( $prod_boxqty != "" ) {
								$boxqty_c = $prod_boxqty;
								echo $boxqty_c;
							}
							else {
								$boxqty_c = 1;
								echo $boxqty_c;
							}
							/*$prod_variations = $_product->get_attributes();
							
							if( $prod_variations ) {
								foreach( $prod_variations as $key => $val ) {
									//echo $val->'pa_boxqty';
									if( $key == 'pa_boxqty' )
										echo $val;
								}
								
							}
							else
								echo '-';*/
							?>
						</td>
						<td class="product-price" data-title="<?php _e( 'Unit Price', 'woocommerce' ); ?>">
							<?php
							$v_price = $_product->get_price_html();
							echo $p_price = calculate_price_by_boxqty( $boxqty_c, $v_price );
								//echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
							?>
						</td>
						<td class="product-quantity" data-title="<?php _e( 'Select Box/es', 'woocommerce' ); ?>">
							<?php
								/*if ( $_product->is_sold_individually() ) {
									$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
								} else {
									$product_quantity = woocommerce_quantity_input( array(
										'input_name'  => "cart[{$cart_item_key}][qty]",
										'input_value' => $cart_item['quantity'],
										'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
										'min_value'   => '0'
									), $_product, false );
								}

								echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );*/
								//echo $cart_item['quantity'];
								/*print_r("<pre style='margin-left:50px'>");
					            print_r($cart_item['data']->manage_stock);
					            print_r("</pre>");*/

								if ( $_product->is_sold_individually() ) {
									$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
								} else {
									$product_quantity = woocommerce_quantity_input( array(
										'input_name'  => "cart[{$cart_item_key}][qty]",
										'input_value' => $cart_item['quantity'],
										'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
										'min_value'   => '0'
									), $_product, false );
								}

								echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
								
							?>
							<div class="boxqty-quantity">
								<?php 
								$cart_qty = $cart_item['quantity']/$boxqty_c;
								?>
								<input type="number" name="boxqty" value="<?php echo $cart_qty; ?>">
							</div>
						</td>	
						<td class="product-subtotal" data-title="<?php _e( 'Total', 'woocommerce' ); ?>">
							<?php
								echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
							?>
						</td>			
						<?php 
							/*print_r("<pre style='margin-left:50px'>");
					            print_r($cart_item['variation_id']);
					            print_r("</pre>");*/
					            $stock = get_post_meta( $cart_item['variation_id'], '_stock_status', true );
					            if($stock=='instock') {
					            	$stock_level = 'In Stock';
					            	$stock_class = 'df-instock';
					            }
					            else if($stock=='outofstock') {
					            	$stock_level = 'Out of Stock';
					            	$stock_class = 'df-outofstock';
					            }

							//echo $cart_item['data']; ?>
						<td class="product-remove" data-title="Remove">
							<?php
								echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
									'<a href="%s" class="remove" title="%s" data-product_id="%s" data-product_sku="%s">×</a>',
									esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
									__( 'Remove this item', 'woocommerce' ),
									esc_attr( $product_id ),
									esc_attr( $_product->get_sku() )
								), $cart_item_key );
							?>
						</td>
					</tr>
					<?php
				}
			}

			do_action( 'woocommerce_cart_contents' );
			?>
			<?php do_action( 'woocommerce_after_cart_contents' ); ?>
			<?php
				}
			?>
			<tr class="custom-cart-update">
				<td colspan="8" class="quick-order-cart-update <?php echo $display_update_btn_cls; ?>">
					<!-- <input type="image" src="<?php echo get_template_directory_uri(); ?>/images/refresh-btn-icon.png" class="button" name="update_cart" value="<?php esc_attr_e( 'Update Cart', 'woocommerce' ); ?>" style="padding:0;" alt="submit" /> -->
					<button type="submit" class="button df-cart" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>"><?php esc_html_e( 'Update cart', 'woocommerce' ); ?></button>

						<?php //do_action( 'woocommerce_cart_actions' ); ?>

						<?php //wp_nonce_field( 'woocommerce-cart' ); ?>
						<?php 
						//wp_nonce_field( -1, 'woocommerce-cart', false, true ); //previous suggest
						wp_nonce_field( 'woocommerce-cart', '_wpnonce', false, true ); 
						?>
						<input type="hidden" name="_wp_http_referer" value="/quick-order-2/">
				</td>
			</tr>

		</tbody>
	</table>
</form>
<div class="hidden-cart-update-btn">
	<tr>
		<td colspan="8" class="quick-order-cart-update">
			<button type="submit" class="button df-cart" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>"><?php esc_html_e( 'Update cart', 'woocommerce' ); ?></button>

				<?php //do_action( 'woocommerce_cart_actions' ); ?>

				<?php //wp_nonce_field( 'woocommerce-cart' ); ?>
				<?php 
				//wp_nonce_field( -1, 'woocommerce-cart', false, true ); //previous suggest
				wp_nonce_field( 'woocommerce-cart', '_wpnonce', false, true ); 
				?>
				<input type="hidden" name="_wp_http_referer" value="/quick-order-2/">
		</td>
	</tr>
</div>