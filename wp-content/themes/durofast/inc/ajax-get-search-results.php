<?php
include('../../../../wp-config.php');
$term = trim($_GET['term']);
global $myterm;
$myterm = $term;
$args = array(
	'post_status'    => 'publish',
	'post_type'      => 'product',
	'posts_per_page' => 100,
    'meta_key'       => '_sku',
    'meta_value'     => $term
);

add_filter( 'posts_where' , 'product_search_sku_custom_s' );
$query = new WP_Query( $args );
$result = array();
$results = array();
//$query = $query->posts;
//echo count($query);
//var_dump($query->request);
if ( $query->have_posts() ) {
	while ( $query->have_posts() ) {
		$query->the_post();
		$prod_info = '';
		$prod_id = $query->post->ID;
		$product_title = get_the_title();
		if ( $product->is_type( 'variable' ) ) {
			//echo 'variable';	
			$prod_variables = new WC_Product_Variable( $prod_id );
            $variables = $prod_variables->get_available_variations();
            //var_dump($variables);
            foreach( $variables as $variables_prod ) {
                $sku = $variables_prod['sku'];
                if($sku==$term) {
                    $results = array();
                //if( strpos( $sku, $term ) !== false ) {
                	$prod_info = '';
                	//echo 'Hello';
                	$var_array = array();
                	//$prod_id_variable = $variables_prod['variation_id'];
                	$sku = $variables_prod['sku'];
                	//$prod_info .= $sku.' ';
                	//$prod_info .= '200 ';
                	//$attributes = $variables_prod['attributes'];
                	/*$attributes_val = '';
                	$attributes_val_name = '';
                	foreach ($attributes as $key => $value) {
                		$attributes_val .= $value.' ';
                		$attributes_val_name .= $key;
                	}
                	$attributes_val = trim($attributes_val);*/
                	//$prod_info .= $attributes_val.' ';
                	$prod_info .= $product_title;
                	//$result[$prod_id_variable] = $prod_info;

                	/*$var_array['prod_id'] = $prod_id;
                	$var_array['var_id'] = $prod_id_variable;
                	$var_array['var_attribute'] = $attributes_val_name;
                	$var_array['var_attribute_val'] = $attributes_val;*/
                	$var_array['value'] = $prod_info;
                	/*$var_array['label'] = $prod_id_variable;
                	$var_array['value'] = $prod_info;*/
                	$results[] = $var_array;
                    break;
                }
                else
                {
                    $prod_info = '';
                    //echo 'Hello';
                    /*$var_array = array();
                    $prod_id_variable = $variables_prod['variation_id'];
                    $sku = $variables_prod['sku'];
                    $prod_info .= $sku.' ';*/
                    //$prod_info .= '200 ';
                    /*$attributes = $variables_prod['attributes'];
                    $attributes_val = '';
                    $attributes_val_name = '';
                    foreach ($attributes as $key => $value) {
                        $attributes_val .= $value.' ';
                        $attributes_val_name .= $key;
                    }
                    $attributes_val = trim($attributes_val);
                    $prod_info .= $attributes_val.' ';*/
                    $prod_info .= $product_title;
                    //$result[$prod_id_variable] = $prod_info;

                    /*$var_array['prod_id'] = $prod_id;
                    $var_array['var_id'] = $prod_id_variable;
                    $var_array['var_attribute'] = $attributes_val_name;
                    $var_array['var_attribute_val'] = $attributes_val;*/
                    $var_array['value'] = $prod_info;
                    
                    $results[] = $var_array;
                }
                //}
            }
		}
		else {
			//echo '<li>' . get_the_title() . '</li>';	
			//echo 'Single';
			$var_array = array();		
			$sku = get_post_meta($prod_id, '_sku', true );
			//$prod_info .= $sku.' ';
            //$prod_info .= '200 ';
            //$prod_info = 'attribute ';
            $prod_info .= $product_title;
			/*$meta = get_post_meta($prod_id);
			echo $meta->sku;
			var_dump($meta['_sku'][0]);
			var_dump($meta);
			echo '<br>Next<br>';*/
			/*$result[$prod_id] = $prod_info;

        	$var_array['prod_id'] = $prod_id;
        	$var_array['var_id'] = 'no';
        	$var_array['var_attribute'] = 'no';
            $var_array['var_attribute_val'] = 'no';*/
        	$var_array['value'] = $prod_info;
        	/*$var_array['label'] = $prod_id;
            $var_array['value'] = $prod_info;*/
        	$results[] = $var_array;
		}
		
		
	}
	//echo '</ul>';
	// Restore original Post Data
	wp_reset_postdata();
	
} else {
	// no posts found
	//$result['no-match'] = 'No Match Found';
	//$results[] = '0';
}
//var_dump($results);
//var_dump(json_encode($results));
echo json_encode($results);


/* Search by SKU */
function product_search_sku_custom_s($where) {
    global $wpdb, $wp, $myterm;
    //VAR_DUMP(http_build_query(array('post_type' => array('product','test'))));die();
    $search_ids = array();
    $terms = explode(' ', $myterm);
    $search_terms_where = '(';
    $search_terms_where_option = 'no';
    foreach ($terms as $term) {
        //Include the search by id if admin area.
        if (is_admin() && is_numeric($term)) {
            $search_ids[] = $term;
        }
        // search for variations with a matching sku and return the parent.

        $sku_to_parent_id = $wpdb->get_col($wpdb->prepare("SELECT p.post_parent as post_id FROM {$wpdb->posts} as p join {$wpdb->postmeta} pm on p.ID = pm.post_id and pm.meta_key='_sku' and pm.meta_value LIKE '%%%s%%' where p.post_parent <> 0 group by p.post_parent", wc_clean($term)));

        //Search for a regular product that matches the sku.
        $sku_to_id = $wpdb->get_col($wpdb->prepare("SELECT post_id FROM {$wpdb->postmeta} WHERE meta_key='_sku' AND meta_value LIKE '%%%s%%';", wc_clean($term)));

        $search_ids = array_merge($search_ids, $sku_to_id, $sku_to_parent_id);
        $search_terms_where .= " post_title like '%".$term."%' OR post_content like '%".$term."%' OR";
        $search_terms_where_option = 'yes';
    }
    $search_terms_where = trim($search_terms_where,'OR');
    $search_terms_where .= ') OR ( wpdufa_postmeta';
    //echo $search_terms_where;
    $search_ids = array_filter(array_map('absint', $search_ids));

    if (sizeof($search_ids) > 0) {

        $where = str_replace('))', ") OR ({$wpdb->posts}.ID IN (" . implode(',', $search_ids) . ")))", $where);
    }

    if($search_terms_where_option=='yesa') {
        $where = str_replace("post_title like '%".$myterm."%' OR post_content like '%".$myterm."%' OR ( wpdufa_postmeta.meta_key", $search_terms_where, $where);
    }
    
    remove_filters_for_anonymous_class('posts_search', 'WC_Admin_Post_Types', 'product_search', 10);
    //echo $where;
    return $where;
}
/* End for Search by SKU */