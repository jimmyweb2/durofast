<?php
include('../../../../wp-config.php');

$term = trim($_GET['sterm']);
if( $term== "" )
	return;

/*global $myterm;
$myterm = $term;*/
/*$s = trim($_GET['s']);
echo $s;*/
$results = array();
$args = array(
                's' => $term,
            );
    // The Query
$the_query = new WP_Query( $args );
if ( $the_query->have_posts() ) {
	$count = 1;
	while ( $the_query->have_posts() ) {
		$the_query->the_post();
		//echo 'maya: '.$count.' <br>';
		$prod_title = get_the_title();
		$var_array = array();
		$var_array['value'] = $prod_title;
		$results[] = $var_array;
		$count++;
	}
	wp_reset_postdata();
}
else
{
	// Do nothing
}
echo json_encode($results);