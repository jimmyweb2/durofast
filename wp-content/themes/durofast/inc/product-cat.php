<?php
if(is_product_category()) {
	$cate = get_queried_object();
	$cateID = $cate->term_id;
	$curr_prod_cat_id = $cateID;
	$parent_cat_id = $cate->parent;
}
else {
	$curr_prod_cat_id = '';
	$parent_cat_id = '';
}
	
 $taxonomy     = 'product_cat';
  $orderby      = 'order';  
  $show_count   = 0;      // 1 for yes, 0 for no
  $pad_counts   = 0;      // 1 for yes, 0 for no
  $hierarchical = 1;      // 1 for yes, 0 for no  
  $title        = '';  
  $empty        = 1;

  $args = array(
         'taxonomy'     => $taxonomy,
         'orderby'      => $orderby,
         'show_count'   => $show_count,
         'pad_counts'   => $pad_counts,
         'hierarchical' => $hierarchical,
         'title_li'     => $title,
         'hide_empty'   => $empty
  );
 $all_categories = get_categories( $args );
 if($all_categories):
 	?>
 <div id="sidebar" role="complementary">
 	<h3 class="widget-title">Product Categories</h3>
 	<div class="panel-group product-categories" id="accordion">
	<?php
		foreach ($all_categories as $cat) {
		    if($cat->category_parent == 0) {
		        $category_id = $cat->term_id;       
		        //echo '<br /><a href="'. get_term_link($cat->slug, 'product_cat') .'">'. $cat->name .'</a>';
		        ?>
		        <div class="panel panel-default">
			      <div class="panel-heading">
			        <h4 class="panel-title">
			        <?php
			        if(strtolower($cat->name)=='clearance') {
			        	?>
			        	<a href="<?php echo get_term_link($cat->term_id); ?>" class="collapsed"><?php echo $cat->name; if($cat->count>0) echo ' <span>('.$cat->count.')</span>'; ?></a>
			        	<?php
			        }
			        else {
			        	?>
			        	<a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $cat->term_id; ?>" class="<?php if($curr_prod_cat_id==$category_id || $category_id==$parent_cat_id) echo ''; else echo 'collapsed'; ?>"><?php echo $cat->name; if($cat->count>0) echo ' <span>('.$cat->count.')</span>'; ?></a>
			        	<?php
			        }
			        ?>
			          
			        </h4>
			      </div>
			      <?php
			      	$args2 = array(
		                'taxonomy'     => $taxonomy,
		                'child_of'     => 0,
		                'parent'       => $category_id,
		                'orderby'      => $orderby,
		                'show_count'   => $show_count,
		                'pad_counts'   => $pad_counts,
		                'hierarchical' => $hierarchical,
		                'title_li'     => $title,
		                'hide_empty'   => $empty
			        );
			        $sub_cats = get_categories( $args2 );
			      
			      if(strtolower($cat->name)!='clearance') {  
			      ?>
			      <div id="collapse<?php echo $cat->term_id; ?>" class="panel-collapse collapse <?php if($curr_prod_cat_id==$category_id || $category_id==$parent_cat_id) echo 'in'; else echo ''; ?>">
			        <div class="panel-body">
			        <?php
			        if($curr_prod_cat_id==$category_id)
			        	$curr_parent_class = 'class="active-prod-cat"';
			        else
			        	$curr_parent_class = '';
			        ?>
			        	<ul>
			        	<li <?php echo $curr_parent_class; ?>><a href="<?php echo get_term_link($cat->slug, 'product_cat'); ?>">All<?php /*echo $cat->name;*/ if($cat->count>0) echo ' <span>('.$cat->count.')</span>'; ?></a></li>
			        	<?php
			        	if($sub_cats) {
				            foreach($sub_cats as $sub_category) {
				            	if($curr_prod_cat_id==$sub_category->term_id)
						        	$curr_child_class = 'class="active-prod-cat"';
						        else
						        	$curr_child_class = '';
				            	?>
				            	<li <?php echo $curr_child_class; ?>><a href="<?php echo get_term_link($sub_category->term_id); ?>"><?php echo  $sub_category->name ; echo ' <span>('.$sub_category->count.')</span>'; ?></a></li>
				            	<?php
				                
				            }   
				        }
			        	?>
			        	</ul>
			        </div>
			      </div>
			      <?php } ?>
			    </div>
		        <?php

		        
		    }       
		}
	?>
	</div>
</div>
<?php endif; ?>