<?php do_action( 'woocommerce_before_cart_contents' );
$cart_html = '';
foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
	$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
	$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

	if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
		$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
		$cart_html .= '<tr class="'.esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ).'"><td class="product-thumbnail"><div class="prod-img">';

		$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

		if ( ! $product_permalink ) {
			$cart_html .= $thumbnail;
		} else {
			$cart_html .= '<a href="'.$product_permalink.'">'.$thumbnail.'</a>';
		}
		$cart_html .= '</div></td>';
		$cart_html .= '<td data-title="Product Code">'.$_product->get_sku().'</td>';

		$cart_html .= '<td class="product-name" data-title="Product">';
		if ( ! $product_permalink ) {
			$cart_html .= apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) . '&nbsp;';
		} else {
			$cart_html .= apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_title() ), $cart_item, $cart_item_key );
		}
		$cart_html .= WC()->cart->get_item_data( $cart_item );
		if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
						$cart_html .= '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p></td>';
					}
		$prod_boxqty = $_product->get_attribute( 'pa_boxqty' );

		if( $prod_boxqty != "" ) {
			$box_qty = $prod_boxqty;
		}
		else {
			$box_qty = 1;
		}
		/*$prod_variations = $_product->get_attributes();
		
		if( $prod_variations ) {
			$box_qty = '';
			foreach( $prod_variations as $key => $val ) {
				//echo $val->'pa_boxqty';
				if( $key == 'pa_boxqty' )
					$box_qty = $val;
			}
			
		}
		else
			$box_qty = '-';*/
		
		$cart_html .= '<td data-title="Box Qty">'.$box_qty.'</td>';
		/* quantity box */
		if ( $_product->is_sold_individually() ) {
			$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
		} else {
			$product_quantity = woocommerce_quantity_input( array(
				'input_name'  => "cart[{$cart_item_key}][qty]",
				'input_value' => $cart_item['quantity'],
				'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
				'min_value'   => '0'
			), $_product, false );
		}

		$qtybox = apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );

		/* Quantity box customize */
		$cart_qty = $cart_item['quantity']/$box_qty;
		$custom_qtybox = '<div class="boxqty-quantity"><input type="number" name="boxqty" value="'.$cart_qty.'"></div>';
		/* end quantity box */
		//$cart_html .= '<td class="product-quantity" data-title="Quantity">'.$cart_item['quantity'].'</td>';
		
		$v_price = $_product->get_price_html();
		$p_price = calculate_price_by_boxqty( $box_qty, $v_price );

		$cart_html .= '<td class="product-price" data-title="Unit Price">'.$p_price.'</td>';
		$cart_html .= '<td class="product-quantity" data-title="Quantity">'.$custom_qtybox.$qtybox.'</td>';
		$cart_html .= '<td class="product-total" data-title="Total">'.apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ).'</td>';
		$cart_html .= '<td class="product-remove" data-title="Remove">'.apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
						'<a href="%s" class="remove" title="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
						esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
						__( 'Remove this item', 'woocommerce' ),
						esc_attr( $product_id ),
						esc_attr( $_product->get_sku() )
					), $cart_item_key ).'</td></tr>';
	}
}
do_action( 'woocommerce_cart_contents' );
do_action( 'woocommerce_after_cart_contents' ); ?>