<?php
/**
 *  Template Name: Help / FAQs
 */
get_header();
?>
    <div class="header-section-inner">
    <?php
      do_action('woo_custom_breadcrumb');
    ?>
  </div>
  <div class="main-page-title">
    <h1 class="main_title"><?php the_title(); ?></h1>
  </div>
  
  <div class="entry-content form-content faqs">
    
    <div class="page-inner">
      <div class="row">
        <div class="col-md-7 col-sm-12 left">
          <?php
          if( have_rows('faqs_rows') ){
            ?>
            
                <div class="panel-group" id="accordion">
                  <div class="panel panel-default">
                    <?php
                    $i = 1;
                    while ( have_rows('faqs_rows') ) {
                      the_row();
                      $question = get_sub_field('question');
                      $answer = get_sub_field('answer');
                      ?>
                      <div class="panel-heading">
                          <h4 class="panel-title">
                              <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $i; ?>" role="button" aria-expanded="true" aria-controls="collapse<?php echo $i; ?>"><?php echo $question; ?></a>
                          </h4>
                      </div>
                      <div id="collapse<?php echo $i; ?>" class="panel-collapse collapse">
                          <div class="panel-body">
                              <?php echo $answer; ?>
                          </div>
                      </div>
                      <?php
                      $i++;
                    }
                    ?>
                  </div>
                </div>
              
            <?php
          }
          ?>
        </div>
        <div class="help-form col-md-5 col-sm-12 right">
          <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

            <?php the_content(); ?>

          <?php endwhile; ?>
          <?php endif; ?>
        </div>
      </div>    
    </div>
  </div>

<?php get_footer(); ?>