<?php
/**
 *	Template Name: Quick Order
 */
get_header(); ?>
    <div class="header-section-inner">
		<?php
    	do_action('woo_custom_breadcrumb');
    ?>
		<h1 class="main_title"><?php the_title(); ?></h1>
	</div>
	
	<div class="entry-content woocommerce" id="main-quick-order">
  <?php wc_print_notices(); ?>
	<div class="row">
    <div class="col-md-9 col-sm-12 quick-order-left">
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<?php the_content(); ?>

			<?php endwhile; ?>
			<?php endif; ?>
			<div class="quick-order">
				<form method="post" class="form-addtocart" enctype="multipart/form-data">
					<input type="text" name="product" id="quick-order" placeholder="Search"><button class="button" id="quick-order-btn">Add to Order</button>
				</form>			
			</div>
			<div class="cart-list">
				
					<?php include('inc/quick-order-cart.php'); ?>

			</div>
    </div>
    <?php
      if ( WC()->cart->get_cart_contents_count() == 0 ) {
        $hid_summary_class = 'hide-quick-order-summary';
      }
      else {
        $hid_summary_class = '';
      }
    ?>
    <div class="col-md-3 col-sm-9 quick-order-right">
			<div class="cart-summary <?php echo $hid_summary_class; ?>">
				<!--<h2>Checkout Now</h2>-->
          <div class="cart-summary-inner">
            <?php //do_action( 'woocommerce_cart_collaterals' ); ?>
            <?php
              global $woocommerce;  
              $subtotal = $woocommerce->cart->get_cart_subtotal();
              $total = $woocommerce->cart->get_cart_total();
              $cart_count = sizeof(WC()->cart->get_cart());
              if($cart_count>=1)
                $disp_class = 'incart';
              else
                $disp_class = 'not-incart';

                $checkout_url = $woocommerce->cart->get_checkout_url();
            ?>
            <div class="cart_totals ">  
              <h2>Cart totals</h2>
              <?php do_action( 'woocommerce_cart_collaterals' ); ?>
              <div class="wc-proceed-to-checkout <?php echo $disp_class; ?> quick-checkout">
                  <a class="custom-cart button alt wc-forward" href="<?php echo WC_Cart::get_cart_url(); ?>">View Cart</a>
                  <a href="<?php echo $checkout_url; ?>" class="checkout-button button alt wc-forward">Checkout Now</a>
              </div>              
            </div>
          </div>
			</div>		
		</div>
	</div>
		
        <!-- <div style="position: relative; height: 80px;">
            <input type="text" name="country" id="autocomplete-ajax" style="position: absolute; z-index: 2; background: transparent;"/>
            <input type="text" name="country" id="autocomplete-ajax-x" disabled="disabled" style="color: #CCC; position: absolute; background: transparent; z-index: 1;"/>
        </div>
        <div id="selction-ajax"></div> -->
	</div>

<?php get_footer(); ?>
<!-- <script type="text/javascript">
	jQuery(document).ready(function($) {
		$('#add-tocart').autocomplete({
	        serviceUrl: '<?php echo get_template_directory_uri().'/inc/ajax-get-products.php'; ?>',
	        onSelect: function (suggestion) {
	            alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
	        }
	    });
	});
</script> -->
<script>
  jQuery( function($) {
    function split( val ) {
      return val.split( /,\s*/ );
    }
    function extractLast( term ) {
      return split( term ).pop();
    }
 
    $( "#quick-order" )
      // don't navigate away from the field on tab when selecting an item
      .on( "keydown", function( event ) {
        jQuery('.quick-order-error').hide();
        if ( event.keyCode === $.ui.keyCode.TAB &&
            $( this ).autocomplete( "instance" ).menu.active ) {
          event.preventDefault();
        }
      })
      .autocomplete({
        source: function( request, response ) {
          $.getJSON( "<?php echo get_template_directory_uri().'/inc/ajax-get-products.php'; ?>", {
            term: extractLast( request.term )
          }, response );
        },
        response: function( event, ui ) {
          //console.log('hello success');
            /*response($.map(data.d, function (item) {
                return {
                    id: item.Value,
                    value: item.Text
                }
            }));*/
        },
        search: function() {
          // custom minLength
          var term = extractLast( this.value );
          if ( term.length < 2 ) {
            return false;
          }
        },
        focus: function() {
          // prevent value inserted on focus
          return false;
        },
        select: function( event, ui ) {
          var terms = split( this.value );
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push( ui.item.value );
          // add placeholder to get the comma-and-space at the end
          //terms.push( "" );
          this.value = terms.join( ", " );
          //console.log(ui);
          jQuery('.form-addtocart input[type="hidden"]').remove();
          var prod_id = ui.item.prod_id;
          var var_id = ui.item.var_id;
          var var_attribute = ui.item.var_attribute;
          var var_attribute_val = ui.item.var_attribute_val;
           var var_attributes_info = ui.item.attributes_info;
          jQuery( ".form-addtocart" ).append( $( '<input type="hidden" name="add-to-cart" value="'+prod_id+'">') );
          jQuery( ".form-addtocart" ).append( $( '<input type="hidden" name="product_id" value="'+prod_id+'">' ) );

          if(var_id!='no') {
          	jQuery( ".form-addtocart" ).append( $( '<input type="hidden" name="variation_id" value="'+var_id+'">' ) );
          }
          /*if(var_attribute!='no' && var_attribute_val!='no') {
          	jQuery( ".form-addtocart" ).append( $( '<input type="hidden" name="'+var_attribute+'" value="'+var_attribute_val+'">' ) );
          }*/

          if( var_attributes_info!="no" ) {
            jQuery( ".form-addtocart" ).append( var_attributes_info );
          }
          //console.log(prod_id+var_id+var_attribute+var_attribute_val);
          return false;
        },
        open: function(event, ui) {
          jQuery('.ui-autocomplete').off('menufocus hover mouseover mouseenter');
        }
      });
  } );
</script>