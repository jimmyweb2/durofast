<?php
/**
 * Template Name: Contact Request Forms
 */

get_header(); ?>
    <div class="header-section-inner">
		<?php
    	do_action('woo_custom_breadcrumb');
    ?>
	</div>
	<div class="main-page-title">
		<h1 class="main_title"><?php the_title(); ?></h1>
	</div>
	
	<div class="entry-content general-cms-page">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<?php the_content(); ?>

		<?php endwhile; ?>
		<?php endif; ?>
		<?php
		$page_image = get_field( 'page_image' );
		if( $page_image ) {
			?>
			<div class="page-image">
				<div class="page-image-img">
					<img src="<?php echo $page_image['url']; ?>" alt="<?php echo $page_image['title']; ?>">
				</div>
			</div>
			<?php
		}
		if( is_user_logged_in() ) {
			$curr_users = wp_get_current_user();
			echo '<input type="hidden" name="logged_user_email" class="logged_user_email" value="'.$curr_users->user_email.'">';
		}
		?>
	</div>

<?php get_footer(); ?>
