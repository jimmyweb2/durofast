<form action="/" method="get">
    <input type="text" name="s" id="search" value="<?php the_search_query(); ?>" placeholder="Search" />
    <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
</form>