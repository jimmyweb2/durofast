<?php if(is_front_page()) { ?>
<!-- Banner Slider -->
<div id="banner">
	<div class="banner-slider">
		<?php
		$banner = get_field('banner', 10);
		if($banner) {
			foreach($banner as $banner_row) {
				$banner_image = $banner_row['image'];
				$banner_title = $banner_row['title'];
				$banner_desc = $banner_row['description'];
				?>
				<div class="banner-wrapper">
					<div class="banner-image"><img class="resize-to-parent" src="<?php echo $banner_image['url']; ?> " title="<?php echo $banner_title; ?>"></div>
					<div class="banner-content">
						<div class="title"><h2><?php echo $banner_title; ?></h2></div>
						<div class="description container"><?php echo $banner_desc; ?></div>
					</div>
				</div>
				<?php
			}
		}
		//var_dump($banner);
		?>
	</div>
</div>
<?php } else { 
$banner_img = get_field('inner_banner_image', 10);
	?>
<div id="banner">
	<div class="inner-page-banner" style="background: url(<?php echo $banner_img['url']; ?>) center center;background-repeat: no-repeat;background-size: cover;">
	</div>
</div>
<?php } ?>