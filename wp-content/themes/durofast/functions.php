<?php
/**
 * Functions and definitions.
 *
 */

// Delcare Header/Footer compatibility.
define( 'DS_LIVE_COMPOSER_HF', true );
define( 'DS_LIVE_COMPOSER_HF_AUTO', false );

// Content Width ( WP requires it and LC uses is to figure out the wrapper width ).
if ( ! isset( $content_width ) ) {
	$content_width = 1180;
}

// Theme menus
function register_menu() {
   /* register_nav_menu('top-menu', __('Top Menu'));
    register_nav_menu('primary', __('Main Menu'));*/
    register_nav_menus( array(
	'top' => __( 'Top Menu', 'durofast' ),
	'primary' => __( 'Main Menu', 'durofast' ),
    'footer-top-1' => __( 'Footer Top First', 'durofast' ),
    'footer-top-2' => __( 'Footer Top Second', 'durofast' ),
    'footer-top-3' => __( 'Footer Top Third', 'durofast' ),
    'footer-top-4' => __( 'Footer Top Forth', 'durofast' ),
    'footer-bottom' => __( 'Footer Bottom', 'durofast' ),
    'footer-bottom-2' => __( 'Footer Bottom 2', 'durofast' )
) );
}

/**
 * Widgets
 */
function duro_widgets_init() {

	register_sidebar( array(
		'name'          => 'Shop Sidebar',
		'id'            => 'sidebar-1',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

    register_sidebar( array(
        'name'          => 'Checkout Sidebar',
        'id'            => 'sidebar-2',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );

}
add_action( 'widgets_init', 'duro_widgets_init' );

add_action('init', 'register_menu');

if ( ! function_exists( 'lct_theme_setup' ) ) {

	/**
	 * Basic theme setup.
	 */
	function lct_theme_setup() {

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support( 'title-tag' );
        
		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		// Enable Post Thumbnails ( Featured Image ).
		add_theme_support( 'post-thumbnails' );

		// Enable support for HTML5 markup.
		add_theme_support( 'html5', array( 'comment-list', 'search-form', 'comment-form' ) );

        // Enable woocommerce theme
        add_theme_support( 'woocommerce' );

	}
} add_action( 'after_setup_theme', 'lct_theme_setup' );

/**
 * Load CSS files.
 */
function duro_load_style() {

	wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/plugins/bootstrap/css/bootstrap.min.css', array(), '3.3.7' );

	wp_enqueue_style( 'slick-css', get_template_directory_uri() . '/plugins/slick/slick.css', array(), '1.6.0' );
	wp_enqueue_style('font-awesome', get_template_directory_uri() . '/plugins/font-awesome/css/font-awesome.min.css');
	wp_enqueue_style('mmenu-style', get_template_directory_uri() . '/plugins/mmenu/dist/css/jquery.mmenu.all.css', array() , '5.7.8');
    wp_enqueue_style('jquery-ui-style', get_template_directory_uri() . '/css/jquery-ui.css', array() , '1.12.1');
    wp_enqueue_style( 'base-style', get_stylesheet_uri(), array(), '1.0' );
    if( is_front_page() ) {
        wp_enqueue_style('magnific-popup', get_template_directory_uri() . '/css/magnific-popup.css', array() , '1.1.0');
    }
    if( is_front_page() || is_checkout() || is_account_page() ) {
        wp_enqueue_style('tooltipster-style', get_template_directory_uri() . '/css/tooltipster.bundle.css', array() , '4.2.6');
    }
	
} 
add_action( 'wp_enqueue_scripts', 'duro_load_style' );

/**
 * Load JS files
 */
function duro_load_scripts() {

	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/plugins/bootstrap/js/bootstrap.min.js', array(), '3.3.7' );
	wp_enqueue_script( 'slick-js', get_template_directory_uri() . '/plugins/slick/slick.min.js', array(), '1.6.0' );
	wp_enqueue_script( 'mmenu-js', get_template_directory_uri() . '/plugins/mmenu/dist/js/jquery.mmenu.all.min.js', array(), '5.7.8' );
	wp_enqueue_script( 'mmenu-fixed-js', get_template_directory_uri() . '/plugins/mmenu/dist/js/jquery.mmenu.fixedelements.min.js', array(), '1.0.0' );
    wp_enqueue_script( 'dotdotdot', get_template_directory_uri() . '/js/jquery.dotdotdot.min.js', array(), '1.8.3' );
    wp_enqueue_script( 'resize', get_template_directory_uri() . '/js/jquery.resizeimagetoparent.min.js', array(), '1.8.3' );
    if( is_account_page() ) {
        wp_enqueue_script( 'jquery-validate', get_template_directory_uri() . '/js/jquery.validate.min.js', array(), '1.0' );
    }

    if ( is_page_template( 'template-forms.php' ) ) {
        wp_enqueue_script( 'jquery-select2', get_template_directory_uri() . '/js/select2.full.min.js', array(), '4.0.3' );
    }
    wp_enqueue_script( 'init', get_template_directory_uri() . '/js/init.js', array(), '1.0' );
    //if ( is_page_template( 'template-quick-order.php' ) ) {
    // about.php is used
        /*wp_enqueue_script( 'jquery-mockjax', get_template_directory_uri() . '/plugins/jquery-autocomplete/jquery.mockjax.js', array(), '1.0' );
        wp_enqueue_script( 'jquery-autocomplete', get_template_directory_uri() . '/plugins/jquery-autocomplete/jquery.autocomplete.js', array(), '1.0' );
        wp_enqueue_script( 'country', get_template_directory_uri() . '/plugins/jquery-autocomplete/countries.js', array(), '1.0' );
         wp_enqueue_script( 'demo', get_template_directory_uri() . '/plugins/jquery-autocomplete/demo.js', array(), '1.0' );*/
         wp_enqueue_script( 'jquery-ui', get_template_directory_uri() . '/js/jquery-ui.js', array(), '1.12.1' );

    //}
    if( is_front_page() || is_checkout() || is_account_page() ) {
        wp_enqueue_script( 'magnific-popup', get_template_directory_uri() . '/js/jquery.magnific-popup.min.js', array(), '1.1.0' );
        wp_enqueue_script( 'tooltipster-js', get_template_directory_uri() . '/js/tooltipster.bundle.min.js', array(), '4.2.6' );
    }

} 
add_action( 'wp_enqueue_scripts', 'duro_load_scripts' );
if ( ! defined( 'DS_LIVE_COMPOSER_VER' ) ) {

	/**
	 * Admin Notice
	 */
	function lct_notification() {
	?>
		<div class="error">
			<p><?php printf( __( '%sLive Composer%s plugin is %srequired%s and has to be active for this theme to function.', 'lc-blank' ), '<a target="_blank" href="https://wordpress.org/plugins/live-composer-page-builder/">', '</a>', '<strong>', '</strong>' ); ?></p>
		</div>
	<?php }
	//add_action( 'admin_notices', 'lct_notification' );
}
add_filter('show_admin_bar', '__return_false');

//* Add stock status to archive pages
function df_stock_catalog() {
    global $product;
    if(!$product->sale_price) {
	    if ( $product->is_in_stock() ) {
            if($product->stock==0)
	            echo '<div class="out-of-stock" >'. __( 'pre order now', 'durofast' ) . '</div>';
            else
                echo '<div class="stock" >'. __( 'in stock', 'durofast' ) . '</div>';

	    } else {
	        echo '<div class="out-of-stock" >' . __( 'pre order now', 'durofast' ) . '</div>';
	    }
	}
}
add_action( 'woocommerce_before_shop_loop_item_title', 'df_stock_catalog' );

add_filter( 'woocommerce_sale_flash', 'wc_custom_replace_sale_text_df' );
function wc_custom_replace_sale_text_df( $html ) {
    return str_replace( __( 'Sale!', 'woocommerce' ), __( 'On Sale', 'woocommerce' ), $html );
}

//Reposition WooCommerce breadcrumb 
function woocommerce_remove_breadcrumb(){
remove_action( 
    'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
}
add_action(
    'woocommerce_before_main_content', 'woocommerce_remove_breadcrumb'
);

function woocommerce_custom_breadcrumb(){
    woocommerce_breadcrumb();
}

add_action( 'woo_custom_breadcrumb', 'woocommerce_custom_breadcrumb' );

// Display 24 products per page. Goes in functions.php
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 48;' ), 20 );

/**
 * Function to display variation table on product page
 */
function cmp_df($a, $b)
{
    return strnatcmp($a['sku'], $b['sku']);
}


function woocommerce_variable_add_to_cart_df(){
    global $product, $post;
 
    $variations = find_valid_variations();
    /*print_r("<pre style='margin-left:50px'>");
    print_r($variations);
    print_r("</pre>");*/
    usort($variations, "cmp_df");
    /*print_r("<pre style='margin-left:50px'>");
    print_r($variations);
    print_r("</pre>");*/
    // Check if the special 'price_grid' meta is set, if it is, load the default template:
    if ( get_post_meta($post->ID, 'price_grid', true) ) {
        // Enqueue variation scripts
        wp_enqueue_script( 'wc-add-to-cart-variation' );
 
        // Load the template
        wc_get_template( 'single-product/add-to-cart/variable.php', array(
                'available_variations'  => $product->get_available_variations(),
                'attributes'            => $product->get_variation_attributes(),
                'selected_attributes'   => $product->get_variation_default_attributes()
            ) );
        return;
    }
 
    // Cool, lets do our own template!
    ?>
    <div class="table-responsive clear variable-product-list">
    <table class="variations variations-grid table shop_table_responsive" id="custom-prod-table" cellspacing="0">
        <tbody>
            <thead>
  				<tr>
	            	<td>Product Code</td>
	            	
                    <?php
                    /*"Length     Width   Height  Depth   Details Drill Diameter  Max Fixture Thickness   Max Fixture Thickness to Steel  Max Fixture Thickness to Concrete   Screw Diameter  Embedment Depth Drive   Internal Diameter   Outside Diameter    Head Diameter   Shank Size  Thickness   Working Length  OAL Working Load    Fits Size   Pipe Size NB    Actual Pipe OD  Wall Thickness  Fastener Size";*/
                    $attribute_arr = array();
                    $size_count = 0;
                    $boxqty = false;
                    $size_arr = array();
                    foreach ($variations as $key => $value) {
                        
                        foreach($value['attributes'] as $key => $val ) {
                            $attribute_title = ucwords(str_replace(array('attribute_pa_','-'), array('',' '), $key));
                            $attr_taxonomy = str_replace('attribute_', '', $key);
                            $attr = $key;
                            //echo $attribute_title.'<br>';
                            if( $attribute_title == 'Length' ) {
                                //$attribute_arr[1] = $attribute_title;
                                $attribute_arr[1] = array(
                                                    'attr_title'=> $attribute_title,
                                                    'attr_taxonomy' => $attr_taxonomy,
                                                    'attr' => $attr
                                                    );
                            }
                            if( $attribute_title == 'Width' ) {
                                //$attribute_arr[2] = $attribute_title;
                                $attribute_arr[2] = array(
                                                    'attr_title' => $attribute_title,
                                                    'attr_taxonomy' => $attr_taxonomy,
                                                    'attr' => $attr
                                                    );
                            }
                            if( $attribute_title == 'Height' ) {
                                //$attribute_arr[3] = $attribute_title;
                                $attribute_arr[3] = array(
                                                    'attr_title' => $attribute_title,
                                                    'attr_taxonomy' => $attr_taxonomy,
                                                    'attr' => $attr
                                                    );
                            }
                            if( $attribute_title == 'Depth' ) {
                                //$attribute_arr[4] = $attribute_title;
                                $attribute_arr[4] = array(
                                                    'attr_title' => $attribute_title,
                                                    'attr_taxonomy' => $attr_taxonomy,
                                                    'attr' => $attr
                                                    );
                            }
                            if( $attribute_title == 'Details' ) {
                                //$attribute_arr[5] = $attribute_title;
                                $attribute_arr[5] = array(
                                                    'attr_title' => $attribute_title,
                                                    'attr_taxonomy' => $attr_taxonomy,
                                                    'attr' => $attr
                                                    );
                            }
                            if( $attribute_title == 'Drill Diameter' ) {
                                //$attribute_arr[6] = $attribute_title;
                                $attribute_arr[6] = array(
                                                    'attr_title' => $attribute_title,
                                                    'attr_taxonomy' => $attr_taxonomy,
                                                    'attr' => $attr
                                                    );
                            }
                            if( $attribute_title == 'Max Fixture Thickness' ) {
                                //$attribute_arr[7] = $attribute_title;
                                $attribute_arr[7] = array(
                                                    'attr_title' => $attribute_title,
                                                    'attr_taxonomy' => $attr_taxonomy,
                                                    'attr' => $attr
                                                    );
                            }
                            if( $attribute_title == 'Max Fixture Thickness to Steel' ) {
                                //$attribute_arr[8] = $attribute_title;
                                $attribute_arr[8] = array(
                                                    'attr_title' => $attribute_title,
                                                    'attr_taxonomy' => $attr_taxonomy,
                                                    'attr' => $attr
                                                    );
                            }
                            if( $attribute_title == 'Max Fixture Thickness to Concrete' ) {
                                //$attribute_arr[9] = $attribute_title;
                                $attribute_arr[9] = array(
                                                    'attr_title' => $attribute_title,
                                                    'attr_taxonomy' => $attr_taxonomy,
                                                    'attr' => $attr
                                                    );
                            }
                            if( $attribute_title == 'Screw Diameter' ) {
                                //$attribute_arr[10] = $attribute_title;
                                $attribute_arr[10] = array(
                                                    'attr_title' => $attribute_title,
                                                    'attr_taxonomy' => $attr_taxonomy,
                                                    'attr' => $attr
                                                    );
                            }
                            if( $attribute_title == 'Embedment Depth' ) {
                                //$attribute_arr[11] = $attribute_title;
                                $attribute_arr[11] = array(
                                                    'attr_title' => $attribute_title,
                                                    'attr_taxonomy' => $attr_taxonomy,
                                                    'attr' => $attr
                                                    );
                            }
                            if( $attribute_title == 'Drive' ) {
                                //$attribute_arr[12] = $attribute_title;
                                $attribute_arr[12] = array(
                                                    'attr_title' => $attribute_title,
                                                    'attr_taxonomy' => $attr_taxonomy,
                                                    'attr' => $attr
                                                    );
                            }
                            if( $attribute_title == 'Internal Diameter' ) {
                                //$attribute_arr[13] = $attribute_title;
                                $attribute_arr[13] = array(
                                                    'attr_title' => $attribute_title,
                                                    'attr_taxonomy' => $attr_taxonomy,
                                                    'attr' => $attr
                                                    );
                            }
                            if( $attribute_title == 'Outside Diameter' ) {
                                //$attribute_arr[14] = $attribute_title;
                                $attribute_arr[14] = array(
                                                    'attr_title' => $attribute_title,
                                                    'attr_taxonomy' => $attr_taxonomy,
                                                    'attr' => $attr
                                                    );
                            }
                            if( $attribute_title == 'Head Diameter' ) {
                                //$attribute_arr[15] = $attribute_title;
                                $attribute_arr[15] = array(
                                                    'attr_title' => $attribute_title,
                                                    'attr_taxonomy' => $attr_taxonomy,
                                                    'attr' => $attr
                                                    );
                            }
                            if( $attribute_title == 'Shank Size' ) {
                                //$attribute_arr[16] = $attribute_title;
                                $attribute_arr[16] = array(
                                                    'attr_title' => $attribute_title,
                                                    'attr_taxonomy' => $attr_taxonomy,
                                                    'attr' => $attr
                                                    );
                            }
                            if( $attribute_title == 'Thickness' ) {
                                //$attribute_arr[17] = $attribute_title;
                                $attribute_arr[17] = array(
                                                    'attr_title' => $attribute_title,
                                                    'attr_taxonomy' => $attr_taxonomy,
                                                    'attr' => $attr
                                                    );
                            }
                            if( $attribute_title == 'Working Length' ) {
                                //$attribute_arr[18] = $attribute_title;
                                $attribute_arr[18] = array(
                                                    'attr_title' => $attribute_title,
                                                    'attr_taxonomy' => $attr_taxonomy,
                                                    'attr' => $attr
                                                    );
                            }
                            if( $attribute_title == 'OAL' ) {
                                //$attribute_arr[19] = $attribute_title;
                                $attribute_arr[19] = array(
                                                    'attr_title' => $attribute_title,
                                                    'attr_taxonomy' => $attr_taxonomy,
                                                    'attr' => $attr
                                                    );
                            }
                            if( $attribute_title == 'Working Load' ) {
                                //$attribute_arr[20] = $attribute_title;
                                $attribute_arr[20] = array(
                                                    'attr_title' => $attribute_title,
                                                    'attr_taxonomy' => $attr_taxonomy,
                                                    'attr' => $attr
                                                    );
                            }
                            if( $attribute_title == 'Fits Size' ) {
                                //$attribute_arr[21] = $attribute_title;
                                $attribute_arr[21] = array(
                                                    'attr_title' => $attribute_title,
                                                    'attr_taxonomy' => $attr_taxonomy,
                                                    'attr' => $attr
                                                    );
                            }
                            if( $attribute_title == 'Pipe Size NB' ) {
                                //$attribute_arr[22] = $attribute_title;
                                $attribute_arr[22] = array(
                                                    'attr_title' => $attribute_title,
                                                    'attr_taxonomy' => $attr_taxonomy,
                                                    'attr' => $attr
                                                    );
                            }
                            if( $attribute_title == 'Actual Pipe OD' ) {
                                //$attribute_arr[23] = $attribute_title;
                                $attribute_arr[23] = array(
                                                    'attr_title' => $attribute_title,
                                                    'attr_taxonomy' => $attr_taxonomy,
                                                    'attr' => $attr
                                                    );
                            }
                            if( $attribute_title == 'Wall Thickness' ) {
                                //$attribute_arr[24] = $attribute_title;
                                $attribute_arr[24] = array(
                                                    'attr_title' => $attribute_title,
                                                    'attr_taxonomy' => $attr_taxonomy,
                                                    'attr' => $attr
                                                    );
                            }
                            if( $attribute_title == 'Fastener Size' ) {
                                //$attribute_arr[25] = $attribute_title;
                                $attribute_arr[25] = array(
                                                    'attr_title' => $attribute_title,
                                                    'attr_taxonomy' => $attr_taxonomy,
                                                    'attr' => $attr
                                                    );
                            }
                            
                            if( $key == 'attribute_pa_size' ) {
                                $size_count++;
                                $size_arr = array(
                                            'attr_title' => $attribute_title,
                                                    'attr_taxonomy' => $attr_taxonomy,
                                                    'attr' => $attr
                                            );
                            }

                            if( $key ==  'attribute_pa_boxqty' ) {
                                $boxqty = true;
                            }

                        }

                    }
                    if( $size_count > 0 )
                                echo '<td>Size</td>';
                    //var_dump($attribute_arr);
                    if( $attribute_arr ) {
                        foreach( $attribute_arr as $val ) {
                            ?>
                            <td><?php echo $val['attr_title']; ?></td>        
                            <?php
                        }
                    }
                    ?>
	            	<td>Unit Price</td>
	            	<?php if( $boxqty ) { ?>
                    <td>Box Qty</td>
                    <?php } ?>
	            	<td>Select Box/es</td>
	            	<td>&nbsp;</td>
                    <td>&nbsp;</td>
            	</tr>
            </thead>
            <?php
            foreach ($variations as $key => $value) {
            	//var_dump($value);
                if( !$value['variation_is_visible'] ) continue;
                $backorder = $value['backorders_allowed'];

                $variable_product = new WC_Product_Variation( $value['variation_id'] );
            ?>
            <tr>
                <td data-title="<?php _e( 'Product Code', 'woocommerce' ); ?>">
                	<?php echo $value['sku']; ?>
                    <?php
                    /*if($value['stock_status']=='instock') {
                        if($value['stock_quantity']==0)
                            echo '<span class="pre-order">Pre order now*</span>';
                    }
                    else {
                        echo '<span class="pre-order">Pre order now*</span>';
                    }*/
                    /*if($backorder===true && $value['stock_quantity'] <= 0)
                        echo '<span class="pre-order">Pre order now*</span>';*/
                    ?>
                </td>
                <?php /*var_dump($value['attributes']);*/ 
                //var_dump($size_arr);
                foreach($value['attributes'] as $key => $val ) {
                    if( $key == 'attribute_pa_size' ) {
                        $size_variation_id = $value['variation_id'];
                        $size_attribute = $size_arr['attr'];
                        $size_taxonomy = $size_arr['attr_taxonomy'];
                        $size_value = getAttributeTitle( $size_variation_id, $size_taxonomy, $size_attribute );
                        echo '<td data-title="Size">';
                        //$val = str_replace(array('-','_'), ' ', $val);
                        //printf( '<span class="attr attr-%s">%s</span> ', $key, ucwords($val) );
                        printf( '<span class="attr attr-%s">%s</span> ', $size_attribute, $size_value );
                        echo '</td>';
                    }
                } ?>
                <?php
                if( $attribute_arr ) {
                    foreach( $attribute_arr as $val ) {
                        //var_dump($val);
                        $prod_variation_id = $value['variation_id'];
                        $variable_attribute = $val['attr'];
                        $varaible_taxonomy = $val['attr_taxonomy'];
                        $attribute_value = getAttributeTitle( $prod_variation_id, $varaible_taxonomy, $variable_attribute );
                        /*$attribue_slug = str_replace(' ','-',strtolower($val));
                        $attribute_value = $value['attributes']['attribute_pa_'.$attribue_slug];*/
                        //$attribute_value = $value['attributes'][$variable_attribute];
                        ?>
                        <td data-title="<?php _e( $val['attr_title'], 'woocommerce' ); ?>"><?php echo $attribute_value; ?></td>
                        <?php
                    }
                }
                if( $boxqty ) { 
                    $boxqty_val = $value['attributes']['attribute_pa_boxqty']; 
                }
                else
                    $boxqty_val = 1;
                /*print_r("<pre style='margin-left:50px'>");
    print_r($value);
    print_r("</pre>");*/
                ?>
                <td data-title="<?php _e( 'Unit Price', 'woocommerce' ); ?>">
                    <?php 
                    $v_price = $variable_product->get_price_html();
                    echo calculate_price_by_boxqty( $boxqty_val, $v_price );
                   //echo $v_price * $boxqty_val;

                    /*if( $value['price_html'] != '' ) {
                        echo $value['price_html'];
                    }
                    else {
                        if( $value['display_price'] != '' ) {
                            echo '<span class="price"><del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>'.$value['display_regular_price'].'</span></del> <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>'.$value['display_price'].'</span></ins></span>';
                        }
                        else {
                            echo '<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>'.$value['display_regular_price'].'</span></span>';
                        }
                        
                    }*/
                    ?>
                </td>
                <?php if( $boxqty ) { $boxqty_val = $value['attributes']['attribute_pa_boxqty']; ?>
                <td data-title="<?php _e( 'Box Qty', 'woocommerce' ); ?>">
                	<?php echo $value['attributes']['attribute_pa_boxqty']; ?>
                </td>
                <?php } else  { $boxqty_val = 1; } ?>
                
                    <td data-title="<?php _e( 'Select Box/es', 'woocommerce' ); ?>">
                        <div class="quantity">
                            <input type="number" name="custom-qty" id="custom-qty" value="1" class="qty custom-qty">
                        </div>
                    </td>
                    <?php //if( $value['is_in_stock'] ) { ?>
                    
                        
                        <td class="fixed-btn-cart ajax-add-cart">
                            <form class="cart variations_form" action="<?php echo esc_url( $product->add_to_cart_url() ); ?>" method="post" enctype='multipart/form-data'>
                            <?php woocommerce_quantity_input(); ?>
                        <?php
                        if(!empty($value['attributes'])){
                            foreach ($value['attributes'] as $attr_key => $attr_value) {
                            ?>
                            <input type="hidden" name="<?php echo $attr_key?>" value="<?php echo $attr_value?>">
                            <?php
                            }
                        }
                        ?>
                        <?php
                        $check_in_list = '';
                        if( woocommerce_wishlists_get_wishlists_for_product( $post->ID ) ) {
                            $check_in_list = checkProductInWishlist( woocommerce_wishlists_get_wishlists_for_product( $post->ID ), $value['variation_id'], 'variable' );
                        }

                        $variation_obj = new WC_Product_variation($value['variation_id']);
                        $stock_qty = $variation_obj->get_stock_quantity();
                            //var_dump($value);
                        if($backorder===true && $stock_qty <= 0)
                            $addtocart_text = 'Back Order Now';
                        else
                            $addtocart_text = 'Add to Cart';
                        ?>
                        <button type="submit" class="single_add_to_cart_button btn btn-primary variable-prod variable-btn"><?php echo $addtocart_text; ?></button>
                        <input type="hidden" name="variation_id" value="<?php echo $value['variation_id']?>" />
                        <input type="hidden" name="product_id" value="<?php echo esc_attr( $post->ID ); ?>" />
                        <input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $post->ID ); ?>" />
                        <input type="hidden" name="boxqty_val" value="<?php echo base64_encode($boxqty_val); ?>">
                         </form>
                        <div class="custom-woo-message">
                        </div>
                        </td>
                   
                        <td class="fixed-btn-cart p-add-to-list">
                            <?php
                            if( $check_in_list === true )
                                $check_in_list_class = 'in-wishlist';
                            else
                                $check_in_list_class = 'not-wishlist';
                            ?>
                            <form class="cart <?php echo $check_in_list_class; ?>" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="variation_id" value="<?php echo $value['variation_id']?>" />
                                <input type="hidden" name="product_id" value="<?php echo esc_attr( $post->ID ); ?>" />
                                <input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $post->ID ); ?>" />
                                <?php
                                if(!empty($value['attributes'])){
                                    foreach ($value['attributes'] as $attr_key => $attr_value) {
                                    ?>
                                    <input id="<?php echo str_replace('attribute_','',$attr_key); ?>" type="hidden" name="<?php echo $attr_key?>" value="<?php echo $attr_value?>">
                                    <?php
                                    }
                                }
                                ?>
                                <?php
                                    $wl = $GLOBALS['wishlists'];
                                    $wl->add_to_wishlist_button();
                                ?>
                            </form>
                        </td>
                    <?php /*} else { ?>
                        <p class="stock out-of-stock"><?php _e( 'This product is currently out of stock and unavailable.', 'woocommerce' ); ?></p>
                    <?php }*/ ?>
                
            </tr>
            <?php } ?>
        </tbody>
    </table>
    </div>
    <?php
}

/**
 *  Function 
 */
function getAttributeTitle( $prod_id, $taxonomy, $attr_taxonomy ) {
    //echo $prod_id. $taxonomy. $attr_taxonomy.'<br>';
    $meta = get_post_meta($prod_id, $attr_taxonomy, true);
    $term = get_term_by('slug', $meta, $taxonomy);
    return $term->name;
}
/**
 * Function to display table for a simple product
 */
function woocommerce_simple_product_table(){
    global $product, $post;
    //var_dump($product);
    $backorder = $product->backorders;
    ?>
    <div class="table-responsive clear variable-product-list">
    <table class="variations variations-grid table shop_table_responsive" cellspacing="0">
        <tbody>
            <thead>
                <tr>
                    <td>Product Code</td>
                    <td>Unit Price</td>
                    <td>Box Qty</td>
                    <td>Select Box/es</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </thead>
            <tr>
                <td data-title="Product Code">
                    <?php echo $product->sku; ?>
                    <?php
                    /*if( $product->stock_status == 'instock' ) {
                        if( $product->stock_quantity == 0 )
                            echo '<span class="pre-order">Pre order now*</span>';
                    }
                    else {
                        echo '<span class="pre-order">Pre order now*</span>';
                    }*/
                    /*if($backorder=='notify' && $product->stock_quantity <= 0)
                        echo '<span class="pre-order">Pre order now*</span>';
                    else
                        echo '';*/
                    ?>
                </td>
                <td data-title="Unit Price">
                    <?php //echo $product['price_html'];?>
                    <?php //echo $product->get_price_html();?>
                    <?php
                    $prod_boxqty = $product->get_attribute( 'pa_boxqty' );
                    if( $prod_boxqty ) {
                        $boxqty_val = $prod_boxqty;
                    }
                    else
                        $boxqty_val = 1;

                    $v_price = $product->get_price_html();
                    echo calculate_price_by_boxqty( $boxqty_val, $v_price );
                    ?>
                </td>
                <td data-title="Box Qty">
                    <?php
                    echo $boxqty_val;
                    ?>
                </td>
                
                <?php //if( $value['is_in_stock'] ) { ?>
                    <td data-title="Select Box/es">
                        <div class="quantity">
                            <input type="number" name="custom-qty" id="custom-qty" value="1" class="qty custom-qty">
                        </div>
                    </td>
                    <td class="fixed-btn-cart ajax-add-cart">
                        <form class="cart variations_form" action="<?php echo esc_url( $product->add_to_cart_url() ); ?>" method="post" enctype='multipart/form-data'>
                        <?php woocommerce_quantity_input(); ?>
                        <?php
                        if($backorder=='notify' && $product->stock_quantity <= 0)
                            $cart_text = 'Back Order Now';
                        else
                            $cart_text = 'Add to Cart';
                        ?>
                        <button type="submit" class="single_add_to_cart_button btn btn-primary simple-prod"><?php echo $cart_text; ?></button>
                        <input type="hidden" name="variation_id" value="<?php echo $value['variation_id']?>" />
                        <input type="hidden" name="product_id" value="<?php echo esc_attr( $post->ID ); ?>" />
                        <input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $post->ID ); ?>" />
                        <input type="hidden" name="boxqty_val" value="<?php echo base64_encode($boxqty_val); ?>">
                    </form>  
                    <div class="custom-woo-message msg-single-prod">
                    </div>
                    </td>                    
                    <td class="fixed-btn-cart p-add-to-list">
                    <?php
                    $check_in_list = '';
                    if( woocommerce_wishlists_get_wishlists_for_product( $post->ID ) ) {
                        $check_in_list = checkProductInWishlist( woocommerce_wishlists_get_wishlists_for_product( $post->ID ), $post->ID, 'simple' );
                    }
                    if( $check_in_list === true )
                        $check_in_list_class = 'in-wishlist';
                    else
                        $check_in_list_class = 'not-wishlist';
                    ?>
                        <form class="cart <?php echo $check_in_list_class; ?>" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $post->ID ); ?>" />
                            <?php
                                $wl = $GLOBALS['wishlists'];
                                $wl->add_to_wishlist_button();
                            ?>
                        </form>
                    </td>              
            </tr>
        </tbody>
    </table>
    </div>
    <?php
}

function find_valid_variations() {
    global $product;
 
    $variations = $product->get_available_variations();
    $attributes = $product->get_attributes();
    $new_variants = array();
 
    // Loop through all variations
    foreach( $variations as $variation ) {
 
        // Peruse the attributes.
 
        // 1. If both are explicitly set, this is a valid variation
        // 2. If one is not set, that means any, and we must 'create' the rest.
 
        $valid = true; // so far
        foreach( $attributes as $slug => $args ) {
            if( array_key_exists("attribute_$slug", $variation['attributes']) && !empty($variation['attributes']["attribute_$slug"]) ) {
                // Exists
 
            } else {
                // Not exists, create
                $valid = false; // it contains 'anys'
                // loop through all options for the 'ANY' attribute, and add each
                foreach( explode( '|', $attributes[$slug]['value']) as $attribute ) {
                    $attribute = trim( $attribute );
                    $new_variant = $variation;
                    $new_variant['attributes']["attribute_$slug"] = $attribute;
                    $new_variants[] = $new_variant;
                }
 
            }
        }
 
        // This contains ALL set attributes, and is itself a 'valid' variation.
        if( $valid )
            $new_variants[] = $variation;
 
    }
 
    return $new_variants;
}

// Ensure cart contents update when products are added to the cart via AJAX (place the following in functions.php).
// Used in conjunction with https://gist.github.com/DanielSantoro/1d0dc206e242239624eb71b2636ab148
add_filter('add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');
 
function woocommerce_header_add_to_cart_fragment( $fragments ) {
    global $woocommerce;
    
    ob_start();
    
    ?>
    <a class="cart-customlocation" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>"><?php echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?> - <?php echo $woocommerce->cart->get_cart_total(); ?></a>
    <?php
    
    $fragments['a.cart-customlocation'] = ob_get_clean();
    
    return $fragments;
    
}

// WooCommerce Checkout Fields Hook
add_filter( 'woocommerce_checkout_fields' , 'checkout_placeholder_wc_checkout_fields', 15 );
 
// This example changes the default placeholder text for the state drop downs to "Select A State"
function checkout_placeholder_wc_checkout_fields( $fields ) {
    //var_dump($fields);
    //unset($fields['billing']['billing_first_name']);
    //unset($fields['billing']['billing_last_name']);
    //unset($fields['billing']['billing_phone']);
    //unset($fields['billing']['billing_company']);
    /*$fields['billing']['billing_email']['placeholder'] = 'Email';
    $fields['billing']['billing_state']['placeholder'] = 'State';
    $fields['billing']['billing_first_name']['placeholder'] = 'First Name';
    $fields['billing']['billing_last_name']['placeholder'] = 'Last Name';
    $fields['billing']['billing_company']['placeholder'] = 'Company Name';
    $fields['billing']['billing_address_1']['placeholder'] = 'Address';
    $fields['billing']['billing_address_2']['placeholder'] = 'Address Optional';
    $fields['billing']['billing_city']['placeholder'] = 'Suburb';
    $fields['billing']['billing_postcode']['placeholder'] = 'Postcode';
    $fields['billing']['billing_country']['placeholder'] = 'Country';*/
    
    //$fields['billing']['billing_phone']['placeholder'] = 'Phone';

   /* $fields['shipping']['shipping_first_name']['placeholder'] = 'First Name';
    $fields['shipping']['shipping_last_name']['placeholder'] = 'Last Name';
    $fields['shipping']['shipping_company']['placeholder'] = 'Company Name';
    $fields['shipping']['shipping_address_1']['placeholder'] = 'Address';
    $fields['shipping']['shipping_address_2']['placeholder'] = 'Address Optional';
    $fields['shipping']['shipping_city']['placeholder'] = 'Suburb';
    $fields['shipping']['shipping_postcode']['placeholder'] = 'Postcode';
    $fields['shipping']['shipping_country']['placeholder'] = 'Country';
    $fields['shipping']['shipping_state']['placeholder'] = 'State';
    //$fields['account']['account_username']['placeholder'] = 'Username';
    $fields['account']['account_password']['placeholder'] = 'Password';*/
    //$fields['account']['account_password-2']['placeholder'] = 'Confirm Password';
    //$fields['account']['password_current']['placeholder'] = 'Confirm Password';
    
    // Purchcase order number
    if( is_account_page() && !is_user_logged_in() ) {
        // don't show purchase order number
    }
    else {
        $fields2['billing']['billing_purchase_order_number'] = array(
            'label' => __("Enter Your Company's Purchase Order Number", 'woocommerce'), // Add custom field label
            'placeholder' => _x('Purchase order number', 'placeholder', 'woocommerce'), // Add custom field placeholder
            'required' => true, // if field is required or not
            'clear' => false, // add clear or not
            'type' => 'text', // add field type
            'class' => array('billing-pon')   // add class name
        );
    }
    if( !is_user_logged_in() && !is_account_page() ) {
        $fields2['billing']['billing_first_name'] = $fields['billing']['billing_first_name'];
        $fields2['billing']['billing_last_name'] = $fields['billing']['billing_last_name'];
        $fields2['billing']['billing_company'] = $fields['billing']['billing_company'];
        $fields2['billing']['billing_phone'] = $fields['billing']['billing_phone'];

        $fields2['billing']['billing_first_name']['label'] = __('First Name', 'woocommerce');
        $fields2['billing']['billing_last_name']['label'] = __('Last Name', 'woocommerce');
        $fields2['billing']['billing_company']['label'] = __('Company Name', 'woocommerce');
        $fields2['billing']['billing_phone']['label'] = __('Phone', 'woocommerce');
    }
    $fields2['billing']['billing_email'] = $fields['billing']['billing_email'];
    $fields2['billing']['billing_address_1'] = $fields['billing']['billing_address_1'];
    $fields2['billing']['billing_address_2'] = $fields['billing']['billing_address_2'];
    $fields2['billing']['billing_city'] = $fields['billing']['billing_city'];
    $fields2['billing']['billing_state'] = $fields['billing']['billing_state'];
    $fields2['billing']['billing_postcode'] = $fields['billing']['billing_postcode'];
    /*$fields2['billing']['shipping_country'] = $fields['billing']['shipping_country'];*/
    $fields2['billing']['billing_address_1']['placeholder'] = 'Street address line 1';
    $fields2['billing']['billing_address_2']['placeholder'] = 'Street address line 2';
    
    $fields2['billing']['billing_email']['placeholder'] = 'Email';
    $fields2['billing']['billing_state']['label'] = 'State';
    $fields2['billing']['billing_state']['placeholder'] = 'State';
    $fields2['billing']['billing_city']['placeholder'] = 'Suburb';
    $fields2['billing']['billing_city']['label'] = 'Suburb';
    $fields2['billing']['billing_postcode']['placeholder'] = 'Postcode';
    $fields2['billing']['billing_postcode']['label'] = 'Postcode';
    $fields2['billing']['billing_email']['label'] = __('Email <a href="#" title="This is the address your invoices & statements will be sent to!" class="df-tooltip checkout"><span class="info"></span></a>', 'woocommerce');
    $fields2['billing']['billing_address_1']['label'] = __('Billing Address', 'woocommerce');
    $fields2['billing']['billing_email' ]['placeholder'] = 'Email';

    $is_credit_user = is_credit_account();
    if( $is_credit_user === true ) {
        $fields2['billing']['billing_address_1']['class'] = array('hide-checkout');
        $fields2['billing']['billing_address_2']['class'] = array('hide-checkout');
        $fields2['billing']['billing_email']['class'] = array('hide-checkout');
        $fields2['billing']['billing_state']['class'] = array('hide-checkout');
        $fields2['billing']['billing_city']['class'] = array('hide-checkout');
        $fields2['billing']['billing_postcode']['class'] = array('hide-checkout');
    }
    
    $fields2['shipping']['shipping_site_contact'] = $fields['shipping']['shipping_site_contact'];
    $fields2['shipping']['shipping_site_contact']['label'] = 'Site Contact Name';
    $fields2['shipping']['shipping_site_contact']['placeholder'] = '';

    $fields2['shipping']['shipping_phone'] = $fields['shipping']['shipping_phone'];
    $fields2['shipping']['shipping_phone']['label'] = 'Site Contact Phone';
    $fields2['shipping']['shipping_phone']['placeholder'] = '';

    $fields2['shipping']['shipping_is_active'] = $fields['shipping']['shipping_is_active'];
    $fields2['shipping']['shipping_is_active']['label'] = 'Is Active';
    $fields2['shipping']['shipping_is_active']['placeholder'] = '';

    $fields2['shipping']['shipping_opportunity'] = $fields['shipping']['shipping_opportunity'];
    $fields2['shipping']['shipping_opportunity']['label'] = 'Opportunity';
    $fields2['shipping']['shipping_opportunity']['placeholder'] = '';

    $fields2['shipping']['shipping_address_id'] = $fields['shipping']['shipping_address_id'];
    $fields2['shipping']['shipping_address_id']['label'] = 'Shipping Address ID';
    $fields2['shipping']['shipping_address_id']['placeholder'] = '';

    /*$fields2['shipping']['shipping_first_name'] = $fields['shipping']['shipping_first_name'];
    $fields2['shipping']['shipping_first_name']['label'] = 'Site Contact Name';
    $fields2['shipping']['shipping_first_name']['placeholder'] = '';

    $fields2['shipping']['shipping_last_name'] = $fields['shipping']['shipping_last_name'];
    $fields2['shipping']['shipping_last_name']['label'] = 'Site Contact Phone';
    $fields2['shipping']['shipping_last_name']['placeholder'] = '';*/
    $fields2['shipping']['shipping_address_1'] = $fields['shipping']['shipping_address_1'];
    $fields2['shipping']['shipping_address_1']['placeholder'] = 'Street address';
    $fields2['shipping']['shipping_address_2'] = $fields['shipping']['shipping_address_2'];
    $fields2['shipping']['shipping_address_2']['placeholder'] = 'Apartment, suite, unit etc. (optional)';
    $fields2['shipping']['shipping_city'] = $fields['shipping']['shipping_city'];
    $fields2['shipping']['shipping_city']['label'] = 'Suburb';

    $fields2['shipping']['shipping_state'] = $fields['shipping']['shipping_state'];
    $fields2['shipping']['shipping_state']['label'] = 'State';

    $fields2['shipping']['shipping_postcode'] = $fields['shipping']['shipping_postcode'];    
    $fields2['shipping']['shipping_postcode']['label'] = 'Postcode';
    $fields2['order']['order_comments'] = $fields1['order']['order_comments'];
    $fields2['order']['order_comments']['type'] = 'textarea';
    $fields2['order']['order_comments']['placeholder'] = 'Please provide any site requirements or additional information to ensure delivery is a breeze, e.g. flashing lights, hi-vis, “do not leave with gate keeper” etc.';
    $fields2['order']['order_comments']['label'] = 'Order Notes';
    //$fields['account']['account_username']['placeholder'] = 'Username';
    $fields2['account']['account_password'] = $fields['account']['account_password'];
    $fields2['account']['account_password']['label'] = 'Account password';
    $fields2['account']['account_password']['type'] = 'password';
     

    return $fields2;
}

/**/
function df_checkout_field_required( $fields ) {
    $fields['billing_company']['required'] = true;
    return $fields;
}
add_filter( 'woocommerce_billing_fields', 'df_checkout_field_required' );

// Limit Woocommerce phone field to 10 digits number
//add_action('woocommerce_checkout_process', 'df_validate_phone_number');
  
function df_validate_phone_number() {
    global $woocommerce;
  
    // Check if set, if its not set add an error. This one is only requite for companies
    if ( (! (preg_match('/^[0-9]{10}$/D', $_POST['billing_phone'] )) || ! (preg_match('/^[0-9]{11}$/D', $_POST['billing_phone'] ))) || isset( $_POST['billing_phone'] ) && empty( $_POST['billing_phone'] )){
        //wc_add_notice( "Please enter valid phone number"  ,'error' );
        $errors->add( 'billing_phone_error', __( 'Please enter valid phone number!.', 'woocommerce' ) );
    }
}

/*
 * Remove required for shipping fields
 */
function df_unrequire_wc_shipping_fields( $fields ) {
    $is_credit_user = is_credit_account();
    if( $is_credit_user === true ) {
        $fields['shipping_first_name']['required'] = false;
        $fields['shipping_last_name']['required'] = false;
        $fields['shipping_address_1']['required'] = false;
        $fields['shipping_city']['required'] = false;
        $fields['shipping_state']['required'] = false;
        $fields['shipping_postcode']['required'] = false;
    }
    return $fields;
}
//add_filter( 'woocommerce_shipping_fields', 'df_unrequire_wc_shipping_fields' );

add_filter( 'woocommerce_widget_cart_is_hidden', 'always_show_cart', 11, 0 );
function always_show_cart() {
    return false;
}

//add_action( 'woocommerce_before_add_to_cart_button', 'df_display_bulk_discount_table' );

function df_display_bulk_discount_table() {

    global $woocommerce, $post, $product;

    $array_rule_sets = get_post_meta( $post->ID, '_pricing_rules', true );

    if ( $array_rule_sets && is_array( $array_rule_sets ) && sizeof( $array_rule_sets ) > 0 ) {

    $tempstring .= '<table class="table pricing-table">';
    if ( $product->is_type( 'variable' ) ) {
        $tempstring .= '<th>Membership</th><th>Size</th><th>Quantity</th><th>Discount</th>';
    }
    else
        $tempstring .= '<th>Membership</th><th>Quantity</th><th>Discount</th>';

    foreach( $array_rule_sets as $pricing_rule_sets ) {
        $roles = '';
        $varaible_name = '';
        /*echo 'Variation <br>';
        echo count($pricing_rule_sets['variation_rules']);
        var_dump($pricing_rule_sets['variation_rules']);
        echo 'Roles :<br>';
        var_dump($pricing_rule_sets['conditions']);
        echo 'collector variation product<br>';
        var_dump($pricing_rule_sets['collector']);
    
    echo '<br>';*/
        // Check if the rule is applied for all variables or specific
        if ( $product->is_type( 'variable' ) ) {
            //echo 'Variation <br>';
            $variable_info = $pricing_rule_sets['variation_rules'];
            //var_dump($variable_info);
            if($variable_info['args']['type']=='product')
                $varaible_name = 'All products';
            else {
                $price_variables = $variable_info['args']['variations'];
                for($vi=0;$vi<count($price_variables);$vi++) {
                    $varaible_id = $price_variables[$vi];
                    //$_product = wc_get_product( $varaible_id );
                    //var_dump($_product);
                    //echo get_taxonomy('pa_thickness')->labels->name;
                    //var_dump(get_taxonomy('pa_thickness'));
                    //var_dump($_product);
                    //echo WC()->cart->get_item_data( $cart_item );
                    $prod_variables = new WC_Product_Variable( $post->ID );
                    $variables = $prod_variables->get_available_variations();
                    if($variables) {
                        foreach($variables as $variables_val) {
                            //echo 'loop id:'.$variables_val['variation_id'].'<br>';
                            //echo 'main id:'.$varaible_id.'<br>';
                            if( $variables_val['variation_id'] == $varaible_id ) {
                                $variable_attributes = $variables_val['attributes'];
                                //var_dump($variable_attributes);
                                foreach ($variable_attributes as $key => $value) {
                                    $tax_name = str_replace('attribute_', '', $key);
                                    $attributes = get_term_by("slug", "$value", "$tax_name");
                                    //echo 'hjaha'.$attributes;
                                    //var_dump($attributes);
                                    $variable_name = $attributes->name;
                                }
                            }
                        }
                        //var_dump($variables);
                    }
                    
                }
            }

            //var_dump($price_variables);
        }
         // End Check if the rule is applied for all variables or specific
    foreach($pricing_rule_sets['conditions'] as $conditions) {
        //var_dump($conditions);
        //echo 'condition roles';
        //var_dump($conditions['args']['roles']);
        $roleinfo = $conditions['args']['roles'];
        if($conditions['args']['roles']) {
            for($ir=0;$ir<count($roleinfo);$ir++) {
                //$get_roles = get_role( $roleinfo[$ir] );
                //var_dump($get_roles);
                $roles .= ucwords( str_replace('-', ' ', $roleinfo[$ir]) ).'<br>';
                //echo $roleinfo[$ir];
            }
        }
        $rolename = get_role( $role );
    }
    /*var_dump($pricing_rule_sets['collector']);
    $i = 1;
    foreach ( $pricing_rule_sets as $key => $val ) {
    
    //var_dump($pricing_rule_sets_val);
        echo 'Key: '.$key.'<br>';
    $i++;
}
echo 'Total loop:'.$i.'<br>';*/
/*foreach ( $pricing_rule_sets['variation_rules'] as $key => $value_rules ) {
    echo $pricing_rule_sets['variation_rules'][$key].$key.'<br>';
    var_dump($value_rules);
}*/
//var_dump($pricing_rule_sets['variation_rules']);
        foreach ( $pricing_rule_sets['rules'] as $key => $value ) {

            switch ( $pricing_rule_sets['rules'][$key]['type'] ) {

                case 'percentage_discount':

                $woosymbol = '%';

                break;

                case 'price_discount':

                $woosymbol = get_woocommerce_currency_symbol();

                break;

            }

        $tempstring .= '<tr>';
        $tempstring .= '<td>'.$roles.'</td>';
        if ( $product->is_type( 'variable' ) ) {
            $tempstring .= '<td>'.$variable_name.'</td>';
        }
        $tempstring .= '<td>'.$pricing_rule_sets['rules'][$key]['from']." - ".$pricing_rule_sets['rules'][$key]['to']."</td>";

        $tempstring .= '<td><span class="amount">' . $woosymbol . "" . $pricing_rule_sets['rules'][$key]['amount']. "</span></td>";

        $tempstring .= '</tr>';


        }

    }
    $tempstring .= "<tr><td colspan='3'>*Price adjustments will be reflected in the cart.</td></tr>";
    $tempstring .= "</table>";

    echo $tempstring;

    }

}

add_filter( 'woocommerce_product_tabs', 'wcs_woo_remove_reviews_tab', 98 );
    function wcs_woo_remove_reviews_tab($tabs) {
    unset($tabs['reviews']);
    return $tabs;
}


/* Add to cart ajax custom */
add_action( 'wp_enqueue_scripts', 'so_load_script', 20 );
function so_load_script(){
    wp_enqueue_script( 'add-to-cart-ajax', get_template_directory_uri() . '/js/add-to-cart-ajax.js', array(), '1.0' );
    $i18n = array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 'checkout_url' => get_permalink( wc_get_page_id( 'checkout' ) ) );
    wp_localize_script( 'so_test', 'SO_TEST_AJAX', $i18n );
}

add_action('wp_ajax_df_ajax_add_to_cart', 'df_ajax_add_to_cart_callback');
add_action('wp_ajax_nopriv_df_ajax_add_to_cart', 'df_ajax_add_to_cart_callback');

    /**
     * AJAX add to cart. for quick order
     */
function df_ajax_add_to_cart_callback() {        
    ob_start();

    //$product_id        = 264;
    $product_id        = $_POST['product_id'];
    $quantity          = 1;
    /*if($_POST[''])*/
    // check if the cart is empty at the beginning
    if ( WC()->cart->get_cart_contents_count() != 0 ) {
        $empty_cart = true;
    }
    else
        $empty_cart = false;   

    if($_POST['variation_id']!='' && $_POST['variation']!='' ) {
        /*$variation_id = $_POST['variation_id'];
        $variations = array();
        $variations[$_POST['var_attribute']] = $_POST['var_attribute_val'];
        $passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity, $variation_id, $variations );
        $product_status    = get_post_status( $product_id );
        //var_dump($variation_id);
        $prod_qty = get_post_meta($variation_id,'_stock',true);
        //if ( $passed_validation && WC()->cart->add_to_cart( $product_id, $quantity, $variation_id, $variations ) && 'publish' === $product_status ) {
        if ( $passed_validation && 'publish' === $product_status ) {

            do_action( 'woocommerce_ajax_added_to_cart', $product_id );

            wc_add_to_cart_message( $product_id );
            include('inc/ajax-quick-order-cart.php');
            global $woocommerce;
            $data = array(
                'success'       => true,
                'result'              => $cart_html,
                'cart_item_count'     => $woocommerce->cart->cart_contents_count,
                'cartsubtotal'        => $woocommerce->cart->get_cart_total(),
                'carttotal'           => $woocommerce->cart->get_cart_total()
            );

        } else {

            // If there was an error adding to the cart, redirect to the product page to show any errors
            $data = array(
                'error'       => true,
                'product_url' => apply_filters( 'woocommerce_cart_redirect_after_error', get_permalink( $product_id ), $product_id )
            );

            

        }*/
        $product_id = apply_filters( 'woocommerce_add_to_cart_product_id', absint( $_POST['product_id'] ) );
        $quantity = 1;
        $variation_id = $_POST['variation_id'];
        $variation  = $_POST['variation'];

        $boxqty_val = get_post_meta( $variation_id, 'attribute_pa_boxqty', true );
        if( $boxqty_val != "" ) {
            $quantity = $quantity * $boxqty_val;
        }
        //var_dump($variation);
        $passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity );

        if ( $passed_validation && WC()->cart->add_to_cart( $product_id, $quantity, $variation_id, $variation  ) ) {
            do_action( 'woocommerce_ajax_added_to_cart', $product_id );
            if ( get_option( 'woocommerce_cart_redirect_after_add' ) == 'yes' ) {
            wc_add_to_cart_message( $product_id );}
            //WC_AJAX::get_refreshed_fragments();
            //$cart_items = WC()->cart->get_cart_contents_count();
            $cart_items = get_cart_qty();
            $product_name = get_the_title($product_id);
            /* Back order message */
            $variation_obj = new WC_Product_variation( $variation_id );
            $stock_qty = $variation_obj->get_stock_quantity();
            $sku = $variation_obj->sku;
            if( $sku != "" )
                $product_name = $sku;
                //var_dump($value);
            $backorder = $variation_obj->backorders;        
            
            $msg = '<p>'.$product_name.' has been added to the cart!</p>';
            if($backorder==='notify' && $stock_qty <= 0)
                $msg .= get_field( 'back_order_text', 'options' );
                

            /*$results = array(
                'success' => true,
                'message' => $msg,
                'cart_num' => $cart_items 
            );*/

            include('inc/ajax-quick-order-cart.php');
            global $woocommerce;
            $cart_summary_total = WC()->cart->get_totals();
            /*$tax = WC()->cart->get_tax_totals();
            $total_inc_tax = */
            $cart_sub_total = $cart_summary_total['cart_contents_total'];
            $cart_tax = $cart_summary_total['cart_contents_tax'];
            $cart_total = $cart_summary_total['total'];
            $data = array(
                'success'       => true,
                'result'              => $cart_html,
                'cart_item_count'     => number_format($cart_items),
                'cartsubtotal'        => '$'.$cart_sub_total,
                'carttotal'           => '$'.$cart_total,
                'gst'                 => '$'.$cart_tax
            );
            $data['woo_success_notice'] = $msg;
            //echo json_encode( $results );
        } 
        else {
            // If there was an error adding to the cart, redirect to the product page to show any errors
            /*$data = array(
                'error'       => true,
                'product_url' => apply_filters( 'woocommerce_cart_redirect_after_error', get_permalink( $product_id ), $product_id )
            );*/
            $all_notices  = WC()->session->get( 'wc_notices', array() );
            $error_msg = '';
            if( $all_notices ) {
                foreach( $all_notices as $key => $value ) {
                    if( $key == 'error' ) {
                        $error_msg =  $value[0];
                        if (substr_count( $error_msg, 'out of stock') > 0) {
                            $variation_obj = new WC_Product_variation( $variation_id );
                            $sku = $variation_obj->sku;
                            $error_msg = '<p>You cannot add "'.$sku.'" to the cart because the product is out of stock.</p>';
                        }
                    }
                }
            }
            wc_clear_notices();
            
            if( $error_msg != "" ) {
                $error_msg = $error_msg;
            } 
            else
                $error_msg = "<p>There is some issue. Please reload the page and process again.</p>";

            $data = array(
                'error'       => true,
                'woo_error_notice' => $error_msg
            );
        }
    }
    else {
        /*$passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity );
        $product_status    = get_post_status( $product_id );
        $prod_qty = get_post_meta($product_id,'_stock',true);
        //if ( $passed_validation && WC()->cart->add_to_cart( $product_id, $quantity ) && 'publish' === $product_status ) {
        if ( $passed_validation ) {

            //do_action( 'woocommerce_ajax_added_to_cart', $product_id );

            //wc_add_to_cart_message( $product_id );
            include('inc/ajax-quick-order-cart.php');
             global $woocommerce;
            $data = array(
                'success'       => true,
                'result'              => $cart_html,
                'cart_item_count'     => $woocommerce->cart->cart_contents_count,
                'cartsubtotal'        => $woocommerce->cart->get_cart_total(),
                'carttotal'           => $woocommerce->cart->get_cart_total()
            );

        } else {

            // If there was an error adding to the cart, redirect to the product page to show any errors
            $data = array(
                'error'       => true,
                'product_url' => apply_filters( 'woocommerce_cart_redirect_after_error', get_permalink( $product_id ), $product_id )
            );

            

        }*/
        $passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity );

        $product = wc_get_product( $product_id );
        $boxqty_val = $product->get_attribute( 'pa_boxqty' );

        //echo $boxqty_val = get_post_meta( $product_id, 'attribute_pa_boxqty', true );
        if( $boxqty_val != "" ) {
            $quantity = $quantity * $boxqty_val;
        }

        if ( $passed_validation && WC()->cart->add_to_cart( $product_id, $quantity  ) ) {
            do_action( 'woocommerce_ajax_added_to_cart', $product_id );
            if ( get_option( 'woocommerce_cart_redirect_after_add' ) == 'yes' ) {
            wc_add_to_cart_message( $product_id );}
            //WC_AJAX::get_refreshed_fragments();
            //$cart_items = WC()->cart->get_cart_contents_count();
            $cart_items = get_cart_qty();
            $product_name = get_the_title($product_id);
            $product = wc_get_product( $product_id );
            $sku = $product->get_sku();
            if( $sku != "" ) {
                $product_name = $sku;
            }

            include('inc/ajax-quick-order-cart.php');
            global $woocommerce;

            $cart_summary_total = WC()->cart->get_totals();
            $cart_sub_total = $cart_summary_total['cart_contents_total'];
            $cart_tax = $cart_summary_total['cart_contents_tax'];
            $cart_total = $cart_summary_total['total'];

            $msg = '<p>'.$product_name.' has been added to the cart!</p>';
            if( $product->get_stock_quantity() <=0 && $product->backorders_allowed() == true ) {            
                $msg .= get_field( 'back_order_text', 'options' );
            }

            $data = array(
                'success'       => true,
                'result'              => $cart_html,
                'cart_item_count'     => number_format($cart_items),
                'cartsubtotal'        => '$'.$cart_sub_total,
                'carttotal'           => '$'.$cart_total,
                'gst'                 => '$'.$cart_tax
            );
            $data['woo_success_notice'] = $msg;
            
        }
        else {
            $all_notices  = WC()->session->get( 'wc_notices', array() );
            $error_msg = '';
            if( $all_notices ) {
                foreach( $all_notices as $key => $value ) {
                    if( $key == 'error' ) {
                        $error_msg =  $value[0];
                        if (substr_count( $error_msg, 'out of stock') > 0) {
                            $product = wc_get_product( $product_id );
                            $sku = $product->get_sku();
                            $error_msg = '<p>You cannot add "'.$sku.'" to the cart because the product is out of stock.</p>';
                        }
                    }
                }
            }
            wc_clear_notices();
            
            if( $error_msg != "" ) {
                $error_msg = $error_msg;
            } 
            else
                $error_msg = "<p>There is some issue. Please reload the page and process again.</p>";

            $data = array(
                'error'       => true,
                'woo_error_notice' => $error_msg
            );
        }
    }
    
    /*$all_notices  = WC()->session->get( 'wc_notices', array() );
    $notice_types = apply_filters( 'woocommerce_notice_types', array( 'success', 'notice' ) );
    //var_dump($all_notices);
    if($all_notices) {
        //var_dump($all_notices);
        if(count($all_notices['error'])>0) {
            $error_msg = '';
            $checkduperror_ = '';
            foreach ($all_notices['error'] as $woo_error) {
                //echo strpos(strtolower($woo_error),'required field').'error';
                if(strpos($woo_error,'required field')===false) {
                    // do nothing
                    $checkduperror = $woo_error;
                    if( $checkduperror != $checkduperror_ )
                        $error_msg .= $woo_error;
                    $checkduperror_ = $checkduperror;
                }            
            }
            //var_dump($all_notices['success']);
            if($error_msg!="")
            {
                $error_msg = '<p>'.str_replace(array('View cart','View Cart','View Basket','View Basket'),array('','','',''),strip_tags($error_msg)).'</p>';
                //$data = array( 'woo_error_notice' => $error_msg, 'error' => true );
                $data['woo_error_notice'] = $error_msg;
                 $data['error'] = true;
            }
        }
        if(count($all_notices['success'])>0) {
            $success_msg = '';
            $checkdupsuccess_ = '';
            foreach ($all_notices['success'] as $woo_success) {
                
                $checkdupsuccess = $woo_success;
                if( $checkdupsuccess != $checkdupsuccess_ )
                    $success_msg .= $woo_success;
                $checkdupsuccess_ = $checkdupsuccess;
                           
            }
            //var_dump($all_notices['success']);
            if($success_msg!="")
            {
                if($prod_qty==0) {
                    $backorder_notice = get_field('back_order_text','options');
                    $backorder_notice = '<span class="prod-notes">'.$backorder_notice.'</span>';
                }
                else
                    $backorder_notice = '';
                $success_msg = '<p>'.str_replace(array('View Cart',"View cart",'View Basket','View basket'),array('','','',''),strip_tags($success_msg)).'</p>'.$backorder_notice;
                //$data = array( 'woo_error_notice' => $error_msg, 'error' => true );
                $data['woo_success_notice'] = $success_msg;
            }
        }
    }*/
    wc_clear_notices();
    
    wp_send_json( $data );
    die();

}

/* Ajax add to cart for product page - Simple and variable products */
add_action( 'wp_ajax_bodycommerce_ajax_add_to_cart_woo', 'df_ajax_add_to_cart_woo_variable_callback' );
add_action( 'wp_ajax_nopriv_bodycommerce_ajax_add_to_cart_woo', 'df_ajax_add_to_cart_woo_variable_callback' );

function df_ajax_add_to_cart_woo_variable_callback() {
    ob_start();
    $product_id = apply_filters( 'woocommerce_add_to_cart_product_id', absint( $_POST['product_id'] ) );
    $quantity = empty( $_POST['quantity'] ) ? 1 : apply_filters( 'woocommerce_stock_amount', $_POST['quantity'] );
    $variation_id = $_POST['variation_id'];
    $variation  = $_POST['variation'];
    $passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity );

    $boxqty_val = base64_decode($_POST['boxqty_val']);

    if( $boxqty_val != "" ) {
        $quantity = $quantity * $boxqty_val;
    }

    if ( $passed_validation && WC()->cart->add_to_cart( $product_id, $quantity, $variation_id, $variation  ) ) {
        do_action( 'woocommerce_ajax_added_to_cart', $product_id );
        if ( get_option( 'woocommerce_cart_redirect_after_add' ) == 'yes' ) {
        wc_add_to_cart_message( $product_id );}
        //WC_AJAX::get_refreshed_fragments();
        //$cart_items = WC()->cart->get_cart_contents_count();
        $cart_items = get_cart_qty();
        $product_name = get_the_title($product_id);
        /* Back order message */
        $variation_obj = new WC_Product_variation( $variation_id );
        $stock_qty = $variation_obj->get_stock_quantity();
        $sku = $variation_obj->sku;
            //var_dump($value);
        $backorder = $variation_obj->backorders;        
        
        $msg = '<p class="main-msg">'.$sku.' has been added to the cart!'.'</p>';
        if($backorder==='notify' && $stock_qty <= 0) {
            $msg .= get_field( 'back_order_text', 'options' );
        }
            

        $results = array(
            'success' => true,
            'message' => $msg,
            'cart_num' => number_format($cart_items) 
        );
        echo json_encode( $results );
    } 
    else {
        $all_notices  = WC()->session->get( 'wc_notices', array() );
        $error_msg = '';
        if( $all_notices ) {
            foreach( $all_notices as $key => $value ) {
                if( $key == 'error' ) {
                    $error_msg =  $value[0];
                    if (substr_count( $error_msg, 'out of stock') > 0) {
                        $variation_obj = new WC_Product_variation( $variation_id );
                        $sku = $variation_obj->sku;
                        $error_msg = 'You cannot add "'.$sku.'" to the cart because the product is out of stock.';
                    }
                }
            }
        }
        wc_clear_notices();
        
        if( $error_msg != "" ) {
            $error_msg = $error_msg;
        } 
        else
            $error_msg = "There is some issue. Please reload the page and process again.";

        $results = array(              
            'error' => true,                
            'error_msg' => $error_msg
        );
        echo json_encode( $results );
    }
    die();
}
        
add_action( 'wp_ajax_bodycommerce_ajax_add_to_cart_woo_single', 'df_ajax_add_to_cart_woo_single_callback' );
add_action( 'wp_ajax_nopriv_bodycommerce_ajax_add_to_cart_woo_single', 'df_ajax_add_to_cart_woo_single_callback' );
        
function df_ajax_add_to_cart_woo_single_callback() {
    ob_start();
    $product_id = apply_filters( 'woocommerce_add_to_cart_product_id', absint( $_POST['product_id'] ) );
    $quantity = empty( $_POST['quantity'] ) ? 1 : apply_filters( 'woocommerce_stock_amount', $_POST['quantity'] );
    $passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity );

    $boxqty_val = base64_decode($_POST['boxqty_val']);

    if( $boxqty_val != "" ) {
        $quantity = $quantity * $boxqty_val;
    }

    if ( $passed_validation && WC()->cart->add_to_cart( $product_id, $quantity  ) ) {
        do_action( 'woocommerce_ajax_added_to_cart', $product_id );
        if ( get_option( 'woocommerce_cart_redirect_after_add' ) == 'yes' ) {
        wc_add_to_cart_message( $product_id );}
        //WC_AJAX::get_refreshed_fragments();
        //$cart_items = WC()->cart->get_cart_contents_count();
        $cart_items = get_cart_qty();
        $product_name = get_the_title($product_id);
        $product = wc_get_product( $product_id );
        $sku = $product->get_sku();
        if( $sku != "" ) {
            $product_name = $sku;
        }

        $msg = $product_name.' has been added to the cart!';
        if( $product->get_stock_quantity() <=0 && $product->backorders_allowed() == true ) {            
            $msg .= get_field( 'back_order_text', 'options' );
        }

        
        $results = array(
            'success' => true,
            'message' => '<p class="main-msg">'.$msg.'</p>',
            'cart_num' => number_format($cart_items)
        );
        echo wp_send_json( $results );
    } 
    else {
        $all_notices  = WC()->session->get( 'wc_notices', array() );
        $error_msg = '';
        if( $all_notices ) {
            foreach( $all_notices as $key => $value ) {
                if( $key == 'error' ) {
                    $error_msg =  $value[0];
                    if (substr_count( $error_msg, 'out of stock') > 0) {
                        $product = wc_get_product( $product_id );
                        $sku = $product->get_sku();
                        $error_msg = 'You cannot add "'.$sku.'" to the cart because the product is out of stock.';
                    }
                }
            }
        }
        wc_clear_notices();
        
        if( $error_msg != "" ) {
            $error_msg = $error_msg;
        } 
        else
            $error_msg = "There is some issue. Please reload the page and process again.";

        $results = array(              
            'error' => true,                
            'error_msg' => $error_msg
        );
        echo json_encode( $results );
    }
    die();
}

/* End End*/

/* 
 *WooComm Registration 
**/
function df_woocomm_extra_register_fields() {
    global $woocommerce;
    $checkout = $woocommerce->checkout();
    foreach ($checkout->checkout_fields['billing'] as $key => $field) :
        if($key!=='billing_email')
            woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
    endforeach;
}
add_action( 'woocommerce_register_form_start', 'df_woocomm_extra_register_fields' );

/**
* register fields Validating.
*/
function wooc_validate_extra_register_fields( $errors, $username, $email ) {
    if ( isset( $_POST['billing_first_name'] ) && empty( $_POST['billing_first_name'] ) ) {
        $errors->add( 'billing_first_name_error', __( 'First name is required!', 'woocommerce' ) );
    }
    if ( isset( $_POST['billing_last_name'] ) && empty( $_POST['billing_last_name'] ) ) {
        $errors->add( 'billing_last_name_error', __( 'Last name is required!.', 'woocommerce' ) );
    }
    if ( isset( $_POST['billing_company'] ) && empty( $_POST['billing_company'] ) ) {
        $errors->add( 'billing_company_error', __( 'Company name is required!.', 'woocommerce' ) );
    }
    if ( isset( $_POST['billing_phone'] ) && empty( $_POST['billing_phone'] ) ) {
        $errors->add( 'billing_phone_error', __( 'Phone is required!.', 'woocommerce' ) );
    }
    else if ( ! (preg_match('/^[0-9]{10}$/D', $_POST['billing_phone'] )) && ! (preg_match('/^[0-9]{11}$/D', $_POST['billing_phone'] )) ){
        //$errors->add( "Please enter valid phone number"  ,'error' );
        $errors->add( 'billing_phone_error', __( 'Please enter valid phone number!.', 'woocommerce' ) );
    }
    if ( isset( $_POST['billing_address_1'] ) && empty( $_POST['billing_address_1'] ) ) {
        $errors->add( 'billing_address_1_error', __( 'Addres is required!.', 'woocommerce' ) );
    }
    if ( isset( $_POST['billing_city'] ) && empty( $_POST['billing_city'] ) ) {
        $errors->add( 'billing_city_error', __( 'Suburb is required!.', 'woocommerce' ) );
    }
    if ( isset( $_POST['billing_state'] ) && empty( $_POST['billing_state'] ) ) {
        $errors->add( 'billing_state_error', __( 'State is required!.', 'woocommerce' ) );
    }
    if ( isset( $_POST['billing_postcode'] ) && empty( $_POST['billing_postcode'] ) ) {
        $errors->add( 'billing_postcode_error', __( 'Postcode is required!.', 'woocommerce' ) );
    }
    return $errors;
}
add_filter( 'woocommerce_process_registration_errors', 'wooc_validate_extra_register_fields', 10, 3 );

/**
* Below code save extra fields.
*/
function df_wooc_save_extra_register_fields( $customer_id ) {
    if ( isset( $_POST['billing_phone'] ) ) {
                 // Phone input filed which is used in WooCommerce
                 update_user_meta( $customer_id, 'billing_phone', sanitize_text_field( $_POST['billing_phone'] ) );
          }
      if ( isset( $_POST['billing_first_name'] ) ) {
             //First name field which is by default
             update_user_meta( $customer_id, 'first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
             // First name field which is used in WooCommerce
             update_user_meta( $customer_id, 'billing_first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
      }
      if ( isset( $_POST['billing_last_name'] ) ) {
             // Last name field which is by default
             update_user_meta( $customer_id, 'last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
             // Last name field which is used in WooCommerce
             update_user_meta( $customer_id, 'billing_last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
      }

      if ( isset( $_POST['billing_company'] ) ) {
            // Company input filed which is used in WooCommerce
            update_user_meta( $customer_id, 'billing_company', sanitize_text_field( $_POST['billing_company'] ) );
      }

      if ( isset( $_POST['billing_country'] ) ) {
            // Country input filed which is used in WooCommerce
            update_user_meta( $customer_id, 'billing_country', sanitize_text_field( $_POST['billing_country'] ) );
      }

      if ( isset( $_POST['billing_address_1'] ) ) {
            // Address 1 input filed which is used in WooCommerce
            update_user_meta( $customer_id, 'billing_address_1', sanitize_text_field( $_POST['billing_address_1'] ) );
      }

      if ( isset( $_POST['billing_address_2'] ) ) {
            // Address 2 input filed which is used in WooCommerce
            update_user_meta( $customer_id, 'billing_address_2', sanitize_text_field( $_POST['billing_address_2'] ) );
      }

      if ( isset( $_POST['billing_city'] ) ) {
            // Suburb input filed which is used in WooCommerce
            update_user_meta( $customer_id, 'billing_city', sanitize_text_field( $_POST['billing_city'] ) );
      }

      if ( isset( $_POST['billing_state'] ) ) {
            // State input filed which is used in WooCommerce
            update_user_meta( $customer_id, 'billing_state', sanitize_text_field( $_POST['billing_state'] ) );
      }

      if ( isset( $_POST['billing_postcode'] ) ) {
            // Postcode input filed which is used in WooCommerce
            update_user_meta( $customer_id, 'billing_postcode', sanitize_text_field( $_POST['billing_postcode'] ) );
      }

      if ( isset( $_POST['billing_how_did_you_hear'] ) ) {
              // WooCommerce billing phone

              update_user_meta( $customer_id, 'billing_how_did_you_hear', sanitize_text_field( $_POST['billing_how_did_you_hear'] ) );
       }

        /*$u = new WP_User( $customer_id );
        $u->add_role('credit-account');*/

}
add_action( 'woocommerce_created_customer', 'df_wooc_save_extra_register_fields' );

function get_excerpt_by_word($the_excerpt_p,$wc=15) {
    $excerpt_length = $wc; //Sets excerpt length by word count

    $the_excerpt = strip_tags(strip_shortcodes($the_excerpt_p)); //Strips tags and images

    $words = explode(' ', $the_excerpt, $excerpt_length + 1);

    if (count($words) > $excerpt_length) :

        array_pop($words);

        array_push($words, '…');

        $the_excerpt = implode(' ', $words);

    endif;

    return $the_excerpt;
}

add_filter('posts_where', 'df_posts_where');
function df_posts_where( $where )
{
    if(isset($_GET['term'])) {
        $where = '';
        $term = trim($_GET['term']);
        
        //$where .= " AND (post_title like '%$term%' OR post_content like '%$term%')";
        //$where = " AND wpdufa_posts.post_type = 'product' AND ((wpdufa_posts.post_status = 'publish')) AND (post_title like '%$term%' OR post_content like '%$term%' OR ( wpdufa_postmeta.meta_key = '_sku' AND wpdufa_postmeta.meta_value = '$term' ))";
        $where = " AND wpdufa_posts.post_type = 'product' AND ((wpdufa_posts.post_status = 'publish')) AND (post_title like '%$term%' OR post_content like '%$term%' OR ( wpdufa_postmeta.meta_key = '_sku' AND wpdufa_postmeta.meta_value = '$term' ))";
    }
    //var_dump($where);
    
    return $where;
}

add_filter('posts_orderby', 'df_posts_orderby');
function df_posts_orderby($orderby_statement) {
    if(isset($_GET['term'])) {
        $term = trim($_GET['term']);
        $orderby_statement = " wpdufa_posts.post_title LIKE '%$term%' DESC, wpdufa_postmeta.meta_value LIKE '%$term%' DESC";
    }    
    return $orderby_statement;
}

function my_acf_init()
{
    acf_update_setting('select2_version', 4);
}
add_action('acf/init', 'my_acf_init');

add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

function searchProductOnly($query) {
    if ($query->is_search) {
    $query->set('post_type', 'product');
    }
    return $query;
}
 
add_filter('pre_get_posts','searchProductOnly');

function change_wp_search_size($query) {
    if ( $query->is_search ) {// Make sure it is a search page
        if( isset($_GET['sterm']) && $_GET['sterm']!='' )
            $query->query_vars['posts_per_page'] = 100;
        else
            $query->query_vars['posts_per_page'] = 20; // Change 10 to the number of posts you would like to show
    }

    return $query; // Return our modified query variables
}
add_filter('pre_get_posts', 'change_wp_search_size'); // Hook our custom function onto the request filter

function df_stock_info_error($error){
    global $woocommerce;
    foreach ($woocommerce->cart->cart_contents as $item) {
        $product_id = isset($item['variation_id']) ? $item['variation_id'] : $item['product_id'];
        $product = new \WC_Product_Factory();
        $product = $product->get_product($product_id);

        if ($item['quantity'] > $product->get_stock_quantity()){
            $name = $product->get_name();
            $error = 'Sorry, we do not have enough "'.$name.'" in stock to fulfill your order. Please edit your cart and try again. We apologize for any inconvenience caused.';
            return $error;
        }
    }
}
//add_filter( 'woocommerce_add_error', 'df_stock_info_error' );

//add_filter( 'wc_password_strength_meter_params', 'df_strength_meter_settings' );
 
function df_strength_meter_settings( $data ) {
 
    return array_merge( $data, array(
        'min_password_strength' => 1,
        'i18n_password_hint' => 'Please make your password <strong>at least 8 characters</strong> long and use a mix of <strong>UPPER</strong> and <strong>lowercase</strong> letters, <strong>numbers</strong>.'
    ) );
 
}

add_filter( 'gettext', 'df_change_text', 20, 3 );

/**
 * Change Shipping to Delivery.
 *
 * @link http://codex.wordpress.org/Plugin_API/Filter_Reference/gettext
 */
function df_change_text( $translated_text, $text, $domain ) {

    if ( is_singular() ) {

        switch ( $translated_text ) {

            case 'Shipping' :

                $translated_text = __( 'Delivery', 'durofast' );
                break;

            case 'Shipping address' :

                $translated_text = __( 'Delivery Address', 'durofast' );
                break;

            case 'Additional Shipping Addresses' :

                $translated_text = __( 'Delivery Addresses', 'durofast' );
                break;

            case 'Add new shipping address' :

                $translated_text = __( 'Add new delivery address', 'durofast' );
                break;

            case 'Wishlists' :

                $translated_text = __( 'Everyday Items', 'durofast' );
                break;

            case 'Wishlist' :

                $translated_text = __( 'Everyday Items', 'durofast' );
                break;

            case 'The Regulars' :

                $translated_text = __( 'Everyday Items', 'durofast' );
                break;

            case 'Create a wish list' :

                $translated_text = __( 'Create a list', 'durofast' );
                break;

            case 'out of stock' :

                $translated_text = __( 'pre order now', 'durofast' );
                break;

            case 'Related products' :
                $translated_text = __( 'You Might Also Like...', 'durofast' );
                break;

            case 'Add to wishlist' :
                $translated_text = __( 'Add to a List', 'durofast' );
                break;

            case 'Go shopping to create one.' :
                $translated_text = __( '', 'durofast' );
                break;

            case 'Proceed to checkout' :
                $translated_text = __( 'Proceed to Checkout', 'durofast' );
                break;

            case 'Select an address' :
                $translated_text = __( 'Select from address book', 'durofast' );
                break;

            case 'Latest Used Addresses' :
                $translated_text = __( 'Company Addresses', 'durofast' );
                break;

            case 'Ship to a different address?' :
                $translated_text = __( 'Deliver to a different address?', 'durofast' );
                break;

            case 'Available on backorder' :
                $translated_text = __( 'On Back Order', 'durofast' );
                break;

            case 'No order has been made yet.' :
                $translated_text = __( "You haven't placed any orders yet.", 'durofast' );
                break;

            case 'Go shop' :
                $translated_text = __( "Return to Product List", 'durofast' );
                break;

            case 'Account details' :
                $translated_text = __( "Account Details", 'durofast' );
                break;

            case 'Add new address' :
                $translated_text = __( "Add New Address", 'durofast' );
                break;

            case 'Add to basket' :
                $translated_text = __( "Add to Basket", 'durofast' );
                break;

            case 'Select options' :
                $translated_text = __( "Select Options", 'durofast' );
                break;

            case 'Manage this list' :
                $translated_text = __( "Manage List", 'durofast' );
                break;
            case 'Go Shopping' :
                $translated_text = __( "Return to Product List", 'durofast' );
                break;
            case 'Wishlist successfully deleted' :
                $translated_text = __( "Everyday Items List successfully deleted", 'durofast' );
                break;
            case 'Select a country' :
                $translated_text = __( "Select a Country", 'durofast' );
                break;
            case 'Basket' :
                $translated_text = __( "Cart", 'durofast' );
                break;
            case 'basket' :
                $translated_text = __( "cart", 'durofast' );
                break;
            case 'Wishlist successfully created' :
                $translated_text = __( "Everyday Items List successfully created", 'durofast' );
                break;
            case 'Wishlist successfully updated' :
                $translated_text = __( "Everyday Items List successfully updated", 'durofast' );
                break;
            case 'This item is already in one of your wishlists' :
                $translated_text = __( "It's on the List", 'durofast' );
                break;
            case 'Create a new list' :
                $translated_text = __( "Create a New List", 'durofast' );
                break;
            case 'Billing address' :
                $translated_text = __( "Billing Address", 'durofast' );
                break;
            case 'Order details' :
            $translated_text = __('Order Details');
                break;
            case 'Customer details' :
            $translated_text = __('Customer Details');
                break;
            case 'Username or email address' :
            $translated_text = __('Username (Email Address)');
                break;
            case 'Create a Wish List' :
            $translated_text = __('Create a List');
                break;
            case 'There is no list' :
            $translated_text = __('There is no lists.');
                break;
            case 'Payment method:' :
            $translated_text = __('Payment Method:');
                break;
            case 'Basket updated.' :
            $translated_text = __('Cart updated.');
                break;
            case 'Select from address book' :
            $translated_text = __('Address');
                break;
            case 'Shipping address used for the previous order' :
            $translated_text = __('Select an Address from the list');
                break;
        }

    }

    if ( is_checkout() || is_cart() || is_page_template( 'template-quick-order.php' ) ) {
        switch ( $translated_text ) {
            case 'Subtotal' :
            $translated_text = __('Subtotal excl. GST');
            break;

            case 'Total' :
            $translated_text = __('Total incl. GST');
            break;

            case 'Postcode / ZIP' :
            $translated_text = __('Postcode');
            break;
        }
    }

    if( is_account_page() || is_wc_endpoint_url( 'lost-password' )) {
        switch ( $translated_text ) {
            case 'Save address' :
            $translated_text = __('Save Address');
            break;
            case 'Save changes' :
            $translated_text = __('Save Changes');
            break;

            case 'Postcode / ZIP' :
            $translated_text = __('Postcode');
            break;

            case 'Username or email' :
            $translated_text = __('Email');
            break;
        }
    }
    else {
        switch ( $translated_text ) {
            case 'Your Private Lists' :
            $translated_text = __('Your Everyday Lists');
            break;
        }
    }

    if ( is_page( 'edit-my-list' ) && isset( $_GET['wlid'] ) ) {
        switch ( $translated_text ) {
            case 'Price' :
            $translated_text = __('Unit Price');
            break;
            case 'Qty' :
            $translated_text = __('Select Box/es');
            break;
        }
    }

    return $translated_text;
}

add_filter( 'woocommerce_breadcrumb_defaults', 'df_change_breadcrumb' );
function df_change_breadcrumb( $defaults ) {
    // Change the breadcrumb home text from 'Home' to 'Apartment'
    if ( is_account_page() && $_SERVER['REQUEST_URI'] == '/my-account/' || strpos( $_SERVER['REQUEST_URI'], '/my-account/' ) !== false) { // User is NOT on my account pages
        global $current_user;
        if ( is_user_logged_in() ) { // Logged in user
            //echo 'Maya';
            //var_dump($current_user);
            $user_display_name = explode(' ', $current_user->first_name);
            //$crumbs[1][0] = 'Welcome, '.$user_display_name[0].'!';
            $defaults['wrap_after'] = '<div class="welcome-msg">Welcome, '.$user_display_name[0].'! </div></nav>';
        }
    }
    return $defaults;
}

add_filter( 'woocommerce_get_breadcrumb', 'change_breadcrumb', 15 );
function change_breadcrumb( $crumbs ) {
    //var_dump( $crumbs );
    /*print_r("<pre style='margin-left:50px'>");
    print_r($crumbs);
    print_r("</pre>");*/
    if (strpos($_SERVER['REQUEST_URI'],'/my-lists/') !== false) {
        //echo 'Car exists.';
        if( $crumbs[1][0] == 'Wishlists' || $crumbs[1][0] == 'The Regulars' )
            $crumbs[1][0] = 'Everyday Items';
    }
    // if ( $_SERVER['REQUEST_URI'] == '/my-account/') { // User is NOT on my account pages

    //     if ( is_user_logged_in() ) { // Logged in user
    //         //echo 'Maya';
    //         //var_dump($current_user);
    //         $user_display_name = explode(' ', $current_user->first_name);
    //         $crumbs[1][0] = 'Welcome, '.$user_display_name[0].'!';
    //     }
    // }
    //var_dump($crumbs);
    if( is_post_type_archive('post') || is_singular( 'post' ) ) {
        if( is_archive() )
            $current_title = str_replace( 'Month:', '', get_the_archive_title() );
        else
            $current_title = get_the_title();
        global $wp;

        $current_url = home_url(add_query_arg(array(),$wp->request));
        $new_crumbs = array();
        if($crumbs) {
            $blog_title = get_the_title( get_option('page_for_posts', true) );
            $blog_link = get_permalink( get_option( 'page_for_posts' ) ); 
            $count = 1;
            foreach( $crumbs as $crumbs_val) {
                    if( $count == 2 )
                    {
                        $new_crumbs[] = array( $blog_title, $blog_link );
                    }
                    else if( $count == 3 ) {
                        $new_crumbs[] = array( $current_title, $current_url );
                    }
                    else
                        $new_crumbs[] = $crumbs_val;
                $count++;
            }
            //var_dump($new_crumbs);
            if($new_crumbs) {
                $crumbs = array();
                $crumbs = $new_crumbs;
            }
        }
    }
    //var_dump($_SERVER);
    if( strpos($_SERVER['REQUEST_URI'],'/my-lists') !== false ) {
        $crumbs_everyday = array();
        foreach( $crumbs as $crumbs_val ) {
            if( in_array('Everyday Items', $crumbs_val) ) {
                $myaccount_page_id = get_option( 'woocommerce_myaccount_page_id' );
                if ( $myaccount_page_id ) {
                  $myaccount_page_url = get_permalink( $myaccount_page_id );
                }
                $crumbs_everyday[] = array( $crumbs_val[0], $myaccount_page_url.'everyday-items/' );
            }
            else
                $crumbs_everyday[] = $crumbs_val;
        }
        if($crumbs_everyday) {
            $crumbs = array();
            $crumbs = $crumbs_everyday;
        }
        
    }
    return $crumbs;
}

add_filter( 'woocommerce_product_tabs', 'df_remove_product_tabs', 15 );
 
function df_remove_product_tabs( $tabs ) {
    unset( $tabs['description'] );          // Remove the description tab
    unset( $tabs['reviews'] );          // Remove the reviews tab
    unset( $tabs['additional_information'] );   // Remove the additional information tab
    return $tabs;
}

add_filter( 'woocommerce_output_related_products_args', 'df_related_products_args' );
function df_related_products_args( $args ) {
    $args['posts_per_page'] = 4; // 4 related products
    return $args;
}

//add_filter( 'woocommerce_get_availability', 'df_custom_get_availability', 20, 2);
function df_custom_get_availability( $availability, $_product ) {
    
    // Change Out of Stock Text
    if ( ! $_product->is_in_stock() ) {
        $availability['availability'] = __('Sold Out', 'woocommerce');
    }
    var_dump($_product);
    echo 'hahamaya';
    return $availability;
}

add_filter('woocommerce_sale_flash', 'my_custom_sale_flash', 10, 3);
function my_custom_sale_flash($text, $post, $_product)
{
    
    /*$product_categories = get_the_terms( $_product->ID, 'product_cat' );
    if($product_categories) {
        foreach($product_categories as $product_categories_row) {
            if( strtolower ( $product_categories_row->name) == 'clearance' ) {
                $sale_text = 'Clearance';
            }
        }
    }*/
    $c_text = checkClearance($_product->ID);
    if($c_text!="") {
        $sale_text = $c_text;
        $cls = 'clearance';
    }
    else {
        $sale_text = 'On Sale';
        $cls = '';
    }
    return '<span class="onsale '.$cls.'">'.$sale_text.'</span>';
}

add_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_clearance', 25 );
function woocommerce_show_product_clearance() {
    //echo 'Mayamaya';
    global $product;
    // Get products on sale
    //$product_ids_on_sale = array_filter( wc_get_product_ids_on_sale() ); // woocommerce_get_product_ids_on_sale() if WC < 2.1
    if( !$product->is_on_sale() ) {
        $c_text = checkClearance($product->ID);
        if($c_text!="")
            echo '<span class="clearance-sale">'.$c_text.'</span>';
    }    
}

function checkClearance($prod_id) {
    $sale_text  = '';
    $product_categories = get_the_terms( $prod_id, 'product_cat' );
    if($product_categories) {
        foreach($product_categories as $product_categories_row) {
            if( strtolower ( $product_categories_row->name) == 'clearance' ) {
                $sale_text = 'Clearance';
            }
        }
    }
    return $sale_text;
}

/* Show Cross sell product on product page*/
add_action( 'woocommerce_after_single_product_summary', 'woocommerce_cross_sell_display' );

/* Remove Cross-Sells from the shopping cart page*/
remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display');

/* Remove related product from product page */
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );


// change return to shop button text on cart page
add_filter( 'gettext', 'change_woocommerce_return_to_shop_text', 20, 3 );

function change_woocommerce_return_to_shop_text( $translated_text, $text, $domain ) {

    switch ( $translated_text ) {

        case 'Return to shop' :

            $translated_text = __( 'Return to Products', 'woocommerce' );
            break;

    }

    return $translated_text;
}

// create blog post type
/*function create_blog()
{
    register_post_type('blog', array(
        'label' => 'Blog',
        'description' => 'Used for Blog',
        'public' => true,
        'exclude_from_search' => false,
        'show_in_nav_menus' => true,
        'menu_position' => 5,
        'show_in_menu' => true,
        'show_ui' => true,
        'hierarchical' => true,
        'supports' => array(
            'title', 'revisions', 'author', 'page-attributes'
        ),
        'rewrite' => array(
            'slug' => 'blog',
            'with_front' => false
        ),
        'has_archive' => true,
        'pages' => true,
        'taxonomies' => array('category')
    ));
}

add_action('init', 'create_blog');*/
if( isset($_GET['wlid']) && $_GET['wlaction']=='add-cart-item' && isset($_GET['wishlist-item-key']) ) {
    // do nothing
}
else 
    add_filter( 'wc_add_to_cart_message', 'df_custom_add_to_cart_message', 15, 2 );
 
function df_custom_add_to_cart_message( $message, $product_id ) {
 
    global $woocommerce;
    $product_info = wc_get_product( $product_id );
    //var_dump($_POST);
    if(isset($_POST['variation_id']) && $_POST['variation_id']!="") {
        $prod_info = wc_get_product($_POST['variation_id']); 
    }
    else
    {
        $prod_info = wc_get_product($_POST['product_id']);
    }
    //var_dump($prod_info);
    $backorder = $prod_info->backorders;
    if($backorder=='notify' && $prod_info->stock_quantity <= 0) {
        $message = get_field( 'back_order_text', 'options' );
    }
    else
       $message = sprintf('%s', __( $message, 'woocommerce' ) );


    $return_to  = get_permalink(woocommerce_get_page_id('cart'));
    $message = str_replace('basket', 'cart', $message);
    return $message;
}

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page('General Settings');

}

function show_cart_sidebar() {
    global $woocommerce;  
    $subtotal = $woocommerce->cart->get_cart_subtotal();
    $total = $woocommerce->cart->get_cart_total();
    ?>
    <div class="summary-wrapper">
        <!-- <div class="cart_totals">
            Total <span><?php //echo $total; ?></span>
        </div>
        <div class="wc-proceed-to-checkout">
            <a href="<?php //echo wc_get_checkout_url(); ?>" class="checkout-button button alt wc-forward">
    Proceed to Checkout</a>
        </div> -->
        <div class="continue-shopping">
            <a href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' ) ); ?>" class="button alt wc-backward">Return to Product List</a>
        </div>
    </div>    
    <?php
    /*if(is_user_logged_in()) {
    ?>
        <div class="cart-login">
            <h2>New Customer</h2>
            <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" class="button btn normal">Sign Up</a>
            <span>or</span>
            <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" class="button btn normal"> Login In</a>
        </div>
    <?php
    }*/
}

//add_filter( 'woocommerce_thankyou_order_received_text', 'df_woo_thankyou' );
function df_woo_thankyou() {
    $added_text = '';
    return $added_text ;
}

/* Blog functions */
if ( ! function_exists( 'blog_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function blog_posted_on() {

    // Get the author name; wrap it in a link.
    $byline = sprintf(
        /* translators: %s: post author */
        __( 'by %s', 'cruise-russia' ),
        '<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . get_the_author() . '</a></span>'
    );

    // Finally, let's write all of this to the page.
    echo '<span class="posted-on">' . blog_time_link() . '</span>';
}
endif;

if ( ! function_exists( 'blog_time_link' ) ) :
/**
 * Gets a nicely formatted string for the published date.
 */
function blog_time_link() {
    $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
    /*if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
        $time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
    }*/

    $time_string = sprintf( $time_string,
        get_the_date( DATE_W3C ),
        get_the_date(),
        get_the_modified_date( DATE_W3C ),
        get_the_modified_date()
    );

    // Wrap the time string in a link, and preface it with 'Posted on'.
    return sprintf(
        /* translators: %s: post date */
        __( '<span class="screen-reader-text">Posted on</span> %s', 'cruise-russia' ),
        '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
    );
}
endif;

// Get the Excerpt Automatically Using the string
function get_excerpt_by_str($the_excerpt) {
    
    $excerpt_length = 25; //Sets excerpt length by word count
    
    $the_excerpt = strip_tags(strip_shortcodes($the_excerpt)); //Strips tags and images
    
    $words = explode(' ', $the_excerpt, $excerpt_length + 1);
    
    if (count($words) > $excerpt_length) :
    
    array_pop($words);
    
    array_push($words, '…');
    
    $the_excerpt = implode(' ', $words);
    
    endif;
    
    return $the_excerpt;
}

/* End blog functions */

if (function_exists('add_image_size')) {
    add_image_size('blogimg', 250, 235, false);
    add_image_size('team_pp', 224, 99999, false);
}

function add_clearance_text() {

    echo '<span class="clearance">clearance</span>';
}
//add_action( 'woocommerce_before_shop_loop_item_title', 'sv_add_logo_above_wc_shop_image', 9 );
function df_template_loop_product_title( $prod_title, $prod_link ) {
    $title = '<h2 class="woocommerce-loop-product__title"><a href="'.$prod_link.'" title="'.$prod_title.'">'.$prod_title.'</a></h2>';
    return $title;
}

/**

* Add new register fields for WooCommerce registration.

*

* @return string Register fields HTML.

*/

function df_wooc_extra_register_fields() {
       ?> 

       <p class="form-row">

       <label for="reg_billing_last_name"><?php _e( 'How did you hear about us?', 'woocommerce' ); ?><span class="required">*</span></label>
       <textarea name="billing_how_did_you_hear" class="input-text " id="reg_billing_how_did_you_hear" rows="2" cols="5"><?php if ( ! empty( $_POST['billing_how_did_you_hear'] ) ) esc_attr_e( $_POST['billing_how_did_you_hear'] ); ?></textarea>

       </p>
       <div class="clear"></div>
<?php
}
add_action( 'woocommerce_register_form_start', 'df_wooc_extra_register_fields' );

/**
* Validate the extra register fields.

*
* @param string $username         Current username.
* @param string $email             Current email.
* @param object $validation_errorsWP_Error object.
*
* @return void
*/
function df_wooc_validate_extra_register_fields( $username, $email, $validation_errors ) {

       if ( isset( $_POST['billing_how_did_you_hear'] ) && empty( $_POST['billing_how_did_you_hear'] ) ) {

              $validation_errors->add( 'billing_how_did_you_hear_error', __( '<strong>Error</strong>: "How did you hear about us" is required!', 'woocommerce' ) );

       }

}
add_action( 'woocommerce_register_post', 'df_wooc_validate_extra_register_fields', 10, 3 );

// Hooks near the bottom of profile page (if current user) 
add_action('show_user_profile', 'custom_user_profile_fields');

// Hooks near the bottom of the profile page (if not current user) 
add_action('edit_user_profile', 'custom_user_profile_fields');

// @param WP_User $user
function custom_user_profile_fields( $user ) {
?>
    <table class="form-table">
        <tr>
            <th>
                <label for="reg_billing_how_did_you_hear"><?php _e( 'How did you hear about us' ); ?></label>
            </th>
            <td>
                <input type="text" name="billing_how_did_you_hear" id="reg_billing_how_did_you_hear" value="<?php echo esc_attr( get_the_author_meta( 'billing_how_did_you_hear', $user->ID ) ); ?>" class="regular-text" disabled="disabled" />
            </td>
        </tr>
    </table>
<?php
}


// Hook is used to save custom fields that have been added to the WordPress profile page (if current user) 
add_action( 'personal_options_update', 'update_extra_profile_fields' );

/*STEP 1 - REMOVE ADD TO CART BUTTON ON PRODUCT ARCHIVE */

function df_remove_loop_button(){
    remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
}
add_action('init','df_remove_loop_button');



/*STEP 2 -ADD NEW BUTTON THAT LINKS TO PRODUCT PAGE FOR EACH PRODUCT */

add_action('woocommerce_after_shop_loop_item','df_replace_add_to_cart');
function df_replace_add_to_cart() {
    global $product;
    $link = $product->get_permalink();
    //echo do_shortcode('<br>[button link="' . esc_attr($link) . '"]View[/button]');
    echo '<a rel="nofollow" href="' . esc_attr($link) . '" class="button product_type_variable add_to_cart_button">View</a>';
}

/* Calcuate shipping rate */
add_filter( 'woocommerce_package_rates', 'custom_package_rates', 15, 2 );
function custom_package_rates( $rates, $package ) {
    /*var_dump($rates);
    var_dump($package);
    $total = WC()->cart->cart_contents_total;

    if( 300 < $total ) {

        unset( $rates['flat_rate'] );
        unset( $rates['free_shipping'] );

    } elseif ( 301 >= $total && 1000 <= $total ) {

        unset( $rates['local_delivery'] );
        unset( $rates['free_shipping'] );

    }*/
    
    // etc add the remaining condition

    return $rates;
} 

function checkProductInWishlist( $wishlist_arr, $variation_id, $option ) {
    
    $check_list = false;
    if( $wishlist_arr ) {
        $prod_ids = array();
        foreach( $wishlist_arr as $wishlist_arr_val ) {
            $wishlist_prods = get_post_meta( $wishlist_arr_val, '_wishlist_items' );
            
            if($wishlist_prods[0]) {
                
                foreach( $wishlist_prods[0] as $all_prod ) {
                    if( $option == 'simple' )
                        $prod_ids[] = $all_prod['product_id'];
                    else
                        $prod_ids[] = $all_prod['variation_id'];
                }
                
            }
        }
        if($prod_ids && in_array($variation_id, $prod_ids))
            $check_list = true;
    }
    return $check_list;
}
function register_my_session(){
  if( !session_id() ){
    session_start();
  }
}

add_action('init', 'register_my_session');

/* Re arrange the menu order in my account page */
function df_woo_account_order($menu) {
    $count = 1;
    $new_menu = array();
    foreach($menu as $key => $menuname) {
        if( $key != 'downloads' ) {
            if( $count == 3 ) {
                $new_menu['everyday-items'] = 'Everyday Items';
                $new_menu[$key] = $menuname;
            }
            else if($key!='account-wishlists')
                $new_menu[$key] = $menuname;
            $count++;
        }
    }
    if($new_menu)
        $menu = $new_menu;

    return $menu;
}
add_filter ( 'woocommerce_account_menu_items', 'df_woo_account_order', 20 );

add_filter('next_post_link', 'df_post_link_attributes');
add_filter('previous_post_link', 'df_post_link_attributes');

function df_post_link_attributes($output) {
    $code = 'class="btn normal"';
    return str_replace('<a href=', '<a '.$code.' href=', $output);
}

/*
 * URL rewrite
 */
function df_custom_url_rewrite( $wp_rewrite ) {
    $wp_rewrite->rules = array(
        'my-account/everyday-items/?$' => 'index.php?page_id=8&account-wishlists'
    ) + $wp_rewrite->rules;
    
    return $wp_rewrite->rules;
    
}
add_action( 'generate_rewrite_rules', 'df_custom_url_rewrite', 15 );

/*
 * Enable ship to different address
 */
add_filter( 'woocommerce_ship_to_different_address_checked', '__return_true' );


add_action('woocommerce_after_checkout_validation', 'df_deny_pobox_postcode');

function df_deny_pobox_postcode( $posted ) {
  global $woocommerce;

  $address  = ( isset( $posted['shipping_address_1'] ) ) ? $posted['shipping_address_1'] : $posted['billing_address_1'];
  $postcode = ( isset( $posted['shipping_postcode'] ) ) ? $posted['shipping_postcode'] : $posted['billing_postcode'];

  $replace  = array(" ", ".", ",");
  $address  = strtolower( str_replace( $replace, '', $address ) );
  $postcode = strtolower( str_replace( $replace, '', $postcode ) );
  if( !isset( $posted['shipping_option_pickup'] ) ) {
    if ( strstr( $address, 'pobox' ) || strstr( $postcode, 'pobox' ) ) {
        wc_add_notice( sprintf( __( "Sorry, we do not deliver to PO BOX addresses.") ) ,'error' );
    }
  }
  
}

/*add_filter('request', function( $vars ) {
    global $wpdb;

    if(isset($vars['error'])){
        $lastVar = basename($_SERVER['REQUEST_URI']);
        $vars['attachment'] = $lastVar;
    }
});*/

/**
 * Add the field to the checkout page
 */
add_action('woocommerce_before_order_notes', 'df_customise_checkout_field');
 
function df_customise_checkout_field($checkout)
{
    echo '<div id="customise_checkout_field">';
    woocommerce_form_field('shipping_option_pickup', array(
        'type' => 'checkbox',
        'class' => array(
            'my-field-class form-row-wide'
        ) ,
        'label' => __('Pick up in store (Please allow 1 hour for your order to be processed)') ,
        'placeholder' => '' ,
        'required' => false,
    ) , $checkout->get_value('shipping_option_pickup'));
    echo '</div>';
}

/**
 * Update value of field
 */
 
add_action('woocommerce_checkout_update_order_meta', 'df_customise_checkout_field_update_order_meta');
 
function df_customise_checkout_field_update_order_meta($order_id)
{
    if (!empty($_POST['shipping_option_pickup'])) {
        $delivery_method = 'Customer Pick-up';
    }
    else
        $delivery_method = 'Delivery by Courier';

    update_post_meta($order_id, 'delivery_method', sanitize_text_field($delivery_method));
}

/* Phone number format */
add_filter( 'gform_phone_formats', 'df_phone_format' );
function df_phone_format( $phone_formats ) {
    $phone_formats['durofast'] = array(
        'label'       => '#### ### ###',
        'mask'        => '9999 999 999',
        'regex'       => '/^\D?(\d{4})\D?\D?(\d{3})\D?(\d{3})$/',
        'instruction' => '#### ### ###',
    );
 
    return $phone_formats;
}

/**
 * @snippet       Add First & Last Name to My Account Register Form - WooCommerce
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @sourcecode    https://businessbloomer.com/?p=21974
 * @author        Rodolfo Melogli
 * @credits       Claudio SM Web
 * @compatible    WC 3.2.1
 */
 
///////////////////////////////
// 1. ADD FIELDS
 
add_action( 'woocommerce_register_form_start', 'bbloomer_add_name_woo_account_registration', 9 );
 
function bbloomer_add_name_woo_account_registration() {
    ?>
 
    <p class="form-row form-row-half left">
    <label for="reg_billing_first_name"><?php _e( 'First name', 'woocommerce' ); ?> <span class="required">*</span></label>
    <input type="text" class="input-text" name="billing_first_name" id="reg_billing_first_name" value="<?php if ( ! empty( $_POST['billing_first_name'] ) ) esc_attr_e( $_POST['billing_first_name'] ); ?>" />
    </p>
 
    <p class="form-row form-row-half right">
    <label for="reg_billing_last_name"><?php _e( 'Last name', 'woocommerce' ); ?> <span class="required">*</span></label>
    <input type="text" class="input-text" name="billing_last_name" id="reg_billing_last_name" value="<?php if ( ! empty( $_POST['billing_last_name'] ) ) esc_attr_e( $_POST['billing_last_name'] ); ?>" />
    </p>
 
    <div class="clear"></div>
    <p class="form-row form-row-half left">
    <label for="reg_billing_company"><?php _e( 'Company Name', 'woocommerce' ); ?> <span class="required">*</span></label>
    <input type="text" class="input-text" name="billing_company" id="reg_billing_company" value="<?php if ( ! empty( $_POST['billing_company'] ) ) esc_attr_e( $_POST['billing_company'] ); ?>" />
    </p>
    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide form-row-half right">
        <label for="reg_email"><?php _e( 'Email <a href="#" title="This is the address your invoices & statements will be sent to!" class="df-tooltip reg"><span class="info"></span></a>', 'woocommerce' ); ?> <span class="required">*</span></label>
        <input type="email" class="woocommerce-Input woocommerce-Input--text input-text" name="email" id="reg_email" value="<?php if ( ! empty( $_POST['email'] ) ) echo esc_attr( $_POST['email'] ); ?>" />
    </p>
    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide" id="billing_phone_field">
        <label for="reg_billing_phone"><?php _e( 'Phone', 'woocommerce' ); ?> <span class="required">*</span></label>
        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="billing_phone" id="reg_billing_phone" value="<?php if ( ! empty( $_POST['billing_phone'] ) ) echo esc_attr( $_POST['billing_phone'] ); ?>" />
    </p>
 
    <?php
}
 
///////////////////////////////
// 2. VALIDATE FIELDS
 
add_filter( 'woocommerce_registration_errors', 'df_validate_name_fields', 10, 3 );
 
function df_validate_name_fields( $errors, $username, $email ) {
    if ( isset( $_POST['billing_first_name'] ) && empty( $_POST['billing_first_name'] ) ) {
        $errors->add( 'billing_first_name_error', __( '<strong>Error</strong>: First name is required!', 'woocommerce' ) );
    }
    if ( isset( $_POST['billing_last_name'] ) && empty( $_POST['billing_last_name'] ) ) {
        $errors->add( 'billing_last_name_error', __( '<strong>Error</strong>: Last name is required!.', 'woocommerce' ) );
    }
    if ( isset( $_POST['billing_phone'] ) && empty( $_POST['billing_phone'] ) ) {
        $errors->add( 'billing_phone_error', __( '<strong>Error</strong>: Phone is required!.', 'woocommerce' ) );
    }
    return $errors;
}
 
///////////////////////////////
// 3. SAVE FIELDS
 
add_action( 'woocommerce_created_customer', 'df_save_name_fields' );
 
function df_save_name_fields( $customer_id ) {
    if ( isset( $_POST['billing_first_name'] ) ) {
        update_user_meta( $customer_id, 'billing_first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
    }
    if ( isset( $_POST['billing_last_name'] ) ) {
        update_user_meta( $customer_id, 'billing_last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
    }
    if ( isset( $_POST['billing_company'] ) ) {
        update_user_meta( $customer_id, 'billing_company', sanitize_text_field( $_POST['billing_company'] ) );
    }
    if ( isset( $_POST['billing_phone'] ) ) {
        update_user_meta( $customer_id, 'billing_phone', sanitize_text_field( $_POST['billing_phone'] ) );
    }
 
}

// remove default sorting dropdown 
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

/* Add custom field for billing address */
add_action( 'woocommerce_checkout_order_processed', 'df_checkout_order_processed_add', 11, 2 );
function df_checkout_order_processed_add( $order_id, $posted ) {
    if ( ! isset( $_POST['billing_purchase_order_number'] ) ) {
        return;
    }

    $order = wc_get_order( $order_id );

    // WC < 3.0
    update_post_meta( $order->id, 'purchase_order_number', wc_clean( $_POST['purchase_order_number'] ) );

    // WC > 3.0
    $order->add_meta_data( 'purchase_order_number', wc_clean( $_POST['billing_purchase_order_number'] ), true );
    $order->save();
}

/* Display Purchase order number in order page on backend */
// display new split order id the order admin panel
function df_display_order_custom_fields_in_admin( $order ){  ?>
    <div class="form-field form-field-wide">
        <?php if(get_post_meta( $order->id, '_billing_purchase_order_number', true )!="") { ?>
        <h4><?php _e( 'Extra Details' ); ?></h4>
        <?php 
            echo '<p><strong>' . __( 'Purchase order number' ) . ': </strong>' . get_post_meta( $order->id, '_billing_purchase_order_number', true ) . '</p>';
        ?>
        <?php }
        if(get_post_meta( $order->id, 'delivery_method', true )!="") { ?>
        <?php 
            echo '<p><strong>' . __( 'Delivery Method' ) . ': </strong> '.get_post_meta( $order->id, 'delivery_method', true ).'</p>';
        ?>
        <?php } ?>
    </div>
<?php }
add_action( 'woocommerce_admin_order_data_after_order_details', 'df_display_order_custom_fields_in_admin' );

/* Order product by sku */
function sv_add_sku_sorting( $args ) {
    
    $args['orderby'] = 'meta_value';
    $args['order'] = 'asc'; 
        // ^ lists SKUs alphabetically 0-9, a-z; change to desc for reverse alphabetical
    $args['meta_key'] = '_sku';    

    return $args;
}
add_filter( 'woocommerce_get_catalog_ordering_args', 'sv_add_sku_sorting', 100 );

/*
 * Set default Shipping method for credit user
 */
/*function set_credit_user_shipping_method( $method, $available_methods ) {
    $user_info = get_currentuserinfo();
    $default_method = 'free_shipping:1';
    if( in_array('credit-account', $user_info->roles) && is_user_logged_in() ) {
        
        $method = $default_method;
    }
        
    return 'free_shipping:1';
}
add_filter('woocommerce_shipping_chosen_method', 'set_credit_user_shipping_method', 10, 2);*/

/**
 * Hide shipping rates when free shipping is available.
 * Updated to support WooCommerce 2.6 Shipping Zones.
 *
 * @param array $rates Array of rates found for the package.
 * @return array
 */
function my_hide_shipping_when_free_is_available( $rates ) {
    $free = array();

    if( is_credit_account() == true ) {
        foreach ( $rates as $rate_id => $rate ) {
            if ( 'free_shipping' === $rate->method_id ) {
                $free[ $rate_id ] = $rate;
                break;
            }
        }
    }
    else {
        foreach ( $rates as $rate_id => $rate ) {
            if ( ( 'free_shipping' === $rate->method_id && $rate_id == 'free_shipping:4' ) || ( 'free_shipping' === $rate->method_id && $rate_id == 'free_shipping:7' ) ) {
                $free[ $rate_id ] = $rate;
                break;
            }
        }
    }

    return ! empty( $free ) ? $free : $rates;
    /*if( in_array('credit-account', $user_info->roles) && is_user_logged_in() ) {
        
        foreach ( $rates as $rate_id => $rate ) {
            if ( 'free_shipping' === $rate->method_id && $rate_id == 'free_shipping:7' || 'free_shipping' === $rate->method_id && $rate_id == 'free_shipping:4' ) { // 7 on local, 4 on staging
                $credit_accounts[ $rate_id ] = $rate;
                break;
            }
        }
        return ! empty( $credit_accounts ) ? $credit_accounts : $rates;
    }
    else {
        foreach ( $rates as $rate_id => $rate ) {
            if ( 'free_shipping' === $rate->method_id ) {
                $free[ $rate_id ] = $rate;
                break;
            }
        }
        return ! empty( $free ) ? $free : $rates;
    }*/
    
    /*var_dump($free);
    var_dump($rates);*/
    
}
add_filter( 'woocommerce_package_rates', 'my_hide_shipping_when_free_is_available', 100 );

// Apply filter for body class
add_filter('body_class', 'df_body_class');

function df_body_class($classes) {
        if( is_cart() ) {
            $classes[] = 'df-cart';
        }
        
        if( is_credit_account() == true ) {
            $classes[] = 'df-credit-account';
        }

        return $classes;
}

/* Check if the user is credit account or not */
function is_credit_account() {
    if( is_user_logged_in() ) {
        $curr_users = get_currentuserinfo();
        $curr_users_role = $curr_users->roles;
        if(in_array('credit-account', $curr_users_role) ) {
            return true;
        }
        else {
            return false;
        }

    }
    else {
        return false;
    }
}

/*
 * Function to index sku for variable products for search - Removed this function as we are not using releveanssi plugin
 */
/*add_filter('relevanssi_content_to_index', 'rlv_index_variation_skus', 10, 2);
function rlv_index_variation_skus($content, $post) {
    if ($post->post_type == "product") {
        $args = array('post_parent' => $post->ID, 'post_type' => 'product_variation', 'posts_per_page' => -1);
        $variations = get_posts($args);
        if (!empty($variations)) {
            foreach ($variations as $variation) {
                $sku = get_post_meta($variation->ID, '_sku', true);
                $content .= " $sku";
            }
        }
    }
 
    return $content;
}*/

/* Save my account details */
add_filter('woocommerce_save_account_details_required_fields', 'df_wc_save_account_details_required_fields' );
function df_wc_save_account_details_required_fields( $required_fields ){
    unset( $required_fields['account_display_name'] );
    return $required_fields;
}

/* Calculate sub total */
//add_filter( 'woocommerce_cart_item_subtotal', 'df_woocommerce_cart_item_subtotal', 10, 3 );
function df_woocommerce_cart_item_subtotal( $wc, $cart_item, $cart_item_key ) { 
    // make filter magic happen here... 
    //var_dump($cart_item);
    //var_dump($wc);
    $qty = $cart_item['quantity'];
    $boxqty = $cart_item['data']->get_attribute( 'pa_boxqty' );
    $per_price = $cart_item['data']->get_price();
    $custom_price = $qty * $boxqty * $per_price;
    if( $custom_price > 0 ) {
        $wc = '<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>'.$custom_price.'</span>';
    }

    return $wc; 
}; 

/* 
 * Update shipping_is_active as per the shipping_changed value after saving the order
 */
add_action( 'save_post', 'df_udpate_order_meta', 10, 3 ); 
function df_udpate_order_meta( $post_id ) {
    $post_type = get_post_type($post_id);
    if ( "shop_order" != $post_type ) return;

    if( is_user_logged_in() ) {
        if( $_POST['shipping_address_id'] != '' && $_POST['shipping_is_active'] != '' ) {
            $selected_id = $_POST['shipping_address_id'];
            $curr_user_id = get_current_user_id();
            $addresses = get_user_meta( $curr_user_id, '_wcmca_additional_addresses', true);
            if( $addresses ) {
                $site_name          = $addresses[$selected_id]['shipping_site_contact'];
                $shipping_phone     = $addresses[$selected_id]['shipping_phone'];
                $shipping_state     = $addresses[$selected_id]['shipping_state'];
                $shipping_address_1 = $addresses[$selected_id]['shipping_address_1'];
                $shipping_address_2 = $addresses[$selected_id]['shipping_address_2'];
                $shipping_city      = $addresses[$selected_id]['shipping_city'];
                $shipping_postcode  = $addresses[$selected_id]['shipping_postcode'];
                $shipping_is_active = $addresses[$selected_id]['shipping_is_active'];
                $data['shipping_site_contact'];
                
                if( $site_name == $_POST['shipping_site_contact'] && $shipping_phone == $_POST['shipping_phone'] && $shipping_state == $_POST['shipping_state'] && $shipping_address_1 == $_POST['shipping_address_1'] && $shipping_address_2 == $_POST['shipping_address_2'] && $shipping_city == $_POST['shipping_city'] && $shipping_postcode == $_POST['shipping_postcode'] && $shipping_is_active == $_POST['shipping_is_active'] ) {
                    // keep is active as it is
                    //echo 'not changed';
                }
                else { //echo 'chnaged';
                    update_post_meta( $post_id, 'shipping_is_active', '0' );
                    update_post_meta( $post_id, '_shipping_is_active', '0' );
                }
                //die();
            } 
        }
        else {
            if( $_POST['shipping_is_active'] == '' ) {
                update_post_meta( $post_id, 'shipping_is_active', '0' );
                update_post_meta( $post_id, '_shipping_is_active', '0' );
            }
        }
        //die();
    }
    else {
        if( $_POST['shipping_is_active'] == '' ) {
            update_post_meta( $post_id, 'shipping_is_active', '0' );
            update_post_meta( $post_id, '_shipping_is_active', '0' );
        }
    }
    /* Save address of the selected list from dropdown for credit users */
    if( is_credit_account() ) {
        if( $_POST['wmua_address_id'] != "" ) {
            $address_id = trim($_POST['wmua_address_id']);
            $curr_user_id = get_current_user_id();
            $addresses = get_user_meta( $curr_user_id, '_wcmca_additional_addresses', true);
            if( $addresses ) {
                $site_name          = $addresses[$address_id]['shipping_site_contact'];
                $shipping_phone     = $addresses[$address_id]['shipping_phone'];
                $shipping_state     = $addresses[$address_id]['shipping_state'];
                $shipping_address_1 = $addresses[$address_id]['shipping_address_1'];
                $shipping_address_2 = $addresses[$address_id]['shipping_address_2'];
                $shipping_city      = $addresses[$address_id]['shipping_city'];
                $shipping_postcode  = $addresses[$address_id]['shipping_postcode'];
                $shipping_is_active = $addresses[$address_id]['shipping_is_active'];
                
                update_post_meta( $post_id, '_shipping_site_contact', $site_name );
                update_post_meta( $post_id, 'shipping_site_contact', $site_name );

                update_post_meta( $post_id, '_shipping_phone', $shipping_phone );
                update_post_meta( $post_id, 'shipping_phone', $shipping_phone );

                update_post_meta( $post_id, '_shipping_address_1', $shipping_address_1 );
                update_post_meta( $post_id, 'shipping_address_1', $shipping_address_1 );

                update_post_meta( $post_id, '_shipping_address_2', $shipping_address_2 );
                update_post_meta( $post_id, 'shipping_address_2', $shipping_address_2 );

                update_post_meta( $post_id, '_shipping_city', $shipping_city );
                update_post_meta( $post_id, 'shipping_city', $shipping_city );

                update_post_meta( $post_id, '_shipping_state', $shipping_state );
                update_post_meta( $post_id, 'shipping_state', $shipping_state );

                update_post_meta( $post_id, '_shipping_phone', $shipping_postcode );
                update_post_meta( $post_id, '_shipping_phone', $shipping_postcode );

                update_post_meta( $post_id, '_shipping_is_active', $shipping_is_active );
                update_post_meta( $post_id, 'shipping_is_active', $shipping_is_active );
            }
    
        }
    }
}

//https://searchwp.com/extensions/woocommerce-integration/
add_filter( 'searchwp_missing_integration_notices', '__return_false' );

// index WooCommerce product_variation SKUs with the parent post - SearchWP
function my_searchwp_index_woocommerce_variation_skus( $extra_meta, $post_being_indexed ) {
    // we only care about WooCommerce Products
    if ( 'product' !== get_post_type( $post_being_indexed ) ) {
        return $extra_meta;
    }
    // retrieve all the product variations
    $args = array(
        'post_type'       => 'product_variation',
        'posts_per_page'  => -1,
        'fields'          => 'ids',
        'post_parent'     => $post_being_indexed->ID,
    );
    $product_variations = get_posts( $args );
    if ( ! empty( $product_variations ) ) {
        // store all SKUs as a Custom Field with a key of 'my_product_variation_skus'
        $extra_meta['my_product_variation_skus'] = array();
        // loop through all product variations, grab and store the SKU
        foreach ( $product_variations as $product_variation ) {
            $extra_meta['my_product_variation_skus'][] = get_post_meta( absint( $product_variation ), '_sku', true );
        }
    }
    return $extra_meta;
}
add_filter( 'searchwp_extra_metadata', 'my_searchwp_index_woocommerce_variation_skus', 10, 2 );

// reference: https://gist.github.com/jchristopher/0cad8418fd9477c57b53
function my_searchwp_custom_field_keys_variation_skus( $keys ) {
  $keys[] = 'my_product_variation_skus';
  
  return $keys;
}
 
add_filter( 'searchwp_custom_field_keys', 'my_searchwp_custom_field_keys_variation_skus', 10, 1 );

// remove the action 
//remove_action( 'woocommerce_email_order_meta', 'action_woocommerce_email_order_meta', 10, 4 );

//add_action( 'woocommerce_email_order_meta', 'df_order_email', 10, 4 );
function df_order_email( $order, $sent_to_admin, $plain_text, $email ) {
    /*$items = $order->get_items(); 
    foreach ($items as $key => $item) {
        $product_name = $item['name'];
        $product_id = $item['product_id'];
        $terms = get_the_terms( $product_id, 'product_cat' );
        $args = array(
            'post_type'      => 'product',
            'posts_per_page' => 10,
            'product_cat'    => $terms[0]->slug
        );
    
        $loop = new WP_Query( $args );
        echo 'Here are few more products based on your recent purchase';
    
        while ( $loop->have_posts() ) : $loop->the_post();
            global $product;
            echo '<br><a href="'.get_permalink().'">' . get_the_title().'</a>';
        endwhile;
    
        wp_reset_query();
    }*/
    echo 'Sales Order Number: '.get_post_meta( $order->id, '_billing_purchase_order_number', true );
}

// define the woocommerce_lost_password_message callback 
function df_filter_woocommerce_lost_password_message( $var ) { 
    // make filter magic happen here... 
    if( !isset($_REQUEST['reset-link-sent']) )
        return "Lost your password? Please enter your email address. You will receive a link to create a new password via email."; 
    else
        return $var;
}; 
         
// add the filter 
add_filter( 'woocommerce_lost_password_message', 'df_filter_woocommerce_lost_password_message', 10, 1 );

add_filter('woocommerce_checkout_get_value','custom_checkout_get_value', 10, 2);
function custom_checkout_get_value( $value, $imput ){
    if( !is_user_logged_in() ) {
         if( $imput == 'billing_country' || $imput == 'shipping_country' ) {
            //echo 'hahabilling'.$value;
            return $value;
        }
        else
            $value = '';
    }
    else {
        if($imput == 'billing_purchase_order_number')
            $value = '';

        if( $imput == 'billing_country' || $imput == 'shipping_country' ) {
            //echo 'hahabilling'.$value;
            return $value;
        }
        if( checkDefaultAddress() === false ) {
            if($imput == 'shipping_site_contact')
                $value = '';
            if($imput == 'shipping_phone')
                $value = '';
            if($imput == 'shipping_address_1')
                $value = '';
            if($imput == 'shipping_address_2')
                $value = '';
            if($imput == 'shipping_city')
                $value = '';
            if($imput == 'shipping_state')
                $value = '';
            if($imput == 'shipping_postcode')
                $value = '';

        }
        else {
            $value = $value;
        }
    }
        
    return $value;
}

/* Function to check if users have defualt address */
function checkDefaultAddress() {
    $show_delivery_address = false;
    if( is_user_logged_in() ) {
        $addresses = get_user_meta( get_current_user_id(), '_wcmca_additional_addresses', true);
        //var_dump($addresses);
        
        if( $addresses ) {
            foreach( $addresses as $address ) {
                if( isset( $address['shipping_is_default_address'] ) ) {
                    $show_delivery_address = true;
                }
            }
        }
    }

    return $show_delivery_address;
}


// Change the default country and state on checkout page. 
// This works for a new session.
add_filter( 'default_checkout_country', 'xa_set_default_checkout_country' );
//add_filter( 'default_checkout_state', 'xa_set_default_checkout_state' );
function xa_set_default_checkout_country() {
  // Returns empty country by default.
  //   return null;
  // Returns US as default country.
     return 'AU';
}

function xa_set_default_checkout_state() {
  // Returns empty state by default.
  //   return null;
  // Returns California as default state.
     return '';
}

/* Update cart quantity for box qty on cart page */
add_action( 'woocommerce_after_cart_item_quantity_update', 'df_udpate_cart_item_quantity', 20, 4 );
function df_udpate_cart_item_quantity( $cart_item_key, $quantity, $old_quantity, $cart ){
    /*if(is_cart()) {
        echo 'This is cart';
    } die();
    return;*/
    /*var_dump($_SERVER);
    if( is_cart() ) {
        echo 'cart page'; die();
    }
    else {
        echo 'no cart page'; die();
    }
    echo 'maya maya '; die();
    if( ! is_cart() ) return; // Only on cart page
    echo 'maya'; die();*/

    // Here the quantity limit
    /*$limit = 5;

    if( $quantity > $limit ){
        // Change the quantity to the limit allowed
        $cart->cart_contents[ $cart_item_key ]['quantity'] = $limit;
        // Add a custom notice
        wc_add_notice( __('Quantity limit reached for this item'), 'notice' );
    }*/
    
    if( ( isset( $_POST['action']) && $_POST['action'] == 'df_ajax_add_to_cart' ) || ( isset( $_POST['action']) && $_POST['action'] == 'bodycommerce_ajax_add_to_cart_woo_single' ) || ( isset( $_POST['action']) && $_POST['action'] == 'bodycommerce_ajax_add_to_cart_woo' ) || ( isset($_GET['wlid']) &&  isset($_GET['wlaction']) && $_GET['wlaction'] == 'add-cart-item' && isset($_GET['wishlist-item-key']) ) ) {
        $quantity = $quantity;
        
    }
    else if( is_page( 'edit-my-list' ) && isset( $_GET['wlid'] ) ) {
        $quantity = $quantity;
    }
    else {
        
        if( $cart->cart_contents[$cart_item_key]['variation_id'] != 0 ) {
            $boxqty = $cart->cart_contents[$cart_item_key]['variation']['attribute_pa_boxqty'];          
        }
        else {
            $item_data = $cart->cart_contents[$cart_item_key]['data'];
            $attributes = $item_data->get_attributes();
            $boxqty = $item_data->get_attribute( 'pa_boxqty' );
        }
        if( $boxqty ) {
            $boxqty_val = $boxqty;
        }
        else
            $boxqty_val = 1; 
        
        $quantity = $quantity * $boxqty_val;
    }
    /*print_r("<pre style='margin-left:50px'>");
    print_r($cart);

    //print_r($cart->cart_contents[$cart_item_key]['attributes']);
     print_r( $prod_boxqty);
     echo 'Maya';
    print_r("</pre>");
    die();*/
    $cart->cart_contents[ $cart_item_key ]['quantity'] = $quantity;
}

// define the woocommerce_checkout_cart_item_quantity callback 
function df_woocommerce_checkout_cart_item_quantity( $strong_class_product_quantity_sprintf_times_s_cart_item_quantity_strong, $cart_item, $cart_item_key ) { 
    // make filter magic happen here... 
    /*print_r("<pre style='margin-left:50px'>");
    print_r($cart_item);
    print_r("</pre>");*/

    if( $cart_item['variation_id'] != 0 ) {
        $boxqty = $cart_item['variation']['attribute_pa_boxqty'];
    }
    else {
        $product = wc_get_product( $cart_item['product_id'] );
        $boxqty = $product->get_attribute( 'pa_boxqty' );
    }
    if( $boxqty != '' )
        $boxqty_val = $boxqty;
    else
        $boxqty_val = 1;
    

    if( is_checkout() ) {
        $quantity = $cart_item['quantity'];
        $strong_class_product_quantity_sprintf_times_s_cart_item_quantity_strong = $quantity / $boxqty_val;
        return ' x '.$strong_class_product_quantity_sprintf_times_s_cart_item_quantity_strong;
    }

    return $strong_class_product_quantity_sprintf_times_s_cart_item_quantity_strong; 
}; 
         
// add the filter 
add_filter( 'woocommerce_checkout_cart_item_quantity', 'df_woocommerce_checkout_cart_item_quantity', 10, 3 );

// define the woocommerce_email_order_item_quantity callback 
function df_filter_woocommerce_email_order_item_quantity( $item_qty, $item ) { 
    // make filter magic happen here... 
    $order_data = $item->get_data();
    if( $item['variation_id'] != 0 ) {
        $product = wc_get_product( $order_data['variation_id'] );
        $boxqty = $product->get_attribute( 'pa_boxqty' );
        //$boxqty = $item['variation']['attribute_pa_boxqty'];
    }
    else {
        $product = wc_get_product( $order_data['product_id'] );
        $boxqty = $product->get_attribute( 'pa_boxqty' );
    }
    if( $boxqty != '' )
        $boxqty_val = $boxqty;
    else
        $boxqty_val = 1;

    
    $quantity = $order_data['quantity'];
    $custom_qty = $quantity / $boxqty_val;
    //$custom_qty = '<strong class="product-quantity"> × '.$custom_qty.'</strong>';

    return $custom_qty; 
}; 
         
// add the filter 
add_filter( 'woocommerce_email_order_item_quantity', 'df_filter_woocommerce_email_order_item_quantity', 10, 2 ); 

function calculate_price_by_boxqty( $boxqty, $price ) {
    $price = strip_tags($price);
    /*$price = str_replace(array( '$', '&#36;' ), array('',''),(string)$price);
    $total_price = number_format($boxqty * $price,5);
    return '$'. $total_price;*/
    return $price;
}

function get_cart_qty() {
    global $woocommerce;
   
    $items = $woocommerce->cart->get_cart();
    /*var_dump($items);
    echo count($items);*/
   
    if( count($items) > 0 ) {
        return count($items);
    }
    else
        return;
}

/* update quantity of the cart for wishilist */
function df_get_wishlist_qty( $product, $quantity, $cart_quantity ) {
    //echo $quantity.'haha'.$cart_quantity;
    $boxqty_val = $product->get_attribute( 'pa_boxqty' );
    $quantity; //die();
    //echo $boxqty_val = get_post_meta( $product_id, 'attribute_pa_boxqty', true );
    if( $boxqty_val != "" ) {
        $quantity = $quantity * $boxqty_val;
    }
    //die();
    return $quantity;
}